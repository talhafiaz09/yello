import React, {useEffect} from 'react';
import {AppRegistry} from 'react-native';
import AppNavigation from './source/navigation/mainnavigation/AppNavigation';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import stores from './source/redux_handler/store/store';
const {configureStore, persistor} = stores();
const ReduxHandler = () => (
  <Provider store={configureStore}>
    <PersistGate persistor={persistor} loading={null}>
      <AppNavigation />
    </PersistGate>
  </Provider>
);
AppRegistry.registerComponent(appName, () => ReduxHandler);
