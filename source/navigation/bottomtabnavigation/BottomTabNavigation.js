import React from 'react';
import Home from '../../views/home/Home';
import Wallet from '../../views/wallet/Wallet';
import * as Animatable from 'react-native-animatable';
import Call from '../../views/call/Call';
import History from '../../views/history/History';
import {Text, TouchableOpacity, View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import WalletIcon from '../../components/wallet_icon/WalletIcon';
import Profile from '../../views/profile/Profile';
import useStyles from './useStyles';

const Tab = createBottomTabNavigator();
const BottomTabNavigation = () => {
  const {Styles, COLORS} = useStyles();

  const IconCnt = ({name, focused, size, color}) => {
    return (
      <View style={Styles.bg_icon(focused)}>
        <Ionicons
          color={focused ? COLORS.color_white : color}
          name={name}
          size={size ? size : 22}
        />
      </View>
    );
  };
  const IconCntCr = ({name, focused, color}) => {
    return (
      <View style={Styles.bg_iconCi(focused)}>
        <Ionicons
          color={focused ? COLORS.color_white : color}
          name={name}
          size={22}
        />
      </View>
    );
  };

  return (
    <Tab.Navigator
      // initialRouteName={'Profile'}
      tabBarOptions={{
        activeTintColor: COLORS.color_yellow,
        inactiveTintColor: COLORS.color_bt_icon,
        style: Styles.tabContainer,
        safeAreaInsets: {
          bottom: 0,
        },
        tabStyle: {},
      }}
      screenOptions={({route}) => ({
        tabBarLabel: ({focused}) => (
          <Text style={Styles.labelStyle(focused)}> {route.name}</Text>
        ),
      })}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          // unmountOnBlur: true,
          tabBarIcon: ({color, focused}) => (
            <IconCnt name={'home'} focused={focused} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Wallet"
        component={WalletScreen}
        options={{
          tabBarIcon: ({color, focused}) => (
            <WalletIcon
              backgroundColor={
                focused ? COLORS.color_yellow : COLORS.color_transparent
              }
              color={focused ? COLORS.color_white : COLORS.color_bt_icon}
              borderColor={focused ? COLORS.color_textfield : color}
              fontFamily={focused ? 'Poppins-Bold' : 'Poppins-Regular'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Call"
        component={CallScreen}
        options={{
          unmountOnBlur: true,
          tabBarIcon: ({focused}) => (
            <IconCntCr
              name={'call'}
              focused={focused}
              color={COLORS.color_white}
            />
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={HistoryScreen}
        options={{
          tabBarIcon: ({color, focused}) => (
            <IconCnt
              name={'md-timer'}
              focused={focused}
              color={color}
              size={24}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({color, focused}) => (
            <IconCnt name={'person'} focused={focused} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};
const HomeScreen = ({navigation}) => {
  return <Home navigation={navigation} />;
};
const WalletScreen = ({navigation}) => {
  return <Wallet navigation={navigation} />;
};
const CallScreen = ({navigation}) => {
  return <Call navigation={navigation} />;
};

const HistoryScreen = ({navigation}) => {
  return <History navigation={navigation} />;
};
const ProfileScreen = ({navigation}) => {
  return <Profile navigation={navigation} />;
};
export default BottomTabNavigation;
