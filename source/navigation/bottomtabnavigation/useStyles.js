import {StyleSheet, Platform} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    labelStyle: (focused) => {
      return {
        fontFamily: focused ? 'Poppins-Bold' : 'Poppins-Regular',
        fontSize: 12,
        color: focused ? COLORS.color_yellow : COLORS.color_bt_icon,
        marginBottom: Platform.OS === 'android' ? 5 : 20,
      };
    },
    tabContainer: {
      height: 80,
      backgroundColor: COLORS.color_textfield,
      borderColor: COLORS.color_transparent,
      borderWidth: 0,
    },
    bg_icon: (focused) => {
      return {
        backgroundColor: focused ? COLORS.color_yellow : COLORS.color_textfield,
        padding: 5,
        borderRadius: 10,
      };
    },
    bg_iconCi: (focused) => {
      return {
        backgroundColor: focused ? COLORS.color_yellow : COLORS.color_bt_icon,
        padding: 15,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 300,
        position: 'absolute',
        top: Platform.OS === 'android' ? 0 : -20,
      };
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
