import 'react-native-gesture-handler';
import SplashScreen from 'react-native-splash-screen';
import React, {useEffect, useState} from 'react';
import GettingStarted from '../../views/gettingstarted/GettingStarted';
import OutgoingCall from '../../views/outgoingcall/OutgoingCall';
// import Notification from '../notification/Notification';
import Toast from 'react-native-toast-message';
import CustomStatusBar from '../../components/statusBar/CustomStatusBar';
import Splash from '../../views/splash/Splash';
import DrawerNavigation from '../drawernavigation/DrawerNavigation';
import AddCredit from '../../views/addcredit/AddCredit';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useDispatch, useSelector} from 'react-redux';
import {addNotification} from '../../redux_handler/actions/notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
import database from '@react-native-firebase/database';
import messaging from '@react-native-firebase/messaging';
import {setCurrencyRate} from '../../redux_handler/actions/user';
import crashlytics from '@react-native-firebase/crashlytics';
import useAsyncStorage from '../../handlers/useAsyncStorage';
import {setuserInformation} from '../../redux_handler/actions/user';
import {setMessages} from '../../redux_handler/actions/app';
import {Platform} from 'react-native';
const Stack = createStackNavigator();
const AppNavigation = () => {
  const [loading, setLoading] = useState(true);

  const {getUserInformationAsyncStorage, setUserInformationAsyncStorage} =
    useAsyncStorage();

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  // const currency_rates = useSelector(
  //   (state) => state.userReducer.currency_rates,
  // );
  const messages = useSelector((state) => state.appReducer.messages);

  const dispatch = useDispatch();

  const add_Notification = async (notification) =>
    dispatch(addNotification(notification));
  const setCurrency_Rate = async (info) => dispatch(setCurrencyRate(info));
  const setuser_Information = async (info) =>
    dispatch(setuserInformation(info));
  const set_Messages = async (data) => dispatch(setMessages(data));

  useEffect(async () => {
    SplashScreen.hide();
    messaging().onNotificationOpenedApp((remoteMessage) => {
      // console.log(
      //   'Notification caused app to open from background state:',
      //   remoteMessage.notification,
      // );
    });

    // Check whether an initial notification is available
    messaging().setBackgroundMessageHandler(async (remoteMessage) => {
      // console.log('Message handled in the background!', remoteMessage);
      add_Notification({
        title: remoteMessage.notification.title,
        message: remoteMessage.notification.body,
        time: new Date().getTime(),
        seen: false,
      });
    });

    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        if (remoteMessage && Platform.OS == 'android') {
          // console.log(
          //   'Notification caused app to open from quit state:',
          //   remoteMessage.notification,
          // );
          add_Notification({
            title: remoteMessage.notification.title,
            message: remoteMessage.notification.body,
            time: new Date().getTime(),
            seen: false,
          });
          // setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
        }
        setLoading(false);
      });

    messaging().onMessage((remoteMessage) => {
      // console.log(remoteMessage);
      if (Platform.OS == 'ios') {
        add_Notification({
          title: remoteMessage.notification.title,
          message: remoteMessage.notification.body,
          time: new Date().getTime(),
          seen: false,
        });
      }
    });

    try {
      const onValueChange = database()
        .ref(`/conversion_rates`)
        .on('value', (snapshot) => {
          // console.log(snapshot.val());
          setCurrency_Rate(snapshot.val());
        });
      await getUserInformationAsyncStorage().then(async (res) => {
        if (res != null) {
          const onValueChangeUser = database()
            .ref(`/customers/` + res.phoneNumber)
            .on('value', async (snapshot) => {
              // console.log('Num: ', snapshot.val().balance, res);
              setuser_Information({
                ...res,
                balance: parseFloat(snapshot.val().balance),
              }).then((res) => {
                // console.log('Saving to async: ', res);
                setUserInformationAsyncStorage(res.data);
              });
            });
        }
      });
    } catch (error) {
      // console.log('Firebase fetch error: ', error.message);
    }
    return () => {
      database().ref(`/conversion_rates`).off('value', onValueChange);
      database().ref(`/customers`).off('value', onValueChangeUser);
      database()
        .ref(`/chats/` + res.phoneNumber + `/` + today)
        .off('value', onValueChangeToday);
    };
  }, []);

  useEffect(() => {
    var onValueChangeToday;
    if (userInformation && userInformation.phoneNumber) {
      var today = new Date();
      // var yesterday = today;
      var dd_today = String(today.getUTCDate()).padStart(2, '0');
      // var dd_yesterday = String(today.getUTCDate()() - 1).padStart(2, '0');
      var mm = String(today.getUTCMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getUTCFullYear();
      today = yyyy + '-' + mm + '-' + dd_today;
      // yesterday = yyyy + '-' + mm + '-' + dd_yesterday;
      onValueChangeToday = database()
        .ref(`/chats/` + userInformation?.phoneNumber + `/` + today)
        .orderByKey()
        .on('value', (snapshot) => {
          // console.log(snapshot);
          const array = [];
          snapshot.forEach((e) => array.push(e.val()));
          array.reverse();
          set_Messages([...array]);
        });
    }
    return () => {
      database()
        .ref(`/chats/` + userInformation?.phoneNumber + `/` + today)
        .off('value', onValueChangeToday);
    };
  }, [userInformation]);

  useEffect(() => {
    PushNotification.configure({
      onRegister: function (token) {
        // console.log('Token:', token);
      },
      onNotification: function (notification) {
        // console.log('NOTIFICATION:', notification);
        notification.title
          ? add_Notification({
              title: notification.title,
              message: notification.message,
              time: new Date().getTime(),
              seen: false,
            })
          : null;
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      onAction: function (notification) {
        // console.log('ACTION:', notification.action);
        // console.log('NOTIFICATION:', JSON.stringify(notification));
      },
      onRegistrationError: function (err) {
        // console.error('Notification error: ', err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
  });

  const COLOR = useSelector((state) => state.appReducer.statusBarColor);

  if (loading) {
    return null;
  }

  return (
    <NavigationContainer>
      <CustomStatusBar backgroundColor={COLOR} />
      <Stack.Navigator
        // initialRouteName={'AddCredit'}
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Splash" component={splashScreen} />
        <Stack.Screen name="GettingStarted" component={gettingStarted} />
        <Stack.Screen name="DrawerNavigation" component={drawerNavigation} />
        <Stack.Screen name="OutgoingCall" component={outgoingCall} />
        <Stack.Screen name="AddCredit" component={addCredit} />
        {/*<Stack.Screen name="Notification" component={notification} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const splashScreen = ({navigation}) => {
  return <Splash navigation={navigation} />;
};
const gettingStarted = ({navigation}) => {
  return <GettingStarted navigation={navigation} />;
};
const drawerNavigation = ({navigation}) => {
  return <DrawerNavigation navigation={navigation} />;
};
const addCredit = ({navigation}) => {
  return <AddCredit navigation={navigation} />;
};
const outgoingCall = ({navigation}) => {
  return <OutgoingCall navigation={navigation} />;
};
// const notification = ({navigation}) => {
//   return <Notification navigation={navigation} />;
// };
export default AppNavigation;
