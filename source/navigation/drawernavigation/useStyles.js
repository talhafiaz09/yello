import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    labelStyle: {fontFamily: 'Poppins-Regular', fontSize: 12},
    itemStyle: {
      borderBottomWidth: 0.25,
      borderBottomColor: COLORS.color_white,
    },
    contentContainerStyle: {
      paddingLeft: 10,
      paddingRight: 10,
    },
    drawerStyle: {width: '70%', backgroundColor: COLORS.color_side},
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
