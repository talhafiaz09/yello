import React from 'react';
import CustomDrawer from '../../components/customdrawer/CustomDrawer';
import BottomTabNavigation from '../bottomtabnavigation/BottomTabNavigation';
import CallingRates from '../../views/callingrates/CallingRates';
import CallingPackages from '../../views/callingpackages/CallingPackages';
import ShareBalance from '../../views/sharebalance/ShareBalance';
import RewardPoints from '../../views/rewardpoints/RewardPoints';
import Refund from '../../views/refund/Refund';
import Coupons from '../../views/coupons/Coupons';
import LiveSupport from '../../views/livesupport/LiveSupport';
import Invite from '../../views/invite/Invite';
import Referrals from '../../views/referrals/Referrals';
import Help from '../../views/help/Help';
import Setting from '../../views/setting/Setting';
import {createDrawerNavigator} from '@react-navigation/drawer';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import useStyles from './useStyles';

const DrawerNavigation = ({navigation}) => {
  const Drawer = createDrawerNavigator();

  const {Styles, COLORS} = useStyles();

  return (
    <>
      <Drawer.Navigator
        // initialRouteName={'Referrals'}
        drawerType={'slide'}
        drawerContent={(props) => <CustomDrawer {...props} />}
        drawerStyle={Styles.drawerStyle}
        drawerContentOptions={{
          activeTintColor: COLORS.color_yellow,
          labelStyle: Styles.labelStyle,
          inactiveTintColor: COLORS.color_white,
          activeBackgroundColor: COLORS.color_transparent,
          itemStyle: Styles.itemStyle,
          contentContainerStyle: Styles.contentContainerStyle,
        }}>
        <Drawer.Screen
          name="Dashboard"
          component={BottomTabNavigationScreen}
          options={{
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="view-dashboard"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Calling Rates"
          component={CallingRatesScreen}
          options={{
            unmountOnBlur: true,
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="phone-in-talk"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Calling Packages"
          component={CallingPackagesScreen}
          options={{
            unmountOnBlur: true,
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="gift"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Reward Points"
          component={RewardPointsScreen}
          options={{
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="seal"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Coupons"
          component={CouponsScreen}
          options={{
            unmountOnBlur: true,
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="label-percent"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Share Balance"
          component={ShareBalanceScreen}
          options={{
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="compare-horizontal"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Refund"
          component={RefundScreen}
          options={{
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="cash-refund"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Invite Friends"
          component={InviteFriendScreen}
          options={{
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="human-male-male"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Referrals"
          component={ReferralsScreen}
          options={{
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="graph-outline"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Live support"
          component={LiveSupportScreen}
          options={{
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="chat-processing"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="FAQs"
          component={HelpScreen}
          options={{
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="comment-question"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Setting"
          component={SettingScreen}
          options={{
            drawerIcon: ({focused, size}) => (
              <MaterialCommunityIcons
                name="cog"
                size={size}
                color={focused ? COLORS.color_yellow : COLORS.color_white}
              />
            ),
          }}
        />
      </Drawer.Navigator>
    </>
  );
};
const BottomTabNavigationScreen = ({navigation}) => {
  return <BottomTabNavigation navigation={navigation} />;
};
const CallingRatesScreen = ({navigation}) => {
  return <CallingRates navigation={navigation} />;
};
const CallingPackagesScreen = ({navigation}) => {
  return <CallingPackages navigation={navigation} />;
};
const SettingScreen = ({navigation}) => {
  return <Setting navigation={navigation} />;
};
const RewardPointsScreen = ({navigation}) => {
  return <RewardPoints navigation={navigation} />;
};
const CouponsScreen = ({navigation}) => {
  return <Coupons navigation={navigation} />;
};
const ShareBalanceScreen = ({navigation}) => {
  return <ShareBalance navigation={navigation} />;
};
const RefundScreen = ({navigation}) => {
  return <Refund navigation={navigation} />;
};
const HelpScreen = ({navigation}) => {
  return <Help navigation={navigation} />;
};
const LiveSupportScreen = ({navigation}) => {
  return <LiveSupport navigation={navigation} />;
};
const InviteFriendScreen = ({navigation}) => {
  return <Invite navigation={navigation} />;
};
const ReferralsScreen = ({navigation}) => {
  return <Referrals navigation={navigation} />;
};
export default DrawerNavigation;
