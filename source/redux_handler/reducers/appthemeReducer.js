import {APP_THEME, LOG_OUT} from '../actions/types';
import {COLORS} from '../../assets/colors/Colors';
const initialState = {
  colors: {...COLORS.COLORS_LIGHT},
  dark: false,
};
const appthemeReducer = (state = initialState, action) => {
  switch (action.type) {
    case APP_THEME:
      return {
        ...state,
        colors: {...COLORS[action.data]},
        dark: action.data === 'COLORS_DARK' ? true : false,
      };
    default:
      return state;
  }
};

export default appthemeReducer;
