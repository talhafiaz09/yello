import {CALL_START_TIME, CALL_END_TIME, CLEAR} from '../actions/types';
const initialState = {
  callStartTime: null,
  callEndTime: null,
};
const callReducer = (state = initialState, action) => {
  switch (action.type) {
    case CALL_START_TIME:
      return {...state, callStartTime: action.data};
    case CALL_END_TIME:
      return {...state, callEndTime: action.data};
    case CLEAR:
      return {...initialState};
    default:
      return state;
  }
};
export default callReducer;
