import {
  STATUS_BAR_COLOR,
  COUNTRYMODAL_VISIBILITY,
  RESENDOTP_VISIBILITY,
  SET_CALLING_RATES,
  SEARCH_BAR,
  DIALTO_INFORMATION,
  DIALED_NUMBER,
  DELETE_DIALED_NUMBER,
  CLEAR_DIALED_NUMBER,
  CONTACTSMODAL_VISIBILITY,
  CONTACT_LIST,
  CALL_STATE,
  MODAL_STRUCTURE,
  COUNTRIES,
  PACKAGES,
  PLATFORM,
  COUPONS,
  CONFIGURATION,
  CALL_STATUS,
  ACCOUNT_DETAIL,
  CALL,
  CALL_START_TIME,
  CALL_END_TIME,
  CALL_DATA,
  CALL_TERMINATION,
  LOG_OUT,
  NUMBER_HISTORY,
  SIGNUP_COUNTRIES,
  QUESTIONS,
  MESSAGES,
  CURSOR,
} from '../actions/types';
import {Endpoint} from 'react-native-pjsip';
import {COLORS} from '../../assets/colors/Colors';
import {callEnd} from '../api/index';
const initialState = {
  countries: [],
  signupCountries: [],
  platforms: '',
  statusBarColor: COLORS.COLORS_DARK.color_primary,
  countryModalVisibility: false,
  contactsModalVisibility: false,
  resendOtpVisibility: false,
  dialPad: [
    {key: '1'},
    {key: '2'},
    {key: '3'},
    {key: '4'},
    {key: '5'},
    {key: '6'},
    {key: '7'},
    {key: '8'},
    {key: '9'},
    {icon: 'phone-in-talk'},
    {key: '0'},
    {icon: 'backspace'},
  ],
  callingRates: [],
  callingPackages: [],
  searchBarText: '',
  dialtoInformation: {
    // countryCode: 'US',
    // dialCode: 1,
    // countryId: 224,
    countryCode: '',
    dialCode: -1,
    countryId: -1,
    dialedFrom: '',
    dialedTo: '',
  },
  dialedNumber: '',
  callState: {microphone: true, speaker: false},
  contactList: [],
  randomAmounts: ['10', '15', '20', '25', '50', '100'],
  modalStructure: {
    visible: false,
    action: '',
    type: 'confirm',
    content: '',
    data: {},
    buttonDisabled: false,
  },
  endpoint: new Endpoint(),
  coupons: [],
  configuration: {
    // name: '923365576517',
    // proxy: 'sip:194.28.167.180:5060', // default disabled.
    // regSerrver: 'sip:194.28.167.180:5060', // default taken from domain
    username: null,
    domain: null,
    password: null,
    proxy: null,
    transport: 'UDP', // default TCP
    regServer: null,
    regTimeout: '2592000', // default 300
  },
  accountDetail: null,
  callStatus: {status: 'Connecting', active: false},
  call: null,
  callStartTime: null,
  callEndTime: null,
  callData: null,
  numberHistory: '',
  cursor: {end: 0, start: 0},
  questions: [],
  messages: [],
};
const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case STATUS_BAR_COLOR:
      return {...state, statusBarColor: action.data};
    case COUNTRYMODAL_VISIBILITY:
      return {...state, countryModalVisibility: !state.countryModalVisibility};
    case RESENDOTP_VISIBILITY:
      return {...state, resendOtpVisibility: !state.resendOtpVisibility};
    case SET_CALLING_RATES:
      return {...state, callingRates: action.data};
    case SEARCH_BAR:
      return {...state, searchBarText: action.data};
    case DIALTO_INFORMATION:
      return {...state, dialtoInformation: action.data};
    case CURSOR:
      return {...state, cursor: {...action.data}};
    case DIALED_NUMBER:
      return {
        ...state,
        dialedNumber:
          action.data.length > 1
            ? action?.data?.replace(/\D+/g, '')
            : action.data
            ? state.dialedNumber.slice(0, state.cursor.end) +
              action?.data?.replace(/\D+/g, '') +
              state?.dialedNumber?.slice(state.cursor.end)
            : '',
        cursor: {
          start: action.data
            ? state.cursor.end + action?.data?.replace(/\D+/g, '').length
            : 0,
          end: action.data
            ? state.cursor.end + action?.data?.replace(/\D+/g, '').length
            : 0,
        },
      };

    case DELETE_DIALED_NUMBER:
      return {
        ...state,
        dialedNumber:
          state.cursor.start == 0 &&
          state.cursor.end == state.dialedNumber.length
            ? ''
            : state.cursor.end != 0
            ? state.dialedNumber.slice(0, state.cursor.end - 1) +
              state.dialedNumber.slice(state.cursor.end)
            : state.dialedNumber,
        cursor: {
          end:
            state.cursor.start == 0 &&
            state.cursor.end == state.dialedNumber.length
              ? 0
              : state.cursor.end > 0
              ? state.cursor.end - 1
              : state.cursor.end,
          start:
            state.cursor.start == 0 &&
            state.cursor.end == state.dialedNumber.length
              ? 0
              : state.cursor.end > 0
              ? state.cursor.end - 1
              : state.cursor.end,
        },
      };
    case SIGNUP_COUNTRIES:
      return {...state, signupCountries: action.data};
    case CLEAR_DIALED_NUMBER:
      return {...state, dialedNumber: ''};
    case CONTACTSMODAL_VISIBILITY:
      return {
        ...state,
        contactsModalVisibility: !state.contactsModalVisibility,
      };
    case CONTACT_LIST:
      return {...state, contactList: action.data};
    case CALL_STATE:
      return {...state, callState: action.data};
    case MODAL_STRUCTURE:
      return {...state, modalStructure: action.data};
    case COUNTRIES:
      return {...state, countries: action.data};
    case PACKAGES:
      return {...state, callingPackages: action.data};
    case COUPONS:
      return {...state, coupons: action.data};
    case PLATFORM:
      return {...state, platform: action.data};
    case CONFIGURATION:
      return {...state, configuration: action.data};
    case CALL_STATUS:
      return {...state, callStatus: action.data};
    case ACCOUNT_DETAIL:
      return {...state, accountDetail: action.data};
    case CALL:
      return {...state, call: action.data};
    case CALL_START_TIME:
      return {...state, callStartTime: action.data};
    case CALL_END_TIME:
      return {...state, callEndTime: action.data};
    case CALL_DATA:
      return {...state, callData: action.data};
    case NUMBER_HISTORY:
      return {...state, numberHistory: action.data};
    case QUESTIONS:
      return {...state, questions: action.data};
    case MESSAGES:
      return {...state, messages: action.data};
    case LOG_OUT:
      return {...initialState};
    case CALL_TERMINATION:
      callEnd({
        call_id: 1,
        package_id: 1,
        call_start: '2021-03-08 6:40:24',
        call_end: '2021-03-08 6:43:24',
        duration: 3,
        mobile_number: 922222222222,
      })
        .then((res) => console.log(res))
        .catch((err) => console.log('Call end: ', err));
      return {...state};
    default:
      return state;
  }
};
export default appReducer;
