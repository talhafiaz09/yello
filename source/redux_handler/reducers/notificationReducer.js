import {ADD_NOTIFICATION, UPDATE_NOTIFICATION, LOG_OUT} from '../actions/types';
const initialState = {
  notifications: [],
};
const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NOTIFICATION:
      return {...state, notifications: [action.data, ...state.notifications]};
    case UPDATE_NOTIFICATION:
      return {...state, notifications: action.data};
    case LOG_OUT:
      return {...initialState};
    default:
      return state;
  }
};
export default notificationReducer;
