import {USER_INFORMATION, LOG_OUT, SET_CURRENCY_RATE} from '../actions/types';
const initialState = {
  userInformation: {
    countryCode: 'US',
    dialCode: 1,
    countryId: 224,
    phoneNumber: '',
    balance: 0,
    rewardpoints: 0,
    packages: [],
    coupons: [],
    device_token: '67',
    currency: '',
  },
  currency_rates: {},
};
const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_INFORMATION:
      return {...state, userInformation: action.data};
    case SET_CURRENCY_RATE:
      return {...state, currency_rates: action.data};
    case LOG_OUT:
      return {...initialState, currency_rates: state.currency_rates};
    default:
      return state;
  }
};
export default userReducer;
