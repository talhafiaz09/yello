import AsyncStorage from '@react-native-async-storage/async-storage';
import {createStore, combineReducers} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import appReducer from '../reducers/appReducer';
import userReducer from '../reducers/userReducer';
import callReducer from '../reducers/callReducer';
import notificationReducer from '../reducers/notificationReducer';
import appthemeReducer from '../reducers/appthemeReducer';
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: [
    'appReducer',
    'userReducer',
    'callReducer',
    'notificationReducer',
  ],
  whitelist: ['appthemeReducer'],
};
const rootReducer = combineReducers({
  appReducer: appReducer,
  userReducer: userReducer,
  callReducer: callReducer,
  notificationReducer: notificationReducer,
  appthemeReducer: appthemeReducer,
});
const persistedReducer = persistReducer(persistConfig, rootReducer);
const configureStore = createStore(persistedReducer);
const persistor = persistStore(configureStore);
export default () => {
  return {configureStore, persistor};
};
