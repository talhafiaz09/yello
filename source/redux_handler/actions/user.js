import {USER_INFORMATION, LOG_OUT, SET_CURRENCY_RATE} from './types';
export const setuserInformation = (user) => ({
  type: USER_INFORMATION,
  data: user,
});
export const setCurrencyRate = (data) => ({
  type: SET_CURRENCY_RATE,
  data: data,
});
export const logoutUser = () => ({
  type: LOG_OUT,
});
