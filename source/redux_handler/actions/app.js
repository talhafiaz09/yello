import {
  STATUS_BAR_COLOR,
  COUNTRYMODAL_VISIBILITY,
  RESENDOTP_VISIBILITY,
  SET_CALLING_RATES,
  SEARCH_BAR,
  DIALTO_INFORMATION,
  DIALED_NUMBER,
  DELETE_DIALED_NUMBER,
  CLEAR_DIALED_NUMBER,
  CONTACTSMODAL_VISIBILITY,
  CONTACT_LIST,
  CALL_STATE,
  MODAL_STRUCTURE,
  COUNTRIES,
  PACKAGES,
  COUPONS,
  PLATFORM,
  CONFIGURATION,
  CALL_STATUS,
  ACCOUNT_DETAIL,
  CALL,
  CALL_START_TIME,
  CALL_END_TIME,
  CALL_DATA,
  CALL_TERMINATION,
  LOG_OUT,
  NUMBER_HISTORY,
  SIGNUP_COUNTRIES,
  QUESTIONS,
  MESSAGES,
  CURSOR,
} from './types';
export const changeStatusBarColor = (color) => ({
  type: STATUS_BAR_COLOR,
  data: color,
});
export const changeCountryModalVisibility = () => ({
  type: COUNTRYMODAL_VISIBILITY,
});
export const changeResendOtpVisibility = () => ({
  type: RESENDOTP_VISIBILITY,
});
export const setCallingRates = (callingRates) => ({
  type: SET_CALLING_RATES,
  data: callingRates,
});
export const setSearchBarText = (text) => ({
  type: SEARCH_BAR,
  data: text,
});
export const setDialtoInformation = (dialTo) => ({
  type: DIALTO_INFORMATION,
  data: dialTo,
});
export const setDialedNumber = (number) => ({
  type: DIALED_NUMBER,
  data: number,
});
export const deleteDialedNumber = () => ({
  type: DELETE_DIALED_NUMBER,
});
export const clearDialedNumber = () => ({
  type: CLEAR_DIALED_NUMBER,
});
export const changeContactsModalVisibility = () => ({
  type: CONTACTSMODAL_VISIBILITY,
});
export const setContactList = (contactList) => ({
  type: CONTACT_LIST,
  data: contactList,
});
export const setCallState = (callState) => ({
  type: CALL_STATE,
  data: callState,
});
export const setModalStructure = (structure) => ({
  type: MODAL_STRUCTURE,
  data: structure,
});
export const setCountries = (countries) => ({
  type: COUNTRIES,
  data: countries,
});
export const setCallingPackages = (packages) => ({
  type: PACKAGES,
  data: packages,
});
export const setCoupons = (coupons) => ({
  type: COUPONS,
  data: coupons,
});
export const setPlatform = (platform) => ({
  type: PLATFORM,
  data: platform,
});
export const setConfiguration = (configuration) => ({
  type: CONFIGURATION,
  data: configuration,
});
export const setCallStatus = (status) => ({
  type: CALL_STATUS,
  data: status,
});
export const setAccountDetail = (detail) => ({
  type: ACCOUNT_DETAIL,
  data: detail,
});
export const setCall = (call) => ({
  type: CALL,
  data: call,
});
export const setCallStartTime = (time) => ({
  type: CALL_START_TIME,
  data: time,
});
export const setCallEndTime = (time) => ({
  type: CALL_END_TIME,
  data: time,
});
export const setNumberHistory = (number) => ({
  type: NUMBER_HISTORY,
  data: number,
});
export const setCallData = (data) => ({
  type: CALL_DATA,
  data: data,
});
export const setCallTermination = () => ({
  type: CALL_TERMINATION,
});
export const logout = () => ({
  type: LOG_OUT,
});
export const signupCountries = (data) => ({
  type: SIGNUP_COUNTRIES,
  data: data,
});
export const setQuestions = (data) => ({
  type: QUESTIONS,
  data: data,
});
export const setMessages = (data) => ({
  type: MESSAGES,
  data: data,
});
export const setCursor = (data) => ({
  type: CURSOR,
  data: data,
});
