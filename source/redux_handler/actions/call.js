import {CALL_START_TIME, CALL_END_TIME, CLEAR} from './types';
export const setCallStartTime = (time) => ({
  type: CALL_START_TIME,
  data: time,
});
export const setCallEndTime = (time) => ({
  type: CALL_END_TIME,
  data: time,
});
export const clear = () => ({
  type: CLEAR,
});
