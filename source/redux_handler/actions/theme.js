import {APP_THEME} from './types';
export const setTheme = (theme) => ({
  type: APP_THEME,
  data: theme,
});
