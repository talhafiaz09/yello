import {ADD_NOTIFICATION, UPDATE_NOTIFICATION, LOG_OUT} from './types';
export const addNotification = (notification) => ({
  type: ADD_NOTIFICATION,
  data: notification,
});
export const updateNotification = (notifications) => ({
  type: UPDATE_NOTIFICATION,
  data: notifications,
});
export const logout = () => ({
  type: LOG_OUT,
});
