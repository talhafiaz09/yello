import axiosMain from 'axios';
import ENDPOINTS from './endpoints';
const validator = (status) => {
  return status == 403 || status == 200;
};
const axios = axiosMain.create({
  baseURL: ENDPOINTS.BASEURL,
  validateStatus: validator(),
});
export const newCustomer = (payload) =>
  axios.post(ENDPOINTS.NEW_CUSTOMER, payload);
export const getCountries = () => axios.get(ENDPOINTS.COUNTRIES);
export const getSplashData = () => axios.get(ENDPOINTS.GET_SPLASH_DATA);
export const getCountriesSignup = () => axios.get(ENDPOINTS.SIGNUP_CONTRIES);
export const getCallingRates = () => axios.get(ENDPOINTS.CALLING_RATES);
export const getPackages = () => axios.get(ENDPOINTS.PACKAGES);
export const getQuestions = () => axios.get(ENDPOINTS.GET_QUESTION_ANSWER);
export const getCoupons = (payload) => axios.post(ENDPOINTS.COUPONS, payload);
export const newTopup = (payload) => axios.post(ENDPOINTS.TOPUP, payload);
export const callInitiate = (payload) =>
  axios.post(ENDPOINTS.CALL_INITIATE, payload);
export const callEnd = (payload) => axios.post(ENDPOINTS.CALL_END, payload);
// export const callEnd = () => console.log(callEndTime());
export const home = (payload) => axios.post(ENDPOINTS.HOME, payload);
export const purchaseCoupon = (payload) =>
  axios.post(ENDPOINTS.PURCHASE_COUPON, payload);
export const getCallHistory = (payload) =>
  axios.post(ENDPOINTS.CALL_HISTORY, {
    ...payload,
    from_date: null,
    to_date: null,
  });
export const getTransactionHistory = (payload) =>
  axios.post(ENDPOINTS.TRANSACTION_HISTORY, {
    ...payload,
    from_date: null,
    to_date: null,
  });
export const packageBuy = (payload) =>
  axios.post(ENDPOINTS.PACKAGE_BUY, {
    ...payload,
    type: 2,
    amount: null,
    transfer_to: null,
    account_number: null,
    cvc: null,
    expiry: null,
  });
export const shareCredit = (payload) =>
  axios.post(ENDPOINTS.SHARE_CREDIT, {
    ...payload,
    type: 3,
    package_id: null,
    account_number: null,
    cvc: null,
    expiry: null,
  });
export const refundRequestApi = (payload) =>
  axios.post(ENDPOINTS.REFUND_REQUEST, payload);
export const addReferralsApi = (payload) =>
  axios.post(ENDPOINTS.ADD_REFEREL, payload);
export const getReferralsApi = (payload) =>
  axios.post(ENDPOINTS.GET_REFERELS, payload);
