import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
    },
    top_container: {
      height: hp('30%'),
      backgroundColor: COLORS.color_top,
      width: '100%',
    },
    svg_view: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    half_C: {
      position: 'absolute',
      top: '7%',
      left: '30%',
      zIndex: 10,
    },
    half_G: {},
    text_container: {position: 'absolute', bottom: '20%', left: '6%'},
    title: {
      color: COLORS.color_white,
      fontFamily: 'Poppins-Bold',
      fontSize: hp('2.5%'),
    },
    sub_title: {
      color: COLORS.color_white,
      fontFamily: 'Poppins-Light',
      fontSize: hp('1.5%'),
    },
    redT_container: {position: 'absolute', bottom: 0, right: 0},
    bottom_container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingLeft: 20,
      paddingRight: 20,
    },
    text_field: {
      height: hp('6%'),
      width: '100%',
      backgroundColor: COLORS.color_textfield,
      marginBottom: 30,
      borderRadius: 15,
      flexDirection: 'row',
      paddingLeft: 10,
      paddingRight: 10,
    },
    logo_container: {
      position: 'absolute',
      top: 0,
      alignSelf: 'center',
      marginTop: 20,
    },
    bottom_container_text: {
      marginTop: 30,
      color: COLORS.color_text,
      fontSize: 10,
      fontFamily: 'Poppins-Regular',
      textAlign: 'center',
    },
    codepicker_container: {
      flexDirection: 'row',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'space-between',
      minWidth: '25%',
    },
    btn_container: {height: hp('6%'), width: '100%'},
    divider: {
      borderColor: COLORS.color_bottom,
      borderWidth: 1,
      position: 'absolute',
      height: '100%',
      left: '32%',
    },
    label_style: {fontSize: hp('2%')},
    dial_number_text: {
      color: COLORS.color_text,
      fontSize: 14,
      fontFamily: 'Poppins-Regular',
    },
    icon_down_background: {
      backgroundColor: COLORS.color_yellow,
      borderRadius: 15,
      height: 20,
      width: 20,
      justifyContent: 'center',
      alignItems: 'center',
    },
    text_field_container: {marginLeft: 15, flex: 1},
    input_field: {
      flex: 1,
      color: COLORS.color_text,
    },
    bottom_container_highlighted_text: {
      color: COLORS.color_yellow,
      fontSize: 10,
      fontFamily: 'Poppins-Regular',
      textAlign: 'center',
      textDecorationLine: 'underline',
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
