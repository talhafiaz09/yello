import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import Flag from 'react-native-flags';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Entypo from 'react-native-vector-icons/Entypo';
import useStyles from './useStyles';
import CountryModal from '../../components/countrymodal/CountryModal';
import {useDispatch, useSelector} from 'react-redux';
import {changeCountryModalVisibility} from '../../redux_handler/actions/app';
import {setuserInformation} from '../../redux_handler/actions/user';
import Button from '../../components/button/Button';
import Logo from '../../assets/images/logo/Logo';
import TopContainer from '../../components/top_container/TopContainer';
import {changeResendOtpVisibility} from '../../redux_handler/actions/app';
const Register = ({
  handleScroll,
  signInWithPhoneNumber,
  showLoading,
  resetModal,
  showAlert,
}) => {
  const dispatch = useDispatch();
  const setuser_Information = (user) => dispatch(setuserInformation(user));
  const changeCountryModal_Visibility = () =>
    dispatch(changeCountryModalVisibility());
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  const changeResendOtp_Visibility = () =>
    dispatch(changeResendOtpVisibility());

  const {Styles, COLORS} = useStyles();

  return (
    <View style={Styles.body}>
      <CountryModal userInfo signupCountry />
      <TopContainer onPress={() => handleScroll(0)}>
        <View style={Styles.text_container}>
          <Text style={Styles.title}>ENTER YOUR NUMBER</Text>
          <Text style={Styles.sub_title}>
            We will send you the confirmation code{'\n'}to this number
          </Text>
        </View>
      </TopContainer>
      <KeyboardAvoidingView
        style={Styles.bottom_container}
        behavior={Platform.OS === 'android' ? 'height' : 'padding'}>
        <View style={Styles.logo_container}>
          <Logo height={hp('15%')} width={hp('15%') * 1.13709} opacity={0.26} />
        </View>
        <View width={'100%'} style={{marginTop: 50}}>
          <View style={Styles.text_field}>
            <TouchableOpacity
              style={Styles.codepicker_container}
              onPress={() => {
                changeCountryModal_Visibility();
              }}>
              <Flag code={userInformation.countryCode} size={32} />
              <Text style={Styles.dial_number_text}>
                +{userInformation.dialCode}
              </Text>
              <View style={Styles.icon_down_background}>
                <Entypo
                  name="chevron-down"
                  size={18}
                  color={COLORS.color_textfield}
                />
              </View>
            </TouchableOpacity>
            <View style={Styles.divider} />
            <View style={Styles.text_field_container}>
              <TextInput
                style={Styles.input_field}
                placeholder={'(000)-0000-0000'}
                keyboardType={'number-pad'}
                value={userInformation.phoneNumber}
                placeholderTextColor={COLORS.color_text}
                onChangeText={(text) => {
                  setuser_Information({
                    ...userInformation,
                    phoneNumber: text.replace(/^0+/, ''),
                  });
                }}
              />
            </View>
          </View>
          <Button
            text={'Verify My Number'}
            type={'contained'}
            contentStyle={Styles.btn_container}
            labelStyle={Styles.label_style}
            onPress={() =>
              showLoading('Please wait...').then(() =>
                signInWithPhoneNumber(
                  '+' + userInformation.dialCode + userInformation.phoneNumber,
                )
                  .then(() =>
                    resetModal().then(() => {
                      setTimeout(() => {
                        changeResendOtp_Visibility();
                      }, 5000);
                      handleScroll(2);
                    }),
                  )
                  .catch((error) => showAlert(error.message)),
              )
            }
            disabled={
              userInformation.phoneNumber == '' ||
              userInformation.phoneNumber.length +
                userInformation.dialCode.toString().length <
                10
                ? true
                : false
            }
          />
        </View>
        <Text style={Styles.bottom_container_text}>
          By tapping this button you agree to our{' '}
          <Text style={Styles.bottom_container_highlighted_text}>
            Terms and{'\n'}Conditions
          </Text>{' '}
          and{' '}
          <Text style={Styles.bottom_container_highlighted_text}>
            Privacy Policy
          </Text>
          .
        </Text>
      </KeyboardAvoidingView>
    </View>
  );
};
export default Register;
