import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
    },
    top_container: {
      backgroundColor: COLORS.color_textfield,
      height: hp('30%'),
      justifyContent: 'center',
      alignItems: 'center',
    },
    main_c: {
      padding: 10,
    },
    row: {
      flexDirection: 'row',
      backgroundColor: COLORS.color_textfield,
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 14,
      borderRadius: 10,
      marginTop: 10,
    },
    text_row: {
      color: COLORS.color_text,
      fontFamily: 'Poppins-Regular',
      fontSize: 14,
    },
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
