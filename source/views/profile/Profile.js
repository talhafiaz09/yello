import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {FAB} from 'react-native-paper';
import Header from '../../components/header/Header';
import ImagePicker from 'react-native-image-crop-picker';
import useStyles from './useStyles';
import {useSelector, useDispatch} from 'react-redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Switch} from 'react-native-paper';
import {setTheme} from '../../redux_handler/actions/theme';
import {BlackBG, YellowBG} from '../../assets/images/svg';
import LogoText from '../../assets/images/logo/LogoText';

const Profile = ({navigation}) => {
  const dispatch = useDispatch();

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  const dark = useSelector((state) => state.appthemeReducer.dark);

  const {Styles, COLORS, hp} = useStyles();

  const ASPECT_RATIO = hp('15%');

  return (
    <View style={Styles.body}>
      <Header menu navigation={navigation} title={'Profile'} notification />
      <View style={Styles.top_container}>
        {dark ? (
          <BlackBG height={ASPECT_RATIO} width={ASPECT_RATIO * 2.44} />
        ) : (
          <YellowBG height={ASPECT_RATIO} width={ASPECT_RATIO * 2.44} />
        )}
        <View style={{position: 'absolute'}}>
          <LogoText width={72} height={65} />
        </View>
      </View>
      <View style={Styles.main_c}>
        <View style={Styles.row}>
          <Text style={Styles.text_row}>Mobile No</Text>
          <Text style={Styles.text_row}>+{userInformation.phoneNumber}</Text>
        </View>
      </View>
    </View>
  );
};
export default Profile;
