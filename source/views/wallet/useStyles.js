import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
    },
    top_container: {
      paddingTop: 10,
      backgroundColor: COLORS.color_textfield,
      minHeight: hp('30%'),
      justifyContent: 'center',
      alignContent: 'center',
    },
    titleStyle: {fontFamily: 'Poppins-Bold', fontSize: 14},
    creditNumber: {
      fontFamily: 'Poppins-Bold',
      fontSize: 30,
    },
    mainContainer: {
      alignItems: 'center',
    },
    creditContainer: {
      justifyContent: 'center',
    },
    button_background: {
      width: '60%',
      marginTop: 10,
      marginBottom: 10,
      alignSelf: 'center',
    },
    btn: {height: hp('6%')},
    labelStyle: {fontSize: 14, fontFamily: 'Poppins-Regular'},
    bottom_container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
