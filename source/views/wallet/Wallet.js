import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, FlatList} from 'react-native';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
import Header from '../../components/header/Header';
import BalanceContainer from '../../components/balance_container/BalanceContainer';
import Button from '../../components/button/Button';
import {WalletBG} from '../../assets/images/svg';

const Wallet = ({navigation}) => {
  const {Styles, COLORS} = useStyles();

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  const currency_rates = useSelector(
    (state) => state.userReducer.currency_rates,
  );

  return (
    <View style={Styles.body}>
      <Header menu navigation={navigation} title={'Home'} notification />
      <View style={Styles.top_container}>
        <BalanceContainer
          symbol={currency_rates[userInformation.currency]?.symbol}
          text={
            userInformation.balance *
            currency_rates[userInformation.currency]?.rate
          }
          title={'My Wallet Balance'}
          titleStyle={Styles.titleStyle}
          creditNumber={Styles.creditNumber}
          mainContainer={Styles.mainContainer}
          creditContainer={Styles.creditContainer}
        />
        <Button
          text={'Add Credit'}
          type={'contained'}
          onPress={() => navigation.navigate('AddCredit')}
          contentStyle={Styles.btn}
          style={Styles.button_background}
          labelStyle={Styles.labelStyle}
        />
        <BalanceContainer
          name={'award'}
          text={userInformation.rewardpoints}
          title={'Reward points'}
          creditNumber={Styles.creditNumber}
          size={28}
          mainContainer={Styles.mainContainer}
          creditContainer={Styles.creditContainer}
        />
      </View>
      <View style={Styles.bottom_container}>
        <WalletBG />
      </View>
    </View>
  );
};
export default Wallet;
