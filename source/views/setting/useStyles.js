import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
    },
    main_c: {
      padding: 20,
    },
    row: {
      flexDirection: 'row',
      backgroundColor: COLORS.color_textfield,
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 10,
      borderRadius: 10,
    },
    text_row: {
      color: COLORS.color_text,
      fontFamily: 'Poppins-Regular',
      fontSize: 14,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
