import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {FAB} from 'react-native-paper';
import Header from '../../components/header/Header';
import useStyles from './useStyles';
import {useSelector, useDispatch} from 'react-redux';
import {Switch} from 'react-native-paper';
import {setTheme} from '../../redux_handler/actions/theme';

const Setting = ({navigation}) => {
  const dispatch = useDispatch();
  const setTheme_ = (theme) => dispatch(setTheme(theme));

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  const dark = useSelector((state) => state.appthemeReducer.dark);

  const {Styles, COLORS} = useStyles();

  return (
    <View style={Styles.body}>
      <Header menu navigation={navigation} title={'Setting'} notification />
      <View style={Styles.main_c}>
        <View style={Styles.row}>
          <Text style={Styles.text_row}>Dark theme</Text>
          <Switch
            value={dark}
            onValueChange={(val) =>
              val ? setTheme_('COLORS_DARK') : setTheme_('COLORS_LIGHT')
            }
            style={{transform: [{scaleX: 0.7}, {scaleY: 0.7}]}}
            color={COLORS.color_yellow}
          />
        </View>
      </View>
    </View>
  );
};
export default Setting;
