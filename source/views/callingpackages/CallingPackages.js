import React, {useEffect, useState} from 'react';
import {View, FlatList, Text} from 'react-native';
import useStyles from './useStyles';
import {FAB} from 'react-native-paper';
import SearchBar from '../../components/searchbar/SearchBar';
import ConfirmationModal from '../../components/confirmationmodal/ConfirmationModal';
import Header from '../../components/header/Header';
import useModal from '../../handlers/useModal';
import {useDispatch, useSelector} from 'react-redux';
import GlobalPackageCard from '../../components/globalpackagecard/GlobalPackageCard';
import {getPackages} from '../../redux_handler/api/index';
import {setCallingPackages} from '../../redux_handler/actions/app';
import {packageBuy} from '../../redux_handler/api/index';
const CallingPackages = ({navigation}) => {
  const {Styles} = useStyles();

  const [loading, setLoading] = useState(false);

  const {showAlert, modalStructure, resetModal, showConfirm, showLoading} =
    useModal();

  const dispatch = useDispatch();

  const setCalling_Packages = async (packages) =>
    dispatch(setCallingPackages(packages));

  const onRefresh = async () => {
    setLoading(true);
    await getPackages()
      .then((res) => {
        setCalling_Packages(res.data.data);
        setLoading(false);
      })
      .catch((error) => {
        console.error('Packages Error:', error);
      });
  };

  const callingPackages = useSelector(
    (state) => state.appReducer.callingPackages,
  );
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const searchBarText = useSelector((state) => state.appReducer.searchBarText);

  const mapCallingRates = ({item, key}) => {
    return (
      <GlobalPackageCard
        key={key}
        data={item}
        showConfirm={showConfirm}
        userInformation={userInformation}
      />
    );
  };

  const buyPackageHandler = async () => {
    showLoading('Please wait...').then(() =>
      packageBuy({
        mobile_number: userInformation.phoneNumber,
        package_id: modalStructure.data.id,
      })
        .then((res) =>
          res.data.status_code == 200
            ? showAlert('Transaction successful.')
            : showAlert(`${res.data.message}\nTransaction not successful.`),
        )
        .catch((error) =>
          showAlert(`${error.message}\nTransaction not successful.`),
        ),
    );
  };

  return (
    <View style={Styles.body}>
      <ConfirmationModal
        modalStructure={modalStructure}
        pressedNo={resetModal}
        pressedYes={() => buyPackageHandler()}
        pressedOk={resetModal}
      />
      <Header
        menu
        navigation={navigation}
        title={'Calling Packages'}
        notification
      />
      {/* <FAB
        style={Styles.fab}
        icon="dialpad"
        color={Colors.color_primary}
        onPress={() => navigation.navigate('Call')}
      /> */}
      <View style={{flex: 1, padding: 10}}>
        <View style={Styles.searchbar_container}>
          <SearchBar placeholder={'Search by country name'} />
        </View>
        <FlatList
          onRefresh={() => onRefresh()}
          refreshing={loading}
          data={
            searchBarText == ''
              ? callingPackages
              : callingPackages?.filter((index) => {
                  return index.country_name
                    .toUpperCase()
                    .replace(/\./g, '')
                    .includes(searchBarText.toUpperCase().replace(/\./g, ''));
                })
          }
          renderItem={mapCallingRates}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{flexGrow: 1}}
          styles={Styles.callingrate_flatlist_container}
          ListEmptyComponent={() => (
            <View style={Styles.NTS_c}>
              <Text style={Styles.NTS_text}>Nothing to show</Text>
            </View>
          )}
        />
      </View>
    </View>
  );
};
export default CallingPackages;
