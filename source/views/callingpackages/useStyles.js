import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
    },
    searchbar_container: {},
    custom_header_style: {
      elevation: 0,
    },
    NTS_text: {
      fontFamily: 'Poppins-Italic',
      color: COLORS.color_text,
    },
    NTS_c: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
    },
    callingrate_flatlist_container: {width: '100%'},
    fab: {
      position: 'absolute',
      // backgroundColor: Colors.color_white,
      right: 25,
      bottom: 30,
      zIndex: 200,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
