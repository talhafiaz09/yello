import React, {useEffect, useState} from 'react';
import {View, Text, FlatList} from 'react-native';
import useStyles from './useStyles';
import {FAB} from 'react-native-paper';
import Header from '../../components/header/Header';
import UserCoupons from '../../components/usercoupons/UserCoupons';
import RewardpointsIcon from '../../assets/images/rewardpointsicon/RewardpointsIcon';
import {getCoupons} from '../../redux_handler/api/index';
import {useDispatch, useSelector} from 'react-redux';
import {setuserInformation} from '../../redux_handler/actions/user';
import useAsyncStorage from '../../handlers/useAsyncStorage';
import Loader from '../../components/loader';

const RewardPoints = ({navigation}) => {
  const dispatch = useDispatch();

  const setuser_Information = async (info) =>
    dispatch(setuserInformation(info));

  const [loading, setLoading] = useState(true);

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  const {setUserInformationAsyncStorage} = useAsyncStorage();
  const {Styles, COLORS, hp} = useStyles();

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getCoupons({
        mobile_number: parseInt(userInformation.phoneNumber),
      })
        .then((res) =>
          setuser_Information({
            ...userInformation,
            coupons: res.data.data,
          }).then(() => {
            setUserInformationAsyncStorage(userInformation);
            setLoading(false);
          }),
        )
        .catch((error) => console.log('Coupons error: ', error));
    });
    return unsubscribe;
  }, [navigation]);

  const mapObtainedCoupons = ({item, key}) => {
    return <UserCoupons key={key} data={item} />;
  };

  const ASPECT_RATIO = hp('40%');

  return (
    <View style={Styles.body}>
      <Header
        menu
        navigation={navigation}
        title={'Reward Points'}
        notification
      />
      {/* <FAB
        style={Styles.fab}
        icon="dialpad"
        color={COLORS.color_primary}
        onPress={() => navigation.navigate('Call')}
      /> */}
      <View flex={1} style={{paddingTop: 20, flexDirection: 'column'}}>
        <View style={Styles.icon_container} flex={1}>
          <RewardpointsIcon
            points={userInformation.rewardpoints}
            height={ASPECT_RATIO}
            width={ASPECT_RATIO}
          />
        </View>
        <View style={Styles.content} flex={1}>
          <Text style={Styles.available_balance_text}>Consumed Coupons</Text>
          {userInformation.coupons.length === 0 ? (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {loading ? (
                <Loader />
              ) : (
                <Text
                  style={{
                    fontFamily: 'Poppins-LightItalic',
                    fontSize: 12,
                    color: COLORS.color_text,
                  }}>
                  No coupons brought to display.
                </Text>
              )}
            </View>
          ) : (
            <FlatList
              data={userInformation.coupons}
              renderItem={mapObtainedCoupons}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
            />
          )}
        </View>
      </View>
    </View>
  );
};
export default RewardPoints;
