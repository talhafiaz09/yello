import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
    },
    fab: {
      position: 'absolute',
      backgroundColor: COLORS.color_white,
      right: 25,
      bottom: 30,
      zIndex: 200,
    },
    content: {padding: 20},
    available_balance_text: {
      fontFamily: 'Poppins-SemiBold',
      fontSize: 14,
      color: COLORS.color_text,
    },
    credit_display_container: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: 20,
      justifyContent: 'center',
      marginTop: 20,
    },
    icon_container: {alignItems: 'center', justifyContent: 'center'},
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
