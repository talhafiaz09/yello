import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_primary,
    },
    logo_container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    logo_text_container: {
      flexDirection: 'row',
      marginTop: 6,
    },
    welcome_text: {
      fontSize: 30,
      color: COLORS.color_white,
      fontFamily: 'Poppins-Regular',
    },
    yello_text: {
      fontSize: 30,
      color: COLORS.color_white,
      fontFamily: 'Poppins-ExtraBoldItalic',
    },
    subtitle: {
      fontSize: 10,
      color: COLORS.color_white,
      fontFamily: 'Poppins-ExtraLightItalic',
      marginTop: 6,
    },
    bottom_container: {
      position: 'absolute',
      width: '90%',
      bottom: hp('14%'),
      alignSelf: 'center',
    },
    btn_container: {height: hp('6%')},
    label_style: {fontSize: hp('2%')},
  });
  return {
    Styles,
  };
};
export default useStyles;
