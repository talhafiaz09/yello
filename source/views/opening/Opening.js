import React from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import Logo from '../../assets/images/logo/Logo';
import Button from '../../components/button/Button';
import useStyles from './useStyles';
const Opening = ({handleScroll}) => {
  const {Styles} = useStyles();

  return (
    <SafeAreaView style={Styles.body}>
      <View style={Styles.logo_container}>
        <Logo />
        <View style={Styles.logo_text_container}>
          <Text style={Styles.welcome_text}>Welcome to </Text>
          <Text style={Styles.yello_text}>yello</Text>
        </View>
        <Text style={Styles.subtitle}>distance is no barrier</Text>
      </View>
      <View style={Styles.bottom_container}>
        <Button
          text={"Let's Start"}
          type={'contained'}
          contentStyle={Styles.btn_container}
          labelStyle={Styles.label_style}
          onPress={() => handleScroll(1)}
        />
      </View>
    </SafeAreaView>
  );
};
export default Opening;
