import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import {FAB} from 'react-native-paper';
import Header from '../../components/header/Header';
import ImagePicker from 'react-native-image-crop-picker';
import useStyles from './useStyles';
import Button from '../../components/button/Button';
import {useSelector} from 'react-redux';
import useModal from '../../handlers/useModal';
import ConfirmationModal from '../../components/confirmationmodal/ConfirmationModal';
import {refundRequestApi} from '../../redux_handler/api/index';
import Loader from '../../components/loader';

const Refund = ({navigation}) => {
  const {Styles, COLORS} = useStyles();

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const [url, setUrl] = useState(null);
  const [base64, setBase64] = useState(null);
  const [email, setEmail] = useState('');
  const [amount, setAmount] = useState('');

  const {modalStructure, showLoading, resetModal, showAlert, showConfirm} =
    useModal();

  const onConfirm = () => {
    const data = {
      mobile_number: userInformation.phoneNumber,
      amount: parseFloat(amount),
      email: email,
      image: base64,
    };
    showLoading(`Please wait...`).then(() => {
      refundRequestApi(data)
        .then((res) =>
          res.data.status_code == 200
            ? showAlert(res.data.message)
            : showAlert(res.data.message),
        )
        .catch((error) => showAlert(`Refund request not successful.`));
    });
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={Styles.body}>
        <Header menu navigation={navigation} notification title={'Refund'} />
        {/* <FAB
          style={Styles.fab}
          icon="dialpad"
          color={COLORS.color_primary}
          onPress={() => navigation.navigate('Call')}
        /> */}
        <ConfirmationModal
          modalStructure={modalStructure}
          pressedOk={resetModal}
          pressedNo={resetModal}
          pressedYes={() => onConfirm()}
        />
        <View style={Styles.main_container}>
          <View style={Styles.text_container}>
            <Text style={Styles.ed_text}>Enter deatils:</Text>
          </View>
          <View style={Styles.text_field_main_container}>
            <TextInput
              style={Styles.text_field}
              value={email}
              onChangeText={(text) => setEmail(text)}
              placeholder={'Enter email'}
              placeholderTextColor={COLORS.color_text}
            />
          </View>
          <View style={Styles.text_field_main_container}>
            <TextInput
              editable={false}
              value={userInformation.phoneNumber}
              style={Styles.text_field}
              placeholder={'Enter number'}
              keyboardType={'number-pad'}
              placeholderTextColor={COLORS.color_text}
            />
          </View>
          <View style={Styles.text_field_main_container}>
            <TextInput
              value={amount}
              onChangeText={(text) => setAmount(text)}
              style={Styles.text_field}
              placeholder={'Enter amount'}
              keyboardType={'number-pad'}
              placeholderTextColor={COLORS.color_text}
            />
          </View>
          <TouchableOpacity
            style={{
              flexWrap: 'wrap',
              justifyContent: 'center',
              alignSelf: 'center',
            }}
            onPress={() => {
              ImagePicker.openPicker({
                width: 300,
                height: 400,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo',
              })
                .then((image) => {
                  setBase64('data:' + image.mime + ';base64,' + image.data);
                  setUrl(image.path);
                })
                .catch((error) => {
                  console.log(error);
                });
            }}>
            <View style={Styles.image_container}>
              <FAB
                style={Styles.camera_fab}
                disabled
                large
                icon="camera-outline"
                color={COLORS.color_primary}
              />
              <Image
                style={Styles.image}
                source={{
                  uri: url,
                }}
              />
            </View>
          </TouchableOpacity>
          <Text style={Styles.sub_text}>
            * please attach valid screenshot{'\n'}from transaction history.
          </Text>
          <View style={Styles.add_credit_button}>
            <Button
              text={'Refund Request'}
              type={'contained'}
              contentStyle={Styles.contentStyle}
              labelStyle={Styles.labelStyle}
              disabled={
                email.length == 0 ||
                base64 == null ||
                amount.length == 0 ||
                parseFloat(amount) == 0
                  ? true
                  : false
              }
              onPress={() =>
                !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/.test(email)
                  ? showAlert(`Enter valid email.`)
                  : showConfirm(`Do you want to make fund request?`)
              }
            />
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};
export default Refund;
