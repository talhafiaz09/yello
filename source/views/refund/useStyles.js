import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
    },
    custom_header_style: {
      elevation: 0,
    },
    credit_display_container: {
      width: '100%',
    },
    camera_fab: {
      position: 'absolute',
      zIndex: 200,
      backgroundColor: COLORS.color_white,
    },
    image: {
      width: wp('50%'),
      height: wp('50%'),
      borderRadius: 20,
    },
    sub_text: {
      fontFamily: 'Poppins-SemiBold',
      color: COLORS.color_text,
      alignSelf: 'center',
      fontSize: 10,
      textAlign: 'center',
      marginBottom: 10,
      lineHeight: 14,
      marginTop: 10,
    },
    add_credit_button: {
      width: '50%',
      alignSelf: 'center',
      marginTop: 10,
    },
    image_container: {
      borderRadius: 20,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      flexWrap: 'wrap',
      padding: 2,
      borderWidth: 2,
      marginTop: 20,
      borderColor: COLORS.color_white,
      shadowColor: COLORS.color_primary,
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.2,
      elevation: 5,
    },
    main_container: {flex: 1, paddingTop: 20},
    text_container: {
      width: wp('70%'),
      alignSelf: 'center',
    },
    ed_text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 14,
      color: COLORS.color_text,
    },
    text_field_main_container: {
      marginTop: hp('2%'),
      width: wp('70%'),
      height: hp('6%'),
      alignItems: 'center',
      flexDirection: 'row',
      borderRadius: 10,
      alignSelf: 'center',
      backgroundColor: COLORS.color_textfield,
      padding: 2,
    },
    text_field: {
      paddingTop: 0,
      paddingBottom: 0,
      margin: 0,
      paddingLeft: 5,
      paddingRight: 5,
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
      width: '100%',
      height: '100%',
    },
    fab: {
      position: 'absolute',
      backgroundColor: COLORS.color_white,
      right: 25,
      bottom: 30,
      zIndex: 200,
    },
    contentStyle: {
      height: hp('6%'),
    },
    labelStyle: {
      fontSize: hp('2%'),
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
