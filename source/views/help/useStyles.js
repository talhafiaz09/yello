import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    fab: {
      position: 'absolute',
      backgroundColor: COLORS.color_white,
      right: 25,
      bottom: 30,
      zIndex: 200,
    },
    body: {flex: 1, backgroundColor: COLORS.color_bottom},
    question: {
      fontFamily: 'Poppins-SemiBold',
      fontSize: 14,
      color: COLORS.color_yellow,
      marginBottom: 5,
    },
    answer: {
      fontFamily: 'Poppins-Regular',
      fontSize: 12,
      color: COLORS.color_text,
      lineHeight: 16,
    },
    question_container: {
      marginLeft: 5,
      paddingLeft: 10,
      borderLeftWidth: 1,
      borderLeftColor: COLORS.color_yellow,
    },
    NTS_container: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
    },
    NTS_text: {
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
    },
    VOB_text: {color: COLORS.color_text, marginTop: 5},
    url: {
      fontFamily: 'Poppins-Bold',
      fontSize: 12,
      color: COLORS.color_yellow,
      marginTop: 5,
    },
    list_footer: {alignItems: 'center', marginTop: 50},
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
