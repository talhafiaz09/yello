import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, Linking} from 'react-native';
import useStyles from './useStyles';
import Header from '../../components/header/Header';
import {useSelector} from 'react-redux';
import {TouchableOpacity} from 'react-native';
import {FAB} from 'react-native-paper';

const Help = ({navigation}) => {
  const {Styles, COLORS} = useStyles();

  const questions = useSelector((state) => state.appReducer.questions);

  const URL = 'https://yellomobile.fr/';

  const mapQuestions = ({item, key}) => (
    <View style={{marginBottom: 30}}>
      <Text style={Styles.question}>{item.question}</Text>
      <View style={Styles.question_container}>
        <Text style={Styles.answer}>{item.answer}</Text>
      </View>
    </View>
  );

  return (
    <View flex={1} style={Styles.body}>
      <Header navigation={navigation} title={'Help'} notification menu />
      {/* <FAB
        style={Styles.fab}
        icon="dialpad"
        color={COLORS.color_primary}
        onPress={() => navigation.navigate('Call')}
      /> */}
      <FlatList
        data={questions}
        renderItem={mapQuestions}
        contentContainerStyle={{flexGrow: 1, padding: 20}}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={() => (
          <View style={Styles.NTS_container}>
            <Text style={Styles.NTS_text}>Nothing to show</Text>
          </View>
        )}
        ListFooterComponent={() => (
          <View style={Styles.list_footer}>
            {/* <Text style={{color: COLORS.color_text}}>
              Get help on WhatsApp:
            </Text>
            <TouchableOpacity
              onPress={() => Linking.openURL(`tel:+33187651931`)}>
              <Text style={{color: COLORS.color_primary, marginTop: 5}}>
                France: + 33 (1) 87651931
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => Linking.openURL(`tel:+18886173677`)}>
              <Text
                style={{
                  color: COLORS.color_primary,
                  marginBottom: 10,
                  marginTop: 5,
                }}>
                USA: + 1 (888) 6173677
              </Text>
            </TouchableOpacity> */}
            {/* <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  'whatsapp://send?text=Hello,\nI want to ask few questions related to yello.&phone=+923005666660',
                ).catch((err) => {
                  console.error('Failed opening page because: ', err);
                });
              }}
              style={{
                justifyContent: 'center',
                marginBottom: 10,
                marginTop: 5,
              }}>
              <WhatsApp height={50} width={50} />
            </TouchableOpacity> */}
            <Text style={Styles.VOB_text}>Visit our website:</Text>
            <TouchableOpacity
              onPress={() => {
                Linking.openURL(URL).catch((err) => {
                  console.error('Failed opening page because: ', err);
                });
              }}>
              <Text style={Styles.url}>{URL}</Text>
            </TouchableOpacity>
          </View>
        )}
      />
    </View>
  );
};

export default Help;
