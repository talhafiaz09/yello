import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
      zIndex: 200,
    },
    container_handling: {
      justifyContent: 'flex-end',
      flex: 1,
    },
    dialer_container: {
      // flex: 1,
      width: wp('70%'),
      height: hp('55%'),
    },
    text_field_main_container: {
      marginTop: hp('5%'),
      width: wp('90%'),
      alignItems: 'center',
      flexDirection: 'row',
      alignSelf: 'center',
      padding: 2,
      marginBottom: hp('5%'),
    },
    codepicker_container: {
      flexDirection: 'row',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'space-evenly',
      minWidth: '26%',
    },
    dial_number_text: {
      color: COLORS.color_text,
      fontSize: 14,
      fontFamily: 'Poppins-Regular',
    },
    icon_down_background: {
      backgroundColor: COLORS.color_yellow,
      borderRadius: 15,
      height: 20,
      width: 20,
      justifyContent: 'center',
      alignItems: 'center',
    },
    divider: {
      borderColor: COLORS.color_bottom,
      borderWidth: 1,
      position: 'absolute',
      height: '100%',
      left: '34.5%',
    },
    text_field_container: {marginLeft: 20, flex: 1},
    input_field: {
      flex: 1,
      color: COLORS.color_text,
    },
    text_field: {
      height: hp('6%'),
      width: '100%',
      backgroundColor: COLORS.color_textfield,
      marginBottom: 30,
      borderRadius: 15,
      flexDirection: 'row',
      paddingLeft: 10,
      paddingRight: 10,
      marginTop: hp('2%'),
    },
    view_contacts: {
      backgroundColor: COLORS.color_yellow,
      width: '100%',
      borderRadius: 15,
      padding: 5,
      height: hp('6%'),
      fontFamily: 'Poppins-Regular',
      fontSize: 14,
      justifyContent: 'center',
    },
    body_cnt: {
      paddingTop: 10,
      paddingLeft: 20,
      paddingRight: 20,
      alignItems: 'center',
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
