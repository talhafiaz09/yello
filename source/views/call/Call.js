import React, {useState, useEffect, useRef} from 'react';
import {View, TextInput, TouchableOpacity, Text} from 'react-native';
import {FAB} from 'react-native-paper';
import Header from '../../components/header/Header';
import Dialer from '../../components/dialer/Dialer';
import useStyles from './useStyles';
import Flag from 'react-native-flags';
import CountryModal from '../../components/countrymodal/CountryModal';
import ContactsModal from '../../components/contactsmodal/ContactsModal';
// import COLORS from '../../assets/colors/COLORS';
import Entypo from 'react-native-vector-icons/Entypo';
import {useDispatch, useSelector} from 'react-redux';
import useCountrySelection from '../../handlers/countrySelection';
import {
  changeCountryModalVisibility,
  changeContactsModalVisibility,
  setDialedNumber,
  setCursor,
} from '../../redux_handler/actions/app';
const Call = ({navigation}) => {
  const {setCountry} = useCountrySelection();

  const {Styles, COLORS} = useStyles();

  const dispatch = useDispatch();
  const setDialed_Number = (number) => dispatch(setDialedNumber(number));
  const changeCountryModal_Visibility = () =>
    dispatch(changeCountryModalVisibility());
  const set_Cursor = (data) => dispatch(setCursor(data));
  const changeContactsModal_Visibility = () =>
    dispatch(changeContactsModalVisibility());

  const dialtoInformation = useSelector(
    (state) => state.appReducer.dialtoInformation,
  );
  const dialedNumber = useSelector((state) => state.appReducer.dialedNumber);
  const cursor = useSelector((state) => state.appReducer.cursor);

  useEffect(() => {
    setCountry(dialedNumber);
  }, [dialedNumber]);

  const inputRef = useRef(null);
  useEffect(() => inputRef.current.focus());

  return (
    <View style={Styles.body}>
      <Header menu navigation={navigation} title="Call" notification />
      <CountryModal dialtoInfo />
      <ContactsModal navigation={navigation} />
      <View keyboardShouldPersistTaps="always" style={Styles.body_cnt}>
        <FAB
          style={Styles.view_contacts}
          icon="phone-log"
          color={COLORS.color_white}
          onPress={() => changeContactsModal_Visibility()}
          label={'Phone Contacts'}
          uppercase={false}
        />
        <View style={Styles.text_field}>
          <TouchableOpacity
            style={Styles.codepicker_container}
            onPress={() => {
              changeCountryModal_Visibility();
            }}>
            <Flag code={dialtoInformation.countryCode} size={32} />

            <Text style={Styles.dial_number_text}>
              +{dialtoInformation.dialCode != -1 && dialtoInformation.dialCode}
            </Text>

            <View style={Styles.icon_down_background}>
              <Entypo
                name="chevron-down"
                size={18}
                color={COLORS.color_textfield}
              />
            </View>
          </TouchableOpacity>
          <View style={Styles.divider} />
          <View style={Styles.text_field_container}>
            <TextInput
              ref={inputRef}
              value={dialedNumber}
              autoFocus={true}
              style={Styles.input_field}
              showSoftInputOnFocus={false}
              onChangeText={(text) => setDialed_Number(text)}
              keyboardType="numeric"
              onSelectionChange={(event) => {
                set_Cursor({
                  end: event.nativeEvent.selection.end,
                  start: event.nativeEvent.selection.start,
                });
              }}
              placeholderTextColor={COLORS.color_text}
              placeholder="Enter Number"
            />
          </View>
        </View>
        <View style={Styles.dialer_container}>
          <Dialer navigation={navigation} />
        </View>
      </View>
    </View>
  );
};
export default Call;
