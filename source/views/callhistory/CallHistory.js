import React, {useEffect} from 'react';
import {View, FlatList, TextInput, TouchableOpacity, Text} from 'react-native';
import useStyles from './useStyles';
import CallHistoryCard from '../../components/callhistorycard/CallHistoryCard';
import {setDialedNumber} from '../../redux_handler/actions/app';
import {useDispatch, useSelector} from 'react-redux';
import Loader from '../../components/loader';

const CallHistory = ({navigation, callData, loading}) => {
  const {Styles, COLORS} = useStyles();

  const dispatch = useDispatch();

  const setDialed_Number = async (number) => dispatch(setDialedNumber(number));

  const numberSet = (number) => {
    setDialed_Number(number.toString()).then(() => navigation.navigate('Call'));
  };

  const mapCallHistory = ({item, key}) => {
    return <CallHistoryCard data={item} numberSet={numberSet} />;
  };

  return (
    <View style={Styles.body}>
      {callData.length == 0 ? (
        <View style={Styles.NCH_container}>
          {loading ? (
            <Loader />
          ) : (
            <Text style={Styles.NCH_text}>No call history.</Text>
          )}
        </View>
      ) : (
        <FlatList
          data={callData}
          renderItem={mapCallHistory}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
        />
      )}
    </View>
  );
};
export default CallHistory;
