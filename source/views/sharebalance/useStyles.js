import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
      paddingBottom: 10,
    },
    divider: {
      height: 32,
      width: '100%',
      marginBottom: 20,
      backgroundColor: COLORS.color_border,
    },
    amount_container: {paddingTop: 20},
    card_container: {
      height: hp('50%'),
    },
    fab: {
      position: 'absolute',
      backgroundColor: COLORS.color_white,
      right: 25,
      bottom: 30,
      zIndex: 200,
    },
    // content: {paddingLeft: 20},
    available_balance_text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 14,
      color: COLORS.color_text,
      alignSelf: 'center',
    },
    credit_number: {
      fontFamily: 'Poppins-Bold',
      fontSize: 26,
      color: COLORS.color_yellow,
      alignSelf: 'center',
      marginTop: 10,
    },
    add_credit_button: {
      width: '50%',
      alignSelf: 'center',
      marginTop: 10,
    },
    heading_text: {
      marginTop: 10,
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
      fontSize: 14,
      alignSelf: 'center',
    },
    flatlist_container: {
      width: wp('72%'),
      alignSelf: 'center',
    },
    amounts_flatlist_container: {
      alignSelf: 'center',
    },
    amount: {
      height: 36,
      width: wp('20%'),
      alignItems: 'center',
      justifyContent: 'center',
      margin: 10,
      backgroundColor: COLORS.color_textfield,
      borderRadius: 6,
      shadowColor: COLORS.color_primary,
      shadowOffset: {width: 0, height: 0},
      shadowOpacity: 0.2,
      elevation: 2,
    },
    amount_text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 12,
      color: COLORS.color_text,
    },
    text_field_container: {
      height: hp('4.5%'),
      width: wp('70%'),
      alignSelf: 'center',
      backgroundColor: COLORS.color_textfield,
      borderRadius: 6,
      shadowColor: COLORS.color_primary,
      shadowOffset: {width: 0, height: 0},
      shadowOpacity: 0.2,
      elevation: 2,
      justifyContent: 'center',
      marginTop: 10,
    },
    textfield: {
      fontSize: 12,
      fontFamily: 'Poppins-Regular',
      height: '100%',
      width: '100%',
      padding: 0,
      paddingLeft: 10,
      paddingRight: 10,
      color: COLORS.color_text,
    },
    sub_text: {
      fontFamily: 'Poppins-SemiBold',
      color: COLORS.color_text,
      alignSelf: 'center',
      fontSize: 10,
      textAlign: 'center',
      lineHeight: 18,
      marginTop: 10,
    },
    contentStyle: {height: hp('6%')},
    labelStyle: {fontSize: hp('2%')},
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
