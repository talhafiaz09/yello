import React, {useState} from 'react';
import {
  View,
  Text,
  FlatList,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import useStyles from './useStyles';
import Header from '../../components/header/Header';
import Button from '../../components/button/Button';
import {useSelector} from 'react-redux';
import {TouchableOpacity} from 'react-native';
import {FAB} from 'react-native-paper';
import useModal from '../../handlers/useModal';
import ConfirmationModal from '../../components/confirmationmodal/ConfirmationModal';
import {shareCredit} from '../../redux_handler/api/index';

const ShareBalance = ({navigation}) => {
  const {Styles, COLORS} = useStyles();

  const [amountValue, setAmountValue] = useState('');
  const [number, setNumber] = useState('');

  const randomAmounts = useSelector((state) => state.appReducer.randomAmounts);
  const currency_rates = useSelector(
    (state) => state.userReducer.currency_rates,
  );
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const {modalStructure, showLoading, resetModal, showAlert, showConfirm} =
    useModal();

  const mapAmounts = ({item, key}) => {
    return (
      <TouchableOpacity
        style={Styles.amount}
        onPress={() => {
          setAmountValue(item);
        }}>
        <Text style={Styles.amount_text}>
          {currency_rates[userInformation.currency]?.symbol + ' ' + item}
        </Text>
      </TouchableOpacity>
    );
  };

  const onConfirm = async () => {
    showLoading('Please wait...').then(() =>
      shareCredit({
        mobile_number: parseInt(userInformation.phoneNumber),
        amount:
          parseFloat(amountValue) /
          currency_rates[userInformation.currency]?.rate,
        transfer_to: parseFloat(number),
      })
        .then((res) =>
          res.data.status_code == 200
            ? showAlert(`Transaction successful.`)
            : showAlert(`${res.data.message}\nTransaction not successful.`),
        )
        .catch((error) => showAlert(`Transaction not successful.`)),
    );
  };

  return (
    <TouchableWithoutFeedback flex={1} onPress={() => Keyboard.dismiss()}>
      <View style={Styles.body}>
        <Header
          navigation={navigation}
          title={'Share Credit'}
          notification
          badge
          menu
        />
        <ConfirmationModal
          modalStructure={modalStructure}
          pressedOk={resetModal}
          pressedNo={resetModal}
          pressedYes={() => onConfirm()}
        />
        {/* <FAB
          style={Styles.fab}
          icon="dialpad"
          color={COLORS.color_primary}
          onPress={() => navigation.navigate('Call')}
        /> */}
        <View style={Styles.amount_container}>
          <Text style={Styles.available_balance_text}>Current balance</Text>
          <Text style={Styles.credit_number}>
            {currency_rates[userInformation.currency]?.symbol +
              ' ' +
              parseFloat(
                userInformation.balance *
                  currency_rates[userInformation.currency]?.rate,
              ).toFixed(4)}
          </Text>
          <Text style={Styles.heading_text}>
            Enter points you want to share
          </Text>
          <View style={Styles.flatlist_container}>
            <FlatList
              data={randomAmounts}
              renderItem={mapAmounts}
              numColumns={3}
              scrollEnabled={false}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={Styles.amounts_flatlist_container}
              style={{padding: 0, marginTop: 10}}
            />
          </View>
          <View style={Styles.text_field_container}>
            <TextInput
              style={Styles.textfield}
              value={amountValue}
              placeholder={'Enter Amount (100 to 5,000)'}
              keyboardType={'number-pad'}
              onChangeText={(text) => {
                setAmountValue(text);
              }}
              placeholderTextColor={COLORS.color_text}
            />
          </View>
          <View style={Styles.text_field_container}>
            <TextInput
              style={Styles.textfield}
              value={number}
              placeholder={'Enter number'}
              keyboardType={'number-pad'}
              onChangeText={(text) => {
                setNumber(text.replace(/[^0-9]+/, ''));
              }}
              placeholderTextColor={COLORS.color_text}
            />
          </View>
        </View>
        <Text style={Styles.sub_text}>
          * enter number with country code without "+" sign.
        </Text>
        <View style={Styles.add_credit_button}>
          <Button
            text={'Share Credit'}
            type={'contained'}
            disabled={amountValue == '' || number.length < 10 ? true : null}
            contentStyle={Styles.contentStyle}
            labelStyle={Styles.labelStyle}
            onPress={() =>
              number == userInformation.phoneNumber.toString()
                ? showAlert(`You can't send credit to yourself.`)
                : showConfirm(
                    `Do you want to share $ ${amountValue} \nto +${number}?`,
                  )
            }
          />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};
export default ShareBalance;
