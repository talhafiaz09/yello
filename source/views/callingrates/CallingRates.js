import React, {useEffect, useState} from 'react';
import {View, FlatList, Text} from 'react-native';
import useStyles from './useStyles';
import {FAB, Portal, Provider} from 'react-native-paper';
import SearchBar from '../../components/searchbar/SearchBar';
import Header from '../../components/header/Header';
import CallingRatesCard from '../../components/callingratescard/CallingRatesCard';
import {getCallingRates} from '../../redux_handler/api/index';
import {useDispatch, useSelector} from 'react-redux';
import {setCallingRates} from '../../redux_handler/actions/app';
const CallingRates = ({navigation}) => {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);

  const {Styles, COLORS} = useStyles();

  const callingRates = useSelector((state) => state.appReducer.callingRates);
  const searchBarText = useSelector((state) => state.appReducer.searchBarText);

  const setCalling_Rates = async (callingRates) =>
    dispatch(setCallingRates(callingRates));

  const onRefresh = async () => {
    setLoading(true);
    await getCallingRates()
      .then((res) => {
        setCalling_Rates(res.data.data);
        setLoading(false);
      })
      .catch((error) => {
        console.error('Calling Rates Error:', error);
      });
  };

  const mapCallingRates = ({item, key}) => {
    return (
      <CallingRatesCard
        key={key}
        country_code={item.character_code}
        country_name={item.name}
        rate={item.rate}
      />
    );
  };

  return (
    <View style={Styles.body}>
      <Header
        menu
        navigation={navigation}
        title={'International Calling Rates'}
        notification
        badge
        style={Styles.custom_header_style}
      />
      {/* <FAB
        style={Styles.fab}
        icon="dialpad"
        color={COLORS.color_primary}
        onPress={() => navigation.navigate('Call')}
      /> */}
      <View style={Styles.searchbar_container}>
        <SearchBar placeholder={'Search by country name'} />
      </View>
      <FlatList
        onRefresh={() => onRefresh()}
        refreshing={loading}
        data={
          searchBarText === ''
            ? callingRates
            : callingRates.filter((index) =>
                index.name
                  .toUpperCase()
                  .replace(/\./g, '')
                  .includes(searchBarText.toUpperCase().replace(/\./g, '')),
              )
        }
        renderItem={mapCallingRates}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={Styles.flatlist_container}
        styles={Styles.callingrate_flatlist_container}
        ListHeaderComponent={() => (
          <View style={{paddingLeft: 20, paddingBottom: 20, paddingTop: 10}}>
            <Text style={Styles.HRAC_text}>How rates are calculated?</Text>
            <Text style={Styles.sub_text}>
              1 minute rounding. Total cost of each rounded to next full minimum
              currency unit.
            </Text>
          </View>
        )}
        ListEmptyComponent={() => (
          <View style={Styles.NTS_container}>
            <Text style={Styles.NTS_text}>Nothing to show</Text>
          </View>
        )}
      />
    </View>
  );
};
export default CallingRates;
