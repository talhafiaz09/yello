import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
      paddingBottom: 10,
    },
    searchbar_container: {
      marginTop: 10,
    },
    custom_header_style: {
      elevation: 0,
    },
    callingrate_flatlist_container: {
      width: '100%',
    },
    flatlist_container: {
      flexGrow: 1,
    },
    fab: {
      position: 'absolute',
      backgroundColor: COLORS.color_white,
      right: 25,
      bottom: 30,
      zIndex: 200,
    },
    HRAC_text: {
      marginBottom: 5,
      fontSize: 12,
      fontFamily: 'Poppins-Bold',
      color: COLORS.color_yellow,
    },
    sub_text: {
      fontSize: 12,
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
    },
    NTS_text: {
      fontFamily: 'Poppins-LightItalic',
      color: COLORS.color_text,
    },
    NTS_container: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
