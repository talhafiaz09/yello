import React, {useState} from 'react';
import {View, FlatList, TextInput, TouchableOpacity, Text} from 'react-native';
import useStyles from './useStyles';
import TransactionHistoryCard from '../../components/transactionhistorycard/TransactionHistoryCard';

const TransactionHistory = ({navigation, transactionData, loading}) => {
  const {Styles} = useStyles();

  const mapCallHistory = ({item, key}) => {
    return <TransactionHistoryCard data={item} />;
  };

  return (
    <View style={Styles.body}>
      {transactionData.length == 0 ? (
        <View style={Styles.NCH_container}>
          {loading ? (
            <Loader />
          ) : (
            <Text style={Styles.NCH_text}>No transaction history.</Text>
          )}
        </View>
      ) : (
        <FlatList
          data={transactionData}
          renderItem={mapCallHistory}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
        />
      )}
    </View>
  );
};
export default TransactionHistory;
