import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
    },
    NCH_text: {
      fontFamily: 'Poppins-LightItalic',
      fontSize: 12,
      color: COLORS.color_text,
    },
    NCH_container: {
      height: '80%',
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
