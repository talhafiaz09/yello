import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, FlatList} from 'react-native';
import useStyles from './useStyles';
import {FAB} from 'react-native-paper';
import CallHistory from '../callhistory/CallHistory';
import TransactionHistory from '../transactionhistory/TransactionHistory';
import Header from '../../components/header/Header';
import {useSelector} from 'react-redux';
import {
  getCallHistory,
  getTransactionHistory,
} from '../../redux_handler/api/index';
const History = ({navigation}) => {
  const {Styles, COLORS} = useStyles();

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const [loading, setLoading] = useState(true);
  const [tab1, setTab1] = useState(true);
  const [tab2, setTab2] = useState(false);
  const [callData, setCallData] = useState([]);
  const [transactionData, setTransactionData] = useState([]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getCallHistory({mobile_number: userInformation.phoneNumber})
        .then((res) => {
          // console.log(res.data.data);
          setCallData(res.data.data);
          setLoading(false);
        })
        .catch((error) => console.log('Call history: ', error));
      getTransactionHistory({
        mobile_number: userInformation.phoneNumber,
      })
        .then((res) => setTransactionData(res.data.data))
        .catch((error) => console.log('Transaction history: ', error));
    });
    return unsubscribe;
  }, [navigation]);

  const tabHandler = () => {
    setTab1(!tab1);
    setTab2(!tab2);
  };

  return (
    <View style={Styles.body}>
      <Header menu navigation={navigation} title={'History'} notification />
      {/* <FAB
        style={Styles.fab}
        icon="dialpad"
        color={COLORS.color_primary}
        onPress={() => navigation.navigate('Call')}
      /> */}
      <View style={{padding: 10}}>
        <View style={Styles.tab}>
          <TouchableOpacity
            onPress={() => {
              !tab1 ? tabHandler() : null;
            }}
            style={[
              Styles.tabs,
              {
                backgroundColor: tab1
                  ? COLORS.color_active_tab
                  : COLORS.color_inactive_tab,
              },
            ]}>
            <Text
              style={{
                color: COLORS.color_white,
                fontFamily: 'Poppins-Regular',
                fontSize: 14,
              }}>
              Calls
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              !tab2 ? tabHandler() : null;
            }}
            style={[
              Styles.tabs,
              {
                backgroundColor: tab2
                  ? COLORS.color_active_tab
                  : COLORS.color_inactive_tab,
              },
            ]}>
            <Text
              style={{
                color: COLORS.color_white,
                fontFamily: 'Poppins-Regular',
                fontSize: 14,
              }}>
              Transactions
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '100%',
            height: '100%',
          }}>
          {tab1 ? (
            <CallHistory
              callData={callData}
              loading={loading}
              navigation={navigation}
            />
          ) : (
            <TransactionHistory
              transactionData={transactionData}
              loading={loading}
            />
          )}
        </View>
      </View>
    </View>
  );
};
export default History;
