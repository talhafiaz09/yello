import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
      paddingBottom: 10,
    },
    searchbar_container: {
      backgroundColor: COLORS.color_primary,
      alignItems: 'center',
    },
    fab: {
      position: 'absolute',
      backgroundColor: COLORS.color_white,
      right: 25,
      bottom: 30,
      zIndex: 200,
    },
    custom_header_style: {
      elevation: 0,
    },
    callingrate_flatlist_container: {width: '100%'},
    tab: {
      backgroundColor: COLORS.color_textfield,
      alignSelf: 'center',
      width: '100%',
      height: hp('6%'),
      borderRadius: 10,
      flexDirection: 'row',
      padding: 5,
      justifyContent: 'space-between',
      marginBottom: 10,
    },
    tabs: {
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10,
      width: '48%',
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
