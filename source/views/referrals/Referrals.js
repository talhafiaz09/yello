import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, RefreshControl, TextInput} from 'react-native';
import useStyles from './useStyles';
import {useSelector} from 'react-redux';
import Header from '../../components/header/Header';
import Button from '../../components/button/Button';
import {getReferralsApi, addReferralsApi} from '../../redux_handler/api/index';
import Loader from '../../components/loader';
import ConfirmationModal from '../../components/confirmationmodal/ConfirmationModal';
import useModal from '../../handlers/useModal';

const Referrals = ({navigation}) => {
  const {Styles, COLORS} = useStyles();

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [rates, setRates] = useState({});
  const [code, setCode] = useState('');

  useEffect(() => {
    fetchData();
  }, []);

  const mapContent = ({item, key}) => (
    <View key={key} style={Styles.content_main}>
      <Text style={Styles.content_text}>{item.mobile}</Text>
    </View>
  );

  const header = () => {
    if (!rates) return <></>;
    return (
      <View style={Styles.header}>
        <Text style={Styles.text_rates}>Rates:</Text>
        {Object.keys(rates).map((e, index) => (
          <View key={index} style={Styles.header_container}>
            <Text style={Styles.header_text_left}>{e}</Text>
            <Text style={Styles.header_text_right}>{rates[e]}</Text>
          </View>
        ))}
        <Text style={Styles.text_rates}>List:</Text>
      </View>
    );
  };

  const fetchData = () => {
    setLoading(true);
    getReferralsApi({
      mobile: userInformation.phoneNumber,
    })
      .then((res) => {
        res.data.data.referrals && setData(res.data.data.referrals);
        res.data.data.discount_rates && setRates(res.data.data.discount_rates);
        setLoading(false);
      })
      .catch((err) => {});
  };

  const {modalStructure, showLoading, resetModal, showAlert, showConfirm} =
    useModal();

  const addData = () => {
    showLoading(`Please wait...`)
      .then(() =>
        addReferralsApi({
          parent: code,
          child: userInformation.phoneNumber,
        }).then(
          (res) =>
            res.data.status_code == 200
              ? showAlert(`Successful.`)
              : showAlert(`${res.data.message}\nNot successful.`),
          setCode(''),
        ),
      )
      .catch((err) => showAlert(`Not successful.`));
  };

  return (
    <View style={Styles.body}>
      <Header menu navigation={navigation} title={'Referrals'} notification />
      <ConfirmationModal
        modalStructure={modalStructure}
        pressedOk={resetModal}
      />
      <View flex={1}>
        <View style={Styles.header}>
          <TextInput
            style={Styles.text_field}
            value={code}
            onChangeText={(text) => setCode(text)}
            placeholder={'Enter referel code'}
            placeholderTextColor={COLORS.color_text}
          />
          <Button
            text={'Become referel'}
            type={'contained'}
            onPress={addData}
            contentStyle={Styles.contentStyle}
            labelStyle={Styles.labelStyle}
            disabled={code.length === 0}
          />
        </View>
        {!loading ? (
          <FlatList
            data={data}
            refreshControl={
              <RefreshControl
                refreshing={loading}
                onRefresh={fetchData}
                tintColor={COLORS.color_text}
              />
            }
            renderItem={mapContent}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{flexGrow: 1}}
            styles={Styles.modal_flatlist_container}
            ListEmptyComponent={() => (
              <View style={Styles.NTS_container}>
                <Text style={Styles.NTS_text}>Nothing to show</Text>
              </View>
            )}
            ListHeaderComponent={header}
          />
        ) : (
          <View style={Styles.loader_container}>
            <Loader />
          </View>
        )}
      </View>
    </View>
  );
};

export default Referrals;
