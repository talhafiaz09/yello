import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';

const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
    },
    content_text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 12,
      color: COLORS.color_text,
    },
    text_field: {
      paddingTop: 0,
      paddingBottom: 0,
      margin: 0,
      paddingLeft: 8,
      height: hp('5.5%'),
      paddingRight: 5,
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
      width: '100%',
      backgroundColor: COLORS.color_textfield,
      borderRadius: 10,
      marginBottom: 10,
      marginTop: 10,
    },
    contentStyle: {
      height: hp('5.5%'),
      borderRadius: 10,
    },
    labelStyle: {fontSize: hp('1.6%')},
    text_rates: {
      fontFamily: 'Poppins-Bold',
      color: COLORS.color_text,
      fontSize: 14,
      marginBottom: 10,
      marginTop: 10,
    },
    header: {width: '95%', alignSelf: 'center'},
    header_container: {
      width: '100%',
      flexDirection: 'row',
      alignSelf: 'center',
      marginTop: hp('0.5%'),
      marginBottom: hp('0.5%'),
    },
    header_text_right: {color: COLORS.color_text},
    header_text_left: {color: COLORS.color_yellow, width: '40%'},
    loader_container: {flex: 1, justifyContent: 'center'},
    content_main: {
      alignSelf: 'center',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: COLORS.color_textfield,
      marginTop: 5,
      height: hp('5.5%'),
      width: '95%',
      borderRadius: 10,
    },
    NTS_text: {
      fontFamily: 'Poppins-Italic',
      color: COLORS.color_text,
    },

    NTS_container: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
    },
    modal_flatlist_container: {width: '100%', height: '100%'},
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
