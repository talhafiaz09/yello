import React, {useEffect, useState} from 'react';
import {View, FlatList, Text} from 'react-native';
import useStyles from './useStyles';
import {FAB} from 'react-native-paper';
import SearchBar from '../../components/searchbar/SearchBar';
import ConfirmationModal from '../../components/confirmationmodal/ConfirmationModal';
import Header from '../../components/header/Header';
import {useSelector, useDispatch} from 'react-redux';
import CouponsCard from '../../components/couponscard/CouponsCard';
import {setCoupons} from '../../redux_handler/actions/app';
import {getCoupons} from '../../redux_handler/api/index';
import useModal from '../../handlers/useModal';
import {purchaseCoupon} from '../../redux_handler/api/index';

const Coupons = ({navigation}) => {
  const dispatch = useDispatch();

  const {Styles, COLORS} = useStyles();

  const set_Coupons = async (coupons) => dispatch(setCoupons(coupons));

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getCoupons({mobile_number: null}).then((res) =>
        set_Coupons(res.data.data),
      );
    });
    return unsubscribe;
  }, [navigation]);

  const coupons = useSelector((state) => state.appReducer.coupons);
  const searchBarText = useSelector((state) => state.appReducer.searchBarText);
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const [data, setData] = useState(null);

  const mapCallingRates = ({item, key}) => {
    return <CouponsCard key={key} data={item} onPress={onPress} />;
  };

  const {modalStructure, showLoading, resetModal, showAlert, showConfirm} =
    useModal();

  const onPress = (data) => {
    setData(data);
    showConfirm(
      `Do you want to buy "${data.name}" coupon\nof amount ${data.amount}?`,
    );
  };

  const onConfirm = async () => {
    await showLoading('PLease wait...')
      .then(() =>
        purchaseCoupon({
          mobile_number: parseInt(userInformation.phoneNumber),
          coupon_id: data.id,
        }).then((res) =>
          res.data.status_code == 200
            ? showAlert(`Transaction successful.`)
            : showAlert(`${res.data.message}\nTransaction not successful.`),
        ),
      )
      .catch(() => showAlert('Transaction not successful.'));
    // setData(null);
  };

  return (
    <View style={Styles.body}>
      <ConfirmationModal
        modalStructure={modalStructure}
        pressedOk={resetModal}
        pressedNo={resetModal}
        pressedYes={() => onConfirm()}
      />
      <Header
        menu
        navigation={navigation}
        title={'Coupons'}
        notification
        style={Styles.custom_header_style}
      />
      {/* <FAB
        style={Styles.fab}
        icon="dialpad"
        color={COLORS.color_primary}
        onPress={() => navigation.navigate('Call')}
      /> */}
      <View style={{flex: 1, padding: 10}}>
        <View style={Styles.searchbar_container}>
          <SearchBar placeholder={'Search by coupon name'} />
        </View>
        <FlatList
          data={
            searchBarText === ''
              ? coupons
              : coupons.filter((index) => {
                  return index.name
                    .toUpperCase()
                    .replace(/\./g, '')
                    .includes(searchBarText.toUpperCase().replace(/\./g, ''));
                })
          }
          renderItem={mapCallingRates}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{flexGrow: 1}}
          styles={Styles.callingrate_flatlist_container}
          ListEmptyComponent={() => (
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                flex: 1,
              }}>
              <Text
                style={{
                  fontFamily: 'Poppins-Italic',
                  color: COLORS.color_text,
                }}>
                Nothing to show
              </Text>
            </View>
          )}
        />
      </View>
    </View>
  );
};
export default Coupons;
