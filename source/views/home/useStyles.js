import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      width: '100%',
      backgroundColor: COLORS.color_bottom,
    },
    top_c: {
      backgroundColor: COLORS.color_textfield,
      width: '100%',
      padding: 20,
      flexDirection: 'row',
    },
    btn: {height: 40, borderRadius: 10},
    button_background: {borderRadius: 10},
    inner_container: {
      flex: 1,
    },
    labelStyle: {fontSize: 14},

    add_credit_button: {
      flex: 1,
      paddingLeft: wp('25%'),
    },
    divider: {
      borderWidth: 1,
      borderColor: COLORS.color_side,
      marginLeft: 20,
      marginRight: 20,
      height: '85%',
      alignSelf: 'center',
      opacity: 0.5,
      backgroundColor: 'red',
    },
    menu_card_container: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: 20,
      marginTop: 10,
    },
    NTS_container: {
      flex: 1,
      height: hp('40%'),
      justifyContent: 'center',
      alignItems: 'center',
    },
    NTS_text: {
      fontFamily: 'Poppins-LightItalic',
      fontSize: 12,
      color: COLORS.color_text,
    },
    available_packages_text: {
      fontFamily: 'Poppins-Bold',
      fontSize: 14,
      color: COLORS.color_text,
      marginBottom: 10,
    },

    fab: {
      position: 'absolute',
      // backgroundColor: Colors.color_white,
      right: 25,
      bottom: 30,
      zIndex: 200,
      elevation: 5,
    },
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
