import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, ScrollView, RefreshControl} from 'react-native';
// import {FAB} from 'react-native-paper';
import Header from '../../components/header/Header';
import useStyles from './useStyles';
import Button from '../../components/button/Button';
import MenuCard from '../../components/menucard/MenuCard';
import UserPackageCard from '../../components/userpackagecard/UserPackageCard';
import CallingRateIcon from '../../assets/images/callingrateicon/CallingRateIcon';
import PackagesIcon from '../../assets/images/packagesicon/PackagesIcon';
import useAsyncStorage from '../../handlers/useAsyncStorage';
import {useDispatch, useSelector} from 'react-redux';
import {home} from '../../redux_handler/api/index';
import {setuserInformation} from '../../redux_handler/actions/user';
import messaging from '@react-native-firebase/messaging';
import database from '@react-native-firebase/database';
import BalanceContainer from '../../components/balance_container/BalanceContainer';

const Home = ({navigation}) => {
  const dispatch = useDispatch();

  const setuser_Information = async (info) =>
    dispatch(setuserInformation(info));

  const {Styles, COLORS} = useStyles();

  const [loading, setLoading] = useState(false);

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  const currency_rates = useSelector(
    (state) => state.userReducer.currency_rates,
  );

  const {setUserInformationAsyncStorage} = useAsyncStorage();

  const homeScreenData = () => {
    setLoading(true);
    home({mobile_number: userInformation.phoneNumber})
      .then((res) => {
        // console.log(res.data.data.client.balance);
        setuser_Information({
          ...userInformation,
          balance: parseFloat(res.data.data.client.balance),
          packages: res.data.data.packages,
          rewardpoints: parseFloat(res.data.data.client.reward_points),
          referral_code: res.data.data.client.referral_code,
        }).then((res) => {
          // console.log(res);
          setUserInformationAsyncStorage(res.data);
          setLoading(false);
        });
      })
      .catch((err) => console.log('Home: ', err));
  };

  useEffect(() => {
    homeScreenData();
  }, []);

  return (
    <View style={Styles.body}>
      <Header menu navigation={navigation} title={'Home'} notification />
      {/* <FAB
        style={Styles.fab}
        icon="dialpad"
        color={Colors.color_primary}
        onPress={() => navigation.navigate('Call')}
      /> */}
      <ScrollView
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={homeScreenData}
            tintColor={COLORS.color_text}
          />
        }>
        <View style={Styles.inner_container}>
          <View style={Styles.top_c}>
            <View flex={1}>
              <BalanceContainer
                symbol={currency_rates[userInformation.currency]?.symbol}
                text={
                  userInformation.balance *
                  currency_rates[userInformation.currency]?.rate
                }
                title={'Available Balance'}
              />
              <BalanceContainer
                name={'award'}
                text={userInformation.rewardpoints}
                title={'Reward points'}
              />
            </View>
            <View flex={1}>
              <Button
                text={'Add Credit'}
                type={'contained'}
                onPress={() => navigation.navigate('AddCredit')}
                contentStyle={Styles.btn}
                style={Styles.button_background}
                labelStyle={Styles.labelStyle}
              />
            </View>
          </View>
          <View style={{padding: 10}}>
            <View style={Styles.menu_card_container}>
              <MenuCard text={'Calling Rates'} navigation={navigation}>
                <CallingRateIcon />
              </MenuCard>
              <View style={Styles.divider} />
              <MenuCard text={'Calling Packages'} navigation={navigation}>
                <PackagesIcon />
              </MenuCard>
            </View>
            <Text style={Styles.available_packages_text}>
              Subscribed Packages
            </Text>
            {userInformation.packages.length == 0 && (
              <View style={Styles.NTS_container}>
                <Text style={Styles.NTS_text}>
                  No packages brought to display.
                </Text>
              </View>
            )}
          </View>
          {userInformation.packages.map((item, key) => (
            <UserPackageCard
              key={key}
              title={item.name}
              total_min={item.total_min}
              end_date={item.exp_date}
              // life={item.life}
              country={item.country}
              countrycode={item.country_code}
              amount={item.amount}
              remaining_minutes={item.remaining_minutes}
            />
          ))}
        </View>
      </ScrollView>
    </View>
  );
};
export default Home;
