import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import CountDown from 'react-native-countdown-component';
import Button from '../../components/button/Button';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
import TopContainer from '../../components/top_container/TopContainer';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {newCustomer} from '../../redux_handler/api/index';
import Logo from '../../assets/images/logo/Logo';
import {setuserInformation} from '../../redux_handler/actions/user';
import useAsyncStorage from '../../handlers/useAsyncStorage';
import auth from '@react-native-firebase/auth';

const Otp = ({
  handleScroll,
  signInWithPhoneNumber,
  showLoading,
  confirmCode,
  showAlert,
  resetModal,
  navigation,
}) => {
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const dispatch = useDispatch();
  const setuser_Information = async (info) =>
    dispatch(setuserInformation(info));

  const {Styles, COLORS} = useStyles();
  const visible = useSelector((state) => state.appReducer.resendOtpVisibility);
  const [otpDisabled, setOtpDisabled] = useState(false);
  const [count, setCount] = useState(0);
  const [value, setValue] = useState('');
  const [execute, setExecute] = useState(false);

  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  const renderCell = ({index, symbol, isFocused}) => {
    let textChild = null;
    if (symbol) {
      textChild = symbol;
    } else if (isFocused) {
      textChild = <Cursor />;
    }
    return (
      <Text
        key={index}
        style={[Styles.cell, isFocused && Styles.focusCell]}
        onLayout={getCellOnLayoutHandler(index)}>
        {textChild}
      </Text>
    );
  };

  const authenticateOtp = async () => {
    showLoading(`Authenticating your phone number \nplease wait...`).then(() =>
      confirmCode(value).then((res) =>
        res?.message ? showAlert(res.message) : newCustomerMethod(),
      ),
    );
  };

  const newCustomerMethod = () => {
    newCustomer({
      country_id: userInformation.countryId,
      mobile_number: parseInt(
        '' + userInformation.dialCode + userInformation.phoneNumber,
      ),
      device_token: userInformation.device_token,
    })
      .then((res) =>
        setuser_Information({
          ...userInformation,
          phoneNumber: userInformation.dialCode + userInformation.phoneNumber,
          balance: parseFloat(res.data.data.client.balance),
          packages: res.data.data.packages,
          currency: res.data.data.client.currency,
          rewardpoints: parseFloat(res.data.data.client.reward_points),
        }).then((res) =>
          resetModal().then(() => navigation.replace('DrawerNavigation')),
        ),
      )
      .catch((error) => showAlert(error));
  };

  const {getUserInformationAsyncStorage} = useAsyncStorage();

  useEffect(async () => {
    const unsubscribe =
      value &&
      Platform.OS === 'android' &&
      auth().onAuthStateChanged(async (user) => {
        // console.log('Here: ', user);
        if (user) {
          await getUserInformationAsyncStorage().then((res) => {
            console.log(
              user?.phoneNumber,

              '+' + userInformation.dialCode + userInformation.phoneNumber,
            );
            !res &&
              '+' + userInformation.dialCode + userInformation.phoneNumber ==
                user?.phoneNumber &&
              showLoading(
                `Authenticating your phone number \nplease wait...`,
              ).then(() => newCustomerMethod());
          });
        }
      });
    return () => unsubscribe();
  }, [value]);

  return (
    <View style={Styles.body}>
      <TopContainer onPress={() => handleScroll(1)}>
        <View style={Styles.text_container}>
          <Text style={Styles.title}>ENTER CODE</Text>
          <Text style={Styles.sub_title}>
            Please enter the code that we sent to
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={Styles.sub_title_number}>
              {'+' + userInformation.dialCode + userInformation.phoneNumber}
            </Text>
            <FontAwesome
              style={Styles.otp_icon}
              name="pencil-square-o"
              color={COLORS.color_yellow}
              size={13}
            />
          </View>
        </View>
      </TopContainer>
      <KeyboardAvoidingView
        style={Styles.bottom_container}
        behavior={Platform.OS === 'android' ? 'height' : 'padding'}>
        <View style={Styles.logo_container}>
          <Logo height={hp('15%')} width={hp('15%') * 1.13709} opacity={0.26} />
        </View>
        <View style={Styles.text_field_main_container}>
          <CodeField
            {...props}
            value={value}
            onChangeText={setValue}
            cellCount={6}
            keyboardType="number-pad"
            textContentType="oneTimeCode"
            renderCell={renderCell}
            rootStyle={Styles.rootStyle}
          />
        </View>
        <Button
          text={'Continue'}
          type={'contained'}
          contentStyle={Styles.btn_container}
          labelStyle={Styles.label_style}
          disabled={value.length === 6 ? false : true}
          onPress={authenticateOtp}
        />
        {visible && (
          <TouchableOpacity
            onPress={() => {
              signInWithPhoneNumber(
                '+' + userInformation.dialCode + userInformation.phoneNumber,
              );
              setCount(120);
              setOtpDisabled(true);
            }}
            disabled={otpDisabled}
            style={Styles.resend_otp_container}>
            <Text style={Styles.resend_otp_text}>Resend OTP</Text>
            {otpDisabled && (
              <CountDown
                size={0}
                until={count}
                onFinish={() => {
                  setTimeout(() => {
                    setOtpDisabled(false);
                  }, 100);
                }}
                style={Styles.countdown_container}
                digitStyle={Styles.digitStyle}
                digitTxtStyle={Styles.digitTxtStyle}
                separatorStyle={Styles.separatorStyle}
                timeToShow={['M', 'S']}
                timeLabels={{m: null, s: null}}
                showSeparator
              />
            )}
          </TouchableOpacity>
        )}
      </KeyboardAvoidingView>
    </View>
  );
};
export default Otp;
