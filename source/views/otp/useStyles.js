import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
    },
    top_container: {
      height: hp('30%'),
      backgroundColor: COLORS.color_top,
      width: '100%',
    },
    svg_view: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    half_C: {
      position: 'absolute',
      top: '7%',
      left: '30%',
      zIndex: 10,
    },
    half_G: {},
    text_container: {position: 'absolute', bottom: '20%', left: '6%'},
    title: {
      color: COLORS.color_white,
      fontFamily: 'Poppins-Bold',
      fontSize: hp('2.5%'),
    },
    sub_title: {
      color: COLORS.color_white,
      fontFamily: 'Poppins-Light',
      fontSize: hp('1.5%'),
    },
    sub_title_number: {
      color: COLORS.color_yellow,
      fontFamily: 'Poppins-Light',
      fontSize: hp('1.5%'),
      marginRight: 5,
    },
    redT_container: {position: 'absolute', bottom: 0, right: 0},
    text_icon_container: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignContent: 'center',
      marginTop: 5,
    },
    heading_container_subheading_text: {
      // fontFamily: 'Montserrat-Light',
      fontSize: 10,
      textAlign: 'center',
      // color: Colors.color_text,
      lineHeight: 15,
    },
    icon_container: {alignSelf: 'center'},
    bottom_container: {
      justifyContent: 'center',
      flex: 1,
      paddingLeft: 20,
      paddingRight: 20,
    },
    text_field_main_container: {
      marginBottom: 20,
    },
    logo_container: {
      position: 'absolute',
      top: 0,
      alignSelf: 'center',
      marginTop: 20,
    },
    button_container: {width: wp('45%'), marginBottom: hp('2%')},
    cell: {
      width: wp('12%'),
      height: hp('7%'),
      fontSize: 20,
      fontFamily: 'Poppins-Bold',
      textAlign: 'center',
      lineHeight: hp('7%'),
      borderColor: COLORS.color_active_tab,
      borderWidth: 1,
      borderRadius: 10,
      backgroundColor: COLORS.color_textfield,
      overflow: 'hidden',
      color: COLORS.color_text,
    },
    rootStyle: {
      width: '100%',
    },
    focusCell: {
      borderColor: COLORS.color_yellow,
    },
    resend_otp_container: {
      width: '70%',
      marginTop: 20,
      backgroundColor: COLORS.color_textfield,
      paddingTop: 5,
      paddingBottom: 5,
      paddingLeft: 10,
      paddingRight: 10,
      borderRadius: 15,
      height: hp('5%'),
      alignSelf: 'center',
      alignItems: 'center',
      justifyContent: 'center',
    },
    resend_otp_text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 10,
      color: COLORS.color_text,
      marginLeft: 10,
      alignSelf: 'center',
    },

    label_style: {fontSize: hp('2%')},
    btn_container: {height: hp('6%'), width: '100%'},
    digitStyle: {
      color: COLORS.color_yellow,
      width: 'auto',
      height: 'auto',
    },
    countdown_container: {marginLeft: 10},
    otp_icon: {alignSelf: 'center'},
    digitTxtStyle: {
      color: COLORS.color_yellow,
      marginHorizontal: 0,
      padding: 0,
      fontSize: 10,
      alignSelf: 'center',
      fontFamily: 'Poppins-Regular',
      fontWeight: null,
    },
    separatorStyle: {
      color: COLORS.color_yellow,
      fontWeight: null,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
