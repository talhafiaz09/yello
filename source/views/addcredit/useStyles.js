import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      backgroundColor: COLORS.color_bottom,
    },
    divider: {
      height: 32,
      width: '100%',
      marginBottom: 20,
      backgroundColor: COLORS.color_border,
    },

    card_container: {
      marginBottom: 100,
    },
    contentStyle: {height: hp('6%')},
    add_credit_button: {
      width: '60%',
      alignSelf: 'center',
      marginTop: 30,
      height: hp('6%'),
    },
    labelStyle: {
      fontSize: hp('1.7%'),
      fontFamily: 'Poppins-Regular',
    },
    field_main_container: {
      // width: '100%',
    },
    inputStyle: {
      borderBottomColor: COLORS.color_text,
      borderBottomWidth: 0.5,
      color: COLORS.color_text,
    },
    cardLabelStyle: {
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_white,
      fontSize: 12,
      backgroundColor: 'red',
      alignSelf: 'baseline',
      padding: 1,
      borderRadius: 5,
    },
    heading_text: {
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_yellow,
      fontSize: 12,
      marginBottom: 10,
    },
    flatlist_container: {
      width: wp('72%'),
      alignSelf: 'center',
    },
    amounts_flatlist_container: {
      alignItems: 'center',
      paddingTop: 10,
    },
    amount: {
      height: 36,
      width: wp('20%'),
      alignItems: 'center',
      justifyContent: 'center',
      margin: 10,
      backgroundColor: COLORS.color_textfield,
      borderRadius: 10,
      shadowColor: COLORS.color_textfield,
      shadowOffset: {width: 0, height: 0},
      shadowOpacity: 0.2,
      elevation: 2,
    },
    amount_text: {
      fontFamily: 'Poppins-Medium',
      fontSize: 12,
      color: COLORS.color_text,
    },
    text_field_container: {
      height: 40,
      width: '100%',
      alignSelf: 'center',
      justifyContent: 'center',
      borderBottomColor: COLORS.color_text,
      borderBottomWidth: 1,
    },
    textfield: {
      fontSize: 10,
      fontFamily: 'Poppins-Regular',
      padding: 0,
      color: COLORS.color_text,
    },
    tab: {
      backgroundColor: COLORS.color_border,
      alignSelf: 'center',
      marginTop: 11,
      width: wp('80%'),
      height: hp('5%'),
      borderRadius: 10,
      flexDirection: 'row',
      padding: 3,
      // marginBottom: 10,
    },
    tabs: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10,
    },
  });
  return {
    Styles,
    COLORS,
    wp,
  };
};
export default useStyles;
