import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  KeyboardAvoidingView,
  Modal,
  ScrollView,
} from 'react-native';
import {WebView} from 'react-native-webview';
import useStyles from './useStyles';
import Header from '../../components/header/Header';
import Button from '../../components/button/Button';
import {useSelector, useDispatch} from 'react-redux';
import {CreditCardInput} from 'react-native-credit-card-input';
import {TouchableOpacity} from 'react-native';
import CreditContainer from '../../components/creditcontainer/CreditContainer';
import ConfirmationModal from '../../components/confirmationmodal/ConfirmationModal';
import useModal from '../../handlers/useModal';
import {newTopup} from '../../redux_handler/api/index';
import PaypalModal from '../../components/paypalmodal/PaypalModal';

const AddCredit = ({navigation}) => {
  const {Styles, COLORS, wp} = useStyles();

  const [amountValue, setAmountValue] = useState('');
  const [view, setView] = useState(false);
  const [cardData, setCardData] = useState(null);
  const [tab, setTab] = useState(true);

  const randomAmounts = useSelector((state) => state.appReducer.randomAmounts);
  const currency_rates = useSelector(
    (state) => state.userReducer.currency_rates,
  );
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const {modalStructure, showLoading, resetModal, showAlert, showConfirm} =
    useModal();

  const onChange = (cardInfo) => {
    cardInfo.status.number == 'valid' &&
    cardInfo.status.cvc == 'valid' &&
    cardInfo.status.expiry == 'valid'
      ? setCardData({...cardInfo.values})
      : setCardData(null);
  };

  const mapAmounts = ({item, key}) => {
    return (
      <TouchableOpacity
        style={Styles.amount}
        onPress={() => {
          setAmountValue(item);
        }}>
        <Text style={Styles.amount_text}>
          {currency_rates[userInformation.currency]?.symbol + ' ' + item}
        </Text>
      </TouchableOpacity>
    );
  };

  const getHeader = () => {
    return <View></View>;
  };

  const showModal = () => {
    setView(!view);
  };

  const makeTransaction = () =>
    showConfirm(
      `Do you want to do a topup\nof ${
        currency_rates[userInformation.currency]?.symbol + amountValue
      }?`,
    );

  const onConfirm = () => {
    showLoading(`Please wait...`)
      .then(() =>
        newTopup({
          type: 1,
          mobile_number: parseInt(userInformation.phoneNumber),
          amount: amountValue / currency_rates[userInformation.currency]?.rate,
          package_id: null,
          tranfer_to: null,
          // account_number: '4242424242424242',
          // cvc: '232',
          // expiry: '12/23',
          account_number: cardData.number,
          cvc: cardData.cvc,
          expiry: cardData.expiry,
        }).then((res) =>
          res.data.status_code == 200
            ? showAlert(`Transaction successful.`)
            : showAlert(`${res.data.message}\nTransaction not successful.`),
        ),
      )
      .catch((err) => showAlert(`Transaction not successful.`));
  };

  return (
    <KeyboardAvoidingView
      flex={1}
      style={Styles.body}
      behavior={(Platform.OS == 'ios' && 'position') || 'padding'}>
      <Header
        navigation={navigation}
        title={'Add Credit'}
        notification
        backButton
      />
      <ConfirmationModal
        modalStructure={modalStructure}
        pressedOk={resetModal}
        pressedNo={resetModal}
        pressedYes={onConfirm}
      />
      <PaypalModal
        showModal={showModal}
        visible={view}
        amountValue={amountValue}
        userInformation={userInformation}
        showAlert={showAlert}
      />
      <ScrollView
        contentContainerStyle={{alignItems: 'center'}}
        showsVerticalScrollIndicator={false}>
        <FlatList
          data={randomAmounts}
          renderItem={mapAmounts}
          numColumns={3}
          scrollEnabled={false}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={Styles.amounts_flatlist_container}
        />
        <View style={{width: wp('92%')}}>
          <View style={Styles.field_main_container}>
            <Text style={Styles.heading_text}>Enter amount</Text>
            <View style={Styles.text_field_container}>
              <TextInput
                style={Styles.textfield}
                value={amountValue}
                placeholder={'Enter Amount (100 to 5,000)'}
                keyboardType={'number-pad'}
                onChangeText={(text) => {
                  setAmountValue(text.replace(/[^0-9]+/, ''));
                }}
                placeholderTextColor={COLORS.color_text}
              />
            </View>
          </View>
          <View style={{}}>
            <CreditContainer
              active={tab}
              onPress={() => {
                !tab && setTab(!tab);
              }}
              subheading={'Card'}
              heading={'VISA / Master'}
            />
            <CreditContainer
              active={!tab}
              onPress={() => {
                tab && setTab(!tab);
              }}
              subheading={'Account Details'}
              heading={'PayPal'}
            />
            {tab ? (
              <View style={{marginTop: 20}}>
                <Text style={Styles.heading_text}>Debit / Credit card</Text>
                <CreditCardInput
                  onChange={(val) => {
                    onChange(val);
                  }}
                  labelStyle={Styles.cardLabelStyle}
                  inputStyle={Styles.inputStyle}
                />
                <View style={Styles.add_credit_button}>
                  <Button
                    text={'Add Credit'}
                    type={'contained'}
                    onPress={() => makeTransaction()}
                    disabled={!amountValue || !cardData ? true : false}
                    contentStyle={Styles.contentStyle}
                    labelStyle={Styles.labelStyle}
                  />
                </View>
              </View>
            ) : (
              <View style={Styles.add_credit_button}>
                <Button
                  text={'Pay by PayPal'}
                  type={'contained'}
                  disabled={amountValue == '' && true}
                  onPress={() => showModal()}
                  contentStyle={Styles.contentStyle}
                  labelStyle={Styles.labelStyle}
                />
              </View>
            )}
          </View>
        </View>
        <View style={{marginBottom: 200}} />
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default AddCredit;
