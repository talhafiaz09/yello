import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
import {
  setCallState,
  setCallData,
  setCallStatus,
  setModalStructure,
} from '../../redux_handler/actions/app';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import useCallhandler from '../../handlers/useCallhandler';
import {callInitiate} from '../../redux_handler/api/index';
import useAsyncStorage from '../../handlers/useAsyncStorage';
import useModal from '../../handlers/useModal';
import ConfirmationModal from '../../components/confirmationmodal/ConfirmationModal';

const OutgoingCall = ({navigation}) => {
  const {Styles, COLORS} = useStyles();

  const dispatch = useDispatch();

  const setCall_State = async (callState) => dispatch(setCallState(callState));
  const setCall_Status = (status) => dispatch(setCallStatus(status));
  const setCall_Data = async (data) => dispatch(setCallData(data));

  const data = useSelector((state) => state.appReducer.callData);
  const configuration = useSelector((state) => state.appReducer.configuration);
  const callState = useSelector((state) => state.appReducer.callState);
  const callStatus = useSelector((state) => state.appReducer.callStatus);
  const accountDetail = useSelector((state) => state.appReducer.accountDetail);
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  const dialtoInformation = useSelector(
    (state) => state.appReducer.dialtoInformation,
  );
  const call = useSelector((state) => state.appReducer.call);

  const {
    setMediacoreConfigurations,
    hangupCall,
    useSpeaker,
    useEarpiece,
    muteCall,
    unMuteCall,
    endCall,
  } = useCallhandler(navigation);

  const {showAlert, modalStructure, resetModal} = useModal();

  const {setCallStartTime} = useAsyncStorage();

  useEffect(() => {
    configuration.username == null &&
    configuration.domain == null &&
    configuration.password == null &&
    !callStatus.active
      ? callInitiate({
          customer_mobile_number: parseInt(userInformation.phoneNumber),
          to_mobile_number: parseInt('' + dialtoInformation.dialedTo),
          country_id: dialtoInformation.countryId,
        })
          .then(async (res) => {
            // console.log(res);
            if (res.data.status_code == 200) {
              await setCallStartTime('0000-00-00 00:00:00');
              res.data.data
                ? res.data.data.min_allowed > 59
                  ? await setCall_Data({
                      ...res.data.data,
                      min_allowed: 59,
                    })
                  : await setCall_Data(res.data.data)
                : null;
              setMediacoreConfigurations(res.data.data);
            } else {
              showAlert(res.data.message);
              setCall_Status({...callStatus, status: 'Terminated'});
              setTimeout(() => {
                setCall_Status({active: false, status: 'Connecting'});
              }, 100);
            }
          })
          .catch((error) => {
            showAlert(error.message);
            setCall_Status({...callStatus, status: 'Terminated'});
            setTimeout(() => {
              setCall_Status({active: false, status: 'Connecting'});
            }, 100);
          })
      : showAlert(`Already in a call.`);
  }, []);
  return (
    <View style={Styles.body}>
      <ConfirmationModal
        modalStructure={modalStructure}
        pressedOk={resetModal}
      />
      <TouchableOpacity
        style={Styles.back_button}
        onPress={() => {
          navigation.goBack();
        }}>
        <MaterialCommunityIcons
          name={'keyboard-backspace'}
          size={30}
          color={COLORS.color_white}
        />
      </TouchableOpacity>
      <View style={Styles.content}>
        <Text style={Styles.outgoing_call_text}>Outgoing call</Text>
        <Text style={Styles.name}>Yello :{')'}</Text>
        <Text style={Styles.number}>
          {dialtoInformation.dialCode != -1 && '+' + dialtoInformation.dialedTo}
        </Text>
        <Text style={Styles.call_status}>{callStatus.status}...</Text>
      </View>
      <View style={Styles.buttons_container}>
        <View style={Styles.function_button}>
          <TouchableOpacity
            style={Styles.buttons}
            onPressIn={() => {
              call != null && call._lastStatusCode == 'PJSIP_SC_OK'
                ? setCall_State({
                    ...callState,
                    microphone: !callState.microphone,
                  }).then((result) =>
                    result.data.microphone ? unMuteCall() : muteCall(),
                  )
                : null;
            }}>
            <MaterialCommunityIcons
              name={callState?.microphone ? 'microphone' : 'microphone-off'}
              size={30}
              color={COLORS.color_white}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={Styles.buttons}
            onPressIn={() => {
              call != null
                ? setCall_State({
                    ...callState,
                    speaker: !callState.speaker,
                  }).then((result) =>
                    result.data.speaker ? useSpeaker() : useEarpiece(),
                  )
                : null;
            }}>
            <MaterialCommunityIcons
              name={!callState?.speaker ? 'volume-off' : 'volume-high'}
              size={30}
              color={COLORS.color_white}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={Styles.button_end}
          onPressIn={() => {
            (call && hangupCall()) ||
              (configuration.username != null &&
                configuration.domain != null &&
                configuration.password != null &&
                accountDetail?._registration?._status
                  ?.toString()
                  ?.toUpperCase() != 'OK' &&
                call == null &&
                endCall());
          }}>
          <MaterialCommunityIcons
            name={'phone-hangup'}
            size={30}
            color={COLORS.color_white}
          />
          <Text style={Styles.end_text}>End call</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default OutgoingCall;
