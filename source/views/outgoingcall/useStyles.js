import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_primary,
    },
    back_button: {position: 'absolute', left: 15, top: 15},
    content: {padding: 30, marginTop: 50},
    outgoing_call_text: {
      color: COLORS.color_white,
      fontFamily: 'Poppins-Regular',
      fontSize: 14,
    },
    name: {
      color: COLORS.color_white,
      fontFamily: 'Poppins-Light',
      fontSize: 30,
      alignSelf: 'center',
      marginTop: 20,
    },
    number: {
      color: COLORS.color_white,
      fontFamily: 'Poppins-SemiBold',
      fontSize: 30,
      alignSelf: 'center',
      marginTop: 20,
    },
    call_status: {
      color: COLORS.color_white,
      fontFamily: 'Poppins-SemiBold',
      fontSize: 12,
      alignSelf: 'center',
      marginTop: 20,
    },
    buttons_container: {
      width: '100%',
      flex: 1,
      justifyContent: 'center',
    },
    function_button: {
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    buttons: {
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.1,
      elevation: 5,
      backgroundColor: COLORS.color_side,
      justifyContent: 'center',
      alignItems: 'center',
      height: 60,
      width: 60,
      borderRadius: 300,
    },
    button_end: {
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.1,
      elevation: 5,
      backgroundColor: COLORS.color_badge,
      justifyContent: 'center',
      alignItems: 'center',
      height: 60,
      padding: 15,
      flexDirection: 'row',
      borderRadius: 300,
      alignSelf: 'center',
      marginTop: 40,
    },
    end_text: {
      fontFamily: 'Poppins-SemiBold',
      color: COLORS.color_white,
      marginLeft: 5,
      fontSize: 14,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
