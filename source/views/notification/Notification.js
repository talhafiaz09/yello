import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  BackHandler,
  TouchableOpacity,
} from 'react-native';
import Styles from './Styles';
import {FAB} from 'react-native-paper';
import Colors from '../../assets/colors/Colors';
import Header from '../../components/header/Header';
import {useDispatch, useSelector} from 'react-redux';
import {updateNotification} from '../../redux_handler/actions/notification';
import NotificationCard from '../../components/notificationcard/NotificationCard';
const Notification = ({navigation}) => {
  const notifications = useSelector(
    (state) => state.notificationReducer.notifications,
  );
  // const [array, setArray] = useState(notifications.reverse());
  // useEffect(() => {
  //   setArray(notifications.reverse());
  // }, [notifications]);
  const dispatch = useDispatch();
  const update_Notification = async (notifications) =>
    dispatch(updateNotification(notifications));
  BackHandler.addEventListener('hardwareBackPress', () => {
    handleNotification();
    navigation.pop();
    return true;
  });
  const handleNotification = () => {
    if (notifications.length > 0) {
      let array = notifications;
      array = array.map((obj) => ({...obj, seen: true}));
      update_Notification(array);
    }
  };
  const mapNotifications = ({item, key}) => {
    return (
      <NotificationCard
        message={item.message}
        title={item.title}
        time={item.time}
        seen={item.seen}
      />
    );
  };
  return (
    <View flex={1}>
      <Header
        navigation={navigation}
        title={'Notifications'}
        backButton
        moveback
        handleNotification={handleNotification}
      />
      <FAB
        style={Styles.fab}
        icon="dialpad"
        color={Colors.color_primary}
        onPress={() => navigation.navigate('Call')}
      />
      {notifications.length == 0 ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontFamily: 'Montserrat-Regular'}}>
            No notifications to show.
          </Text>
        </View>
      ) : (
        <View flex={1}>
          {notifications.filter((data) => data.seen == false).length ==
          0 ? null : (
            <View style={Styles.countContainer}>
              <Text style={Styles.countNotificationText}>
                {notifications.filter((data) => data.seen == false).length} New
                notifications
              </Text>
            </View>
          )}
          <View style={Styles.mainContainer}>
            <FlatList
              data={notifications}
              renderItem={mapNotifications}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
            />
          </View>
        </View>
      )}
    </View>
  );
};
export default Notification;
