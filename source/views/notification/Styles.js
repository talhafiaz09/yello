import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../assets/colors/Colors';
const Styles = StyleSheet.create({
  mainContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    flex: 1,
  },
  fab: {
    position: 'absolute',
    backgroundColor: Colors.color_white,
    right: 25,
    bottom: 30,
    zIndex: 200,
  },
  countContainer: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 20,
    backgroundColor: Colors.color_primary,
    alignSelf: 'baseline',
    alignItems: 'center',
    padding: 6,
    borderRadius: 8,
  },
  countNotificationText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 10,
    color: Colors.color_white,
  },
});
export default Styles;
