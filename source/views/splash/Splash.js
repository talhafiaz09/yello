import React, {useEffect, useState} from 'react';
import {View, Text, Platform} from 'react-native';
import LogoText from '../../assets/images/logo/LogoText';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
import messaging from '@react-native-firebase/messaging';
import {version as app_version} from '../../../package.json';
import {
  setCountries,
  setCallingRates,
  setCallingPackages,
  setCoupons,
  setModalStructure,
  signupCountries,
  setQuestions,
} from '../../redux_handler/actions/app';
import {setuserInformation} from '../../redux_handler/actions/user';
import VersionCheck from 'react-native-version-check';
import ConfirmationModal from '../../components/confirmationmodal/ConfirmationModal';
import {getSplashData} from '../../redux_handler/api/index';
import useAsyncStorage from '../../handlers/useAsyncStorage';
import {callEnd} from '../../redux_handler/api/index';
import {requestPermission} from 'react-native-contacts';
import Loader from '../../components/loader';
import useModal from '../../handlers/useModal';

const Splash = ({navigation}) => {
  const {Styles} = useStyles();

  const {
    getUserInformationAsyncStorage,
    getCallEndDataAsyncStorage,
    removeCallEndDataAsyncStorage,
  } = useAsyncStorage();

  const dispatch = useDispatch();

  const set_Countries = async (countries) => dispatch(setCountries(countries));

  const set_Questions = async (data) => dispatch(setQuestions(data));

  const signup_Countries = async (data) => dispatch(signupCountries(data));

  const setuser_Information = async (info) =>
    dispatch(setuserInformation(info));

  const setCalling_Rates = async (callingRates) =>
    dispatch(setCallingRates(callingRates));

  const setCalling_Packages = async (packages) =>
    dispatch(setCallingPackages(packages));

  const set_Coupons = async (coupons) => dispatch(setCoupons(coupons));

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const {modalStructure, showLoading} = useModal();

  let check = true;

  useEffect(async () => {
    await VersionCheck.getLatestVersion()
      .then((latestVersion) => {
        if (app_version < latestVersion) {
          errorModal('Please update your app to the new version.');
        } else {
          getCallEndDataAsyncStorage().then((res) => {
            res &&
              callEnd(res)
                .then(async (result) => {
                  await removeCallEndDataAsyncStorage();
                })
                .catch((err) => {});
          });
          initializeServices();
        }
      })
      .catch(async (err) => {
        check = false;
        errorModal(err.message);
      });
  }, []);

  const firebaseNotification = async () => {
    const enabled = await messaging().hasPermission();
    if (enabled) {
      getToken();
    } else {
      requestPermission();
    }
  };

  const getToken = async () => {
    const fcmToken = await messaging().getToken();
    setuser_Information({
      ...userInformation,
      device_token: fcmToken,
    });
  };

  const requestPermission = async () => {
    try {
      await messaging.requestPermission();
      getToken();
    } catch (error) {}
  };

  const initializeServices = async () => {
    firebaseNotification();
    await getSplashData()
      .then(async (res) => {
        const {countries, coupons, packages, questions} = res?.data?.data;
        await set_Countries(countries);
        await setCalling_Rates(countries);
        await set_Questions(questions);
        await setCalling_Packages(packages);
        await set_Coupons(coupons);
        const array = [];
        for (const e of countries) {
          e.signup && e.signup == 1 && array.push(e);
        }
        await signup_Countries([...array]);
      })
      .catch(async (error) => {
        errorModal(error.message);
        check = false;
      });
    check == true &&
      (await getUserInformationAsyncStorage().then(async (res) => {
        if (res == null) navigation.replace('GettingStarted');
        else
          setuser_Information(res).then(() =>
            navigation.replace('DrawerNavigation'),
          );
      }));
  };

  const errorModal = (err) => showLoading(err);
  return (
    <View style={Styles.body}>
      <ConfirmationModal modalStructure={modalStructure} />
      <LogoText width={72} height={65} />
      <View style={Styles.loader_container}>
        <Loader />
      </View>
    </View>
  );
};
export default Splash;
