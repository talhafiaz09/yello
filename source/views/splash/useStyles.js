import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_primary,
      justifyContent: 'center',
    },
    loader_container: {
      position: 'absolute',
      bottom: hp('15%'),
      alignSelf: 'center',
    },
  });
  return {
    Styles,
  };
};
export default useStyles;
