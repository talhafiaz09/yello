import React, {useRef} from 'react';
import {View} from 'react-native';
import useStyles from './useStyles';
import Swiper from 'react-native-swiper';
import Register from '../register/Register';
import Opening from '../opening/Opening';
import Otp from '../otp/Otp';
import {useDispatch} from 'react-redux';
import useModal from '../../handlers/useModal';
import ConfirmationModal from '../../components/confirmationmodal/ConfirmationModal';
import usePhoneAuth from '../../handlers/usePhoneAuth';
const GettingStarted = ({navigation}) => {
  const {signInWithPhoneNumber, confirmCode} = usePhoneAuth();

  const {modalStructure, showLoading, resetModal, showAlert} = useModal();

  const scroll = useRef(null);

  const handleScroll = (index) => {
    scroll && scroll.current.scrollTo(index);
  };

  const {Styles} = useStyles();

  return (
    <>
      <ConfirmationModal
        modalStructure={modalStructure}
        pressedOk={resetModal}
      />
      <Swiper
        ref={scroll}
        loop={false}
        scrollEnabled={false}
        dotStyle={Styles.dotStyle}
        activeDotStyle={Styles.dotActive}>
        <Opening handleScroll={handleScroll} />
        <Register
          handleScroll={handleScroll}
          signInWithPhoneNumber={signInWithPhoneNumber}
          showLoading={showLoading}
          resetModal={resetModal}
          showAlert={showAlert}
        />
        <Otp
          handleScroll={handleScroll}
          navigation={navigation}
          confirmCode={confirmCode}
          showLoading={showLoading}
          signInWithPhoneNumber={signInWithPhoneNumber}
          showAlert={showAlert}
          resetModal={resetModal}
        />
      </Swiper>
    </>
  );
};
export default GettingStarted;
