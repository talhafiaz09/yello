import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const dotSimilar = {
    alignSelf: 'center',
    marginBottom: hp('2%'),
    width: hp('2.8%'),
    height: hp('0.8%'),
  };
  const Styles = StyleSheet.create({
    body: {flex: 1},
    dotStyle: {
      ...dotSimilar,
      backgroundColor: COLORS.color_dot_inactive,
    },
    dotActive: {
      ...dotSimilar,
      backgroundColor: COLORS.color_yellow,
    },
  });
  return {
    Styles,
  };
};
export default useStyles;
