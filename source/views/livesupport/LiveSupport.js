import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  FlatList,
  ScrollView,
  KeyboardAvoidingView,
  Keyboard,
} from 'react-native';
import useStyles from './useStyles';
import TextField from '../../components/textfield/TextField';
import Header from '../../components/header/Header';
import {useSelector} from 'react-redux';
import database from '@react-native-firebase/database';

const LiveSupport = ({navigation}) => {
  const scrollViewRef = useRef();

  const {Styles, COLORS} = useStyles();

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const messages = useSelector((state) => state.appReducer.messages);

  const sendMessage = () => {
    if (text) {
      var today = new Date();
      var dd = String(today.getUTCDate()).padStart(2, '0');
      var mm = String(today.getUTCMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getUTCFullYear();
      today = yyyy + '-' + mm + '-' + dd;
      database()
        .ref('/chats/' + userInformation.phoneNumber + '/' + today)
        .push({to: 'admin', message: text});
      setText('');
    }
  };

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        scrollViewRef.current.scrollToEnd({animated: true});
      },
    );
    return () => {
      keyboardDidShowListener.remove();
    };
  }, []);

  const [text, setText] = useState('');

  useEffect(() => {
    scrollViewRef.current.scrollToEnd({animated: true});
  }, [messages]);

  return (
    <KeyboardAvoidingView flex={1} behavior={'padding'} style={Styles.body}>
      <Header
        navigation={navigation}
        title={'Live Support'}
        notification
        menu
      />
      <View flex={1}>
        <ScrollView
          ref={scrollViewRef}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={Styles.message_container}>
          <View
            flex={1}
            style={{
              height: '100%',
              flexDirection: 'column-reverse',
              // backgroundColor: 'red',
            }}>
            {messages.map((value, index) => (
              <View
                key={index}
                style={[
                  Styles.message_view,
                  value?.to?.toLowerCase() == 'client'
                    ? {
                        alignSelf: 'baseline',
                        borderWidth: 1,
                        borderColor: COLORS.color_yellow,
                      }
                    : {
                        alignSelf: 'flex-end',
                        backgroundColor: COLORS.color_yellow,
                      },
                ]}>
                <Text
                  style={{
                    color:
                      value?.to?.toLowerCase() == 'client'
                        ? COLORS.color_yellow
                        : COLORS.color_white,
                    fontSize: 14,
                  }}>
                  {value?.message}
                </Text>
              </View>
            ))}
          </View>
        </ScrollView>
        <View style={Styles.textfield}>
          <TextField
            placeholder={'Enter text'}
            value={text}
            onChangeText={(text) => setText(text)}
            onClick={() => sendMessage()}
          />
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};
export default LiveSupport;
