import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    fab: {
      position: 'absolute',
      backgroundColor: COLORS.color_white,
      right: 25,
      bottom: 30,
      zIndex: 200,
    },
    body: {flex: 1, backgroundColor: COLORS.color_bottom},
    message_container: {
      paddingBottom: hp('12%'),
      width: wp('90%'),
      alignSelf: 'center',
    },
    textfield: {
      position: 'absolute',
      bottom: 0,
      width: '100%',
      height: 'auto',
      backgroundColor: COLORS.color_bottom,
    },
    message_view: {
      maxWidth: '60%',
      padding: 10,
      borderRadius: 10,
      marginTop: 20,
    },
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
