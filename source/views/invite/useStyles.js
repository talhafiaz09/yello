import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';

const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.color_bottom,
    },
    mainContainer: {
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingBottom: 50,
      paddingLeft: 10,
      paddingRight: 10,
      marginBottom: 20,
    },
    code_container: {
      backgroundColor: COLORS.color_textfield,
      borderRadius: 10,
      borderStyle: 'dotted',
      flexDirection: 'row',
      borderColor: COLORS.color_text,
      borderWidth: 1,
      padding: 10,
      marginBottom: 20,
      alignItems: 'center',
    },
    code_text: {
      color: COLORS.color_text,
      fontFamily: 'Poppins-Regular',
      marginRight: 5,
    },
    text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 14,
      color: COLORS.color_text,
      textAlign: 'center',
      marginBottom: 20,
      marginTop: 20,
    },
    divider: {
      width: '80%',
      borderBottomColor: COLORS.color_side,
      borderBottomWidth: 0.5,
    },
    bottom_container: {
      width: '70%',
      flexDirection: 'row',
      backgroundColor: COLORS.color_textfield,
      padding: 10,
      borderRadius: 10,
      justifyContent: 'space-between',
      marginBottom: 30,
    },
    content_text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 10,
      color: COLORS.color_text,
      marginTop: 5,
      textAlign: 'center',
    },
    content_main: {alignItems: 'center'},
    contentStyle: {
      height: hp('5.5%'),
    },
    labelStyle: {fontSize: hp('1.6%')},
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
