import React, {useEffect, useState} from 'react';
import {View, Text, Platform, Share, ScrollView} from 'react-native';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
import Header from '../../components/header/Header';
import Button from '../../components/button/Button';
import {ReferFriend} from '../../assets/images/svg';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Invite = ({navigation}) => {
  const {Styles, COLORS} = useStyles();

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          Platform.OS == 'android'
            ? `https://play.google.com/store/apps/details?id=com.yello_fr\nUse this referal code: ${userInformation.referral_code}`
            : `https://apps.apple.com/us/app/yello-mobile/id1571777509\nUse this referal code: ${userInformation.referral_code}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      console.log(error);
    }
  };

  const Content = ({name, text}) => (
    <View style={Styles.content_main}>
      <MaterialCommunityIcons name={name} size={24} color={COLORS.color_text} />
      <Text style={Styles.content_text}>{text}</Text>
    </View>
  );

  return (
    <View style={Styles.body}>
      <Header
        menu
        navigation={navigation}
        title={'Invite Friends'}
        notification
      />
      <View flex={1}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={Styles.mainContainer}>
          <Text style={Styles.text}>
            Refer to your friend and Get a credit reward
          </Text>
          <ReferFriend />
          <Text style={Styles.text}>
            Share this link with your friend and after they registered, you will
            get credit rewards
          </Text>
          <View style={Styles.code_container}>
            <Text style={Styles.code_text}>
              {userInformation.referral_code}
            </Text>
            <MaterialCommunityIcons
              name={'content-copy'}
              size={24}
              color={COLORS.color_text}
            />
          </View>
          <View style={Styles.divider} />
          <Text style={Styles.text}>To understand how referral works</Text>
          <View style={Styles.bottom_container}>
            <Content name="content-copy" text={'Copy Link'} />
            <Content
              name="check-circle-outline"
              text={`Friends registered\nsuccessfully`}
            />
            <Content name="database" text={'Earn credit\nrewards'} />
          </View>
          <Button
            text={'Refer Friends'}
            type={'contained'}
            onPress={onShare}
            contentStyle={Styles.contentStyle}
            labelStyle={Styles.labelStyle}
          />
        </ScrollView>
      </View>
    </View>
  );
};

export default Invite;
