import {useEffect, useState} from 'react';
import {Platform} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {setModalStructure} from '../redux_handler/actions/app';
export default useModal = () => {
  const dispatch = useDispatch();

  const setModal_Structure = async (structure) =>
    dispatch(setModalStructure(structure));

  const [isOpen, setOpen] = useState(false);

  const modalStructure = useSelector(
    (state) => state.appReducer.modalStructure,
  );

  const toggleOpen = async () => setOpen(!isOpen);

  const showAlert = async (content) => {
    setModal_Structure({
      ...modalStructure,
      visible: true,
      type: 'alert',
      content,
    });
  };

  const showLoading = async (content) => {
    setModal_Structure({
      ...modalStructure,
      visible: true,
      type: 'loading',
      content,
    });
  };

  const showConfirm = async (content, data) => {
    setModal_Structure({
      ...modalStructure,
      visible: true,
      type: 'confirm',
      content,
      data,
    });
  };

  const resetModal = async () => {
    setModal_Structure({
      visible: false,
      action: '',
      type: 'confirm',
      content: '',
      data: {},
      buttonDisabled: false,
    });
    toggleOpen();
  };
  return {
    modalStructure,
    resetModal,
    showAlert,
    showLoading,
    showConfirm,
    isOpen,
    toggleOpen,
  };
};
