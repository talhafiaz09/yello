import {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
export default useAsyncStorage = () => {
  const getUserInformationAsyncStorage = async () =>
    await AsyncStorage.getItem('userInformation')
      .then((value) => JSON.parse(value))
      .catch((error) => console.log(error));
  const setUserInformationAsyncStorage = async (data) => {
    await AsyncStorage.setItem('userInformation', JSON.stringify(data))
      .then(() => console.log('Data saved'))
      .catch((error) => console.log(error));
  };
  const getCallStartTime = async () =>
    await AsyncStorage.getItem('callStartTime')
      .then((value) => value)
      .catch((error) => console.log(error));
  const setCallStartTime = async (data) => {
    await AsyncStorage.setItem('callStartTime', data)
      .then(() => console.log('Data saved'))
      .catch((error) => console.log(error));
  };
  const getCallEndTime = async () =>
    await AsyncStorage.getItem('callEndTime')
      .then((value) => value)
      .catch((error) => console.log(error));
  const setCallEndTime = async (data) => {
    await AsyncStorage.setItem('callEndTime', data)
      .then(() => console.log('Data saved'))
      .catch((error) => console.log(error));
  };
  const removeUserInformationAsyncStorage = async () => {
    await AsyncStorage.removeItem('userInformation')
      .then(() => console.log('Data removed'))
      .catch((error) => console.log(error));
  };
  const removeCallDetails = async () => {
    const keys = ['callStartTime', 'callEndTime'];
    await AsyncStorage.multiRemove(keys)
      .then(() => console.log('Data removed'))
      .catch((error) => console.log(error));
  };
  const setCallEndDataAsyncStorage = async (data) => {
    await AsyncStorage.setItem('callEndData', JSON.stringify(data))
      .then(() => console.log('Data saved'))
      .catch((error) => console.log(error));
  };
  const removeCallEndDataAsyncStorage = async () => {
    await AsyncStorage.removeItem('callEndData')
      .then(() => console.log('Data removed'))
      .catch((error) => console.log(error));
  };
  const getCallEndDataAsyncStorage = async () =>
    await AsyncStorage.getItem('callEndData')
      .then((value) => JSON.parse(value))
      .catch((error) => console.log(error));
  return {
    getUserInformationAsyncStorage,
    setUserInformationAsyncStorage,
    removeUserInformationAsyncStorage,
    setCallStartTime,
    getCallStartTime,
    getCallEndTime,
    setCallEndTime,
    removeCallDetails,
    setCallEndDataAsyncStorage,
    removeCallEndDataAsyncStorage,
    getCallEndDataAsyncStorage,
  };
};
