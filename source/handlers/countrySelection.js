import {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {setDialtoInformation} from '../redux_handler/actions/app';
const canada = [
  '403',
  '587',
  '780',
  '825',
  '236',
  '250',
  '604',
  '672',
  '778',
  '204',
  '431',
  '506',
  '709',
  '782',
  '902',
  '226',
  '249',
  '289',
  '343',
  '365',
  '416',
  '437',
  '519',
  '548',
  '613',
  '647',
  '705',
  '807',
  '905',
  '367',
  '418',
  '438',
  '450',
  '514',
  '579',
  '581',
  '819',
  '873',
  '306',
  '639',
  '867',
];
export default useCountrySelection = () => {
  const dispatch = useDispatch();

  const countries = useSelector((state) => state.appReducer.countries);
  const dialtoInformation = useSelector(
    (state) => state.appReducer.dialtoInformation,
  );

  const setDialto_Information = (dialTo) =>
    dispatch(setDialtoInformation(dialTo));

  const setCountry = async (text) => {
    let result = await countries
      .filter((c) =>
        c.code.toString()[0] == (1 || 7) && text.length <= 11
          ? text.includes(c.code.toString()) && c.code.toString()[0] == text[0]
          : text.substring(0, 4).includes(c.code.toString()) &&
            c.code.toString()[0] == text[0] &&
            c.code.toString()[1] == text[1],
      )
      .reverse();

    if (text[0] == 1 && canada.indexOf(text.slice(1, 4)) > -1)
      result[0] = result[1];

    setDialto_Information({
      ...dialtoInformation,
      countryCode: result.length ? result[0].character_code : '',
      dialCode: result.length ? result[0].code : -1,
      countryId: result.length ? result[0].id : -1,
    });
  };
  return {setCountry};
};
