import Contacts from 'react-native-contacts';
var CONTACT_LIST = [];
export const fetchAllContacts = async () => {
  await Contacts.getAll().then((response) => {
    CONTACT_LIST = response.map((index) =>
      Object.keys(index)
        .filter((attribute) =>
          ['givenName', 'phoneNumbers'].includes(attribute),
        )
        .reduce((object, key) => {
          key === 'phoneNumbers'
            ? (object[key] = index.phoneNumbers.map((val) =>
                Object.keys(val)
                  .map((att) => {
                    return att === 'number'
                      ? val[att].replace(/[^0-9]/gi, '')
                      : '';
                  })
                  .join(''),
              ))
            : (object[key] = index[key]);
          return object;
        }, {}),
    );
  });
  return CONTACT_LIST.sort(compare);
};
const compare = (a, b) => {
  if (a?.givenName?.toUpperCase() < b?.givenName?.toUpperCase()) {
    return -1;
  }
  if (a?.givenName?.toUpperCase() > b?.givenName?.toUpperCase()) {
    return 1;
  }
  return 0;
};
