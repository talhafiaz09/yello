import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import BackgroundTimer from 'react-native-background-timer';
import {
  setCallState,
  setConfiguration,
  clearDialedNumber,
  setCallStatus,
  setAccountDetail,
  setCall,
  setCallData,
} from '../redux_handler/actions/app';
import useAsyncStorage from '../handlers/useAsyncStorage';
import {callEnd} from '../redux_handler/api/index';
let check = false;
let states = null;
export default useCallhandler = (navigation) => {
  const endpoint = useSelector((state) => state.appReducer.endpoint);
  const dispatch = useDispatch();
  const setCall_State = async (call_state) =>
    dispatch(setCallState(call_state));
  const clearDialed_Number = async () => dispatch(clearDialedNumber());
  const setCall_Status = async (status) => dispatch(setCallStatus(status));
  const setAccount_Detail = async (detail) =>
    dispatch(setAccountDetail(detail));
  const set_Configuration = async (configuration) =>
    dispatch(setConfiguration(configuration));
  const set_Call = async (call) => dispatch(setCall(call));
  const setCall_Data = async (data) => dispatch(setCallData(data));
  const configuration = useSelector((state) => state.appReducer.configuration);
  const call = useSelector((state) => state.appReducer.call);
  const accountDetail = useSelector((state) => state.appReducer.accountDetail);
  const callStatus = useSelector((state) => state.appReducer.callStatus);
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  const dialtoInformation = useSelector(
    (state) => state.appReducer.dialtoInformation,
  );
  const data = useSelector((state) => state.appReducer.callData);
  const {
    setCallStartTime,
    getCallStartTime,
    setCallEndTime,
    getCallEndTime,
    removeCallDetails,
    setCallEndDataAsyncStorage,
  } = useAsyncStorage();
  useEffect(() => {
    accountDetail != null && !callStatus.active ? initiateCall() : null;
  }, [accountDetail]);
  useEffect(() => {
    // console.log(configuration);
    configuration.username != null &&
    configuration.domain != null &&
    configuration.password != null &&
    !callStatus.active
      ? startServices()
      : null;
  }, [configuration]);
  useEffect(() => {
    callStatus.status == 'Terminated'
      ? clearDialed_Number().then(() =>
          setTimeout(() => {
            navigation.pop();
          }, 200),
        )
      : null;
  }, [callStatus]);
  const setMediacoreConfigurations = async (data) => {
    await set_Configuration({
      ...configuration,
      username: data.media_user.toString(),
      domain: data.media_ip.toString(),
      password: data.media_pass.toString(),
    });
  };
  const startServices = async () => {
    states = await endpoint.start();
    let {accounts, calls, settings, connectivity} = states;
    for (const id in accounts) {
      if (accounts.hasOwnProperty(id)) {
        endpoint.deleteAccount(accounts[id]);
      }
    }
    endpoint.changeCodecSettings({
      'G722/16000/1': 0,
      'G7221/16000/1': 0,
      'G7221/32000/1': 0,
      'GSM/8000/1': 128, //
      'PCMA/8000/1': 128, //
      'PCMU/8000/1': 128, //
      'iLBC/8000/1': 0,
      'opus/48000/2': 0,
      'speex/16000/1': 0,
      'speex/32000/1': 0,
      'speex/8000/1': 0,
    });
    endpoint.on('registration_changed', async (account) => {
      console.log('Account state: ', account);
      if (
        account._registration._statusText.toUpperCase() === 'OK' &&
        !callStatus.active
      ) {
        setAccount_Detail(account);
      } else {
        callEnd({
          call_id: data.call_id,
          package_id: data.cp_id,
          call_start: '0000-00-00 00:00:00',
          call_end: '0000-00-00 00:00:00',
          duration: 0,
          mobile_number: parseInt(userInformation.phoneNumber),
        })
          .then((res) => console.log(res.data))
          .catch((err) => {
            console.log(err);
            setCallEndDataAsyncStorage({
              call_id: data.call_id,
              package_id: data.cp_id,
              call_start: '0000-00-00 00:00:00',
              call_end: '0000-00-00 00:00:00',
              duration: 0,
              mobile_number: parseInt(userInformation.phoneNumber),
            });
          });
        for (const id in accounts) {
          if (accounts.hasOwnProperty(id)) {
            endpoint.deleteAccount(accounts[id]);
          }
        }
        setCall_Status({...callStatus, status: 'Terminated'});
        set_Configuration({
          ...configuration,
          username: null,
          domain: null,
          password: null,
        });
        set_Call(null);
        setCall_State({microphone: true, speaker: false}).then(() =>
          setTimeout(() => {
            setAccount_Detail(null);
            setCall_Status({active: false, status: 'Connecting'});
          }, 100),
        );
        // endpoint.stop();
        await endpoint.removeAllListeners();
      }
    });
    endpoint.on('connectivity_changed', (online) => {
      console.log('Connectivity changed: ', online);
    });
    endpoint.on('call_received', (call) => {
      console.log('Call received: ', call);
    });
    endpoint.on('call_changed', (call) => {
      console.log('Call: ', call);
      set_Call(call);
      if (
        call._lastStatusCode == 'PJSIP_SC_RINGING' ||
        call._lastStatusCode == 'PJSIP_SC_PROGRESS'
      ) {
        setCall_Status({active: true, status: 'Ringing'});
      } else if (call._lastStatusCode == 'PJSIP_SC_OK' && check == false) {
        check = true;
        setCall_Status({active: true, status: 'In call'});
        BackgroundTimer.runBackgroundTimer(() => {
          hangupCallListener(call);
        }, data.min_allowed * 60000);
        getCallStartTime().then((res) =>
          res === '0000-00-00 00:00:00'
            ? setCallStartTime(new Date().toString())
            : null,
        );
      }
    });
    endpoint.on('call_terminated', async (call) => {
      console.log('Terminated: ', call);
      await setCall_Status({...callStatus, status: 'Terminated'});
      BackgroundTimer.stopBackgroundTimer();
      let st = null;
      let et = null;
      let timer = 0;
      await getCallStartTime().then(
        (res) => (st = res),
        // console.log(res);
      );
      if (st === '0000-00-00 00:00:00') {
        et = '0000-00-00 00:00:00';
        // await setCallEndTime('0000-00-00 00:00:00');
        // await getCallEndTime().then((res) => (et = res));
      } else {
        st = new Date(st);
        await setCallEndTime(new Date().toString());
        await getCallEndTime().then((res) => (et = new Date(res)));
        timer = (et - st) / 60000;
        st =
          st.getFullYear() +
          '-' +
          parseInt(st.getMonth() + 1) +
          '-' +
          st.getDate() +
          ' ' +
          st.getHours() +
          ':' +
          st.getMinutes() +
          ':' +
          st.getSeconds();
        et =
          et.getFullYear() +
          '-' +
          parseInt(et.getMonth() + 1) +
          '-' +
          et.getDate() +
          ' ' +
          et.getHours() +
          ':' +
          et.getMinutes() +
          ':' +
          et.getSeconds();
      }
      callEnd({
        call_id: data.call_id,
        package_id: data.cp_id,
        call_start: st,
        call_end: et,
        duration: timer,
        mobile_number: parseInt(userInformation.phoneNumber),
      })
        .then((res) => console.log(res.data))
        .catch((err) => {
          console.log(err);
          setCallEndDataAsyncStorage({
            call_id: data.call_id,
            package_id: data.cp_id,
            call_start: st,
            call_end: et,
            duration: timer,
            mobile_number: parseInt(userInformation.phoneNumber),
          });
        });
      for (const id in accounts) {
        if (accounts.hasOwnProperty(id)) {
          endpoint.deleteAccount(accounts[id]);
        }
      }
      await set_Configuration({
        ...configuration,
        username: null,
        domain: null,
        password: null,
      });
      await set_Call(null);
      await setCall_Data(null);
      await setCall_State({microphone: true, speaker: false}).then(() =>
        setTimeout(async () => {
          await setAccount_Detail(null);
          await setCall_Status({active: false, status: 'Connecting'});
          await removeCallDetails();
          // clear?.clearAppCache(() => {
          //   console.log('Cache cleared.');
          // });
          // callEnd();
        }, 100),
      );
      // endpoint.stop();
      await endpoint.removeAllListeners();
      check = false;
    });
    createAccount();
  };

  const createAccount = async () => {
    await endpoint
      .createAccount(configuration)
      .then((account) => {
        console.log('Initial account state: ', account);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const initiateCall = async () => {
    let options = {
      headers: {
        'P-Assserted-Identity': 'Header example',
        'X-UA': 'CCP Mobile',
      },
    };
    endpoint
      .makeCall(
        accountDetail,
        '' +
          dialtoInformation.dialCode.toString() +
          dialtoInformation.dialedTo
            .toString()
            .replace(dialtoInformation.dialCode, ''),
        {
          audioCount: 1,
          videoCount: 0,
        },
        null,
      )
      .then(() => setCall_Status({...callStatus, active: true}));
  };
  const useSpeaker = () => {
    endpoint.useSpeaker(call);
  };
  const useEarpiece = () => {
    endpoint.useEarpiece(call);
  };
  const muteCall = () => {
    endpoint.muteCall(call);
  };
  const unMuteCall = () => {
    endpoint.unMuteCall(call);
  };
  const hangupCallListener = async (livecall) => {
    await endpoint.hangupCall(livecall);
  };
  const hangupCall = async () => {
    await endpoint.hangupCall(call);
  };
  const endCall = async () => {
    await endpoint.removeAllListeners();
    let {accounts, calls, settings, connectivity} = states;
    // console.log('here');
    await setCall_Status({...callStatus, status: 'Terminated'});
    callEnd({
      call_id: data.call_id,
      package_id: data.cp_id,
      call_start: '0000-00-00 00:00:00',
      call_end: '0000-00-00 00:00:00',
      duration: 0,
      mobile_number: parseInt(userInformation.phoneNumber),
    })
      .then((res) => console.log(res.data))
      .catch((err) => {
        console.log(err);
        setCallEndDataAsyncStorage({
          call_id: data.call_id,
          package_id: data.cp_id,
          call_start: '0000-00-00 00:00:00',
          call_end: '0000-00-00 00:00:00',
          duration: 0,
          mobile_number: parseInt(userInformation.phoneNumber),
        });
      });
    for (const id in accounts) {
      if (accounts.hasOwnProperty(id)) {
        endpoint.deleteAccount(accounts[id]);
      }
    }
    for (const id in calls) {
      if (calls.hasOwnProperty(id)) {
        endpoint.hangupCall(accounts[id]);
      }
    }
    await set_Configuration({
      ...configuration,
      username: null,
      domain: null,
      password: null,
    });
    await set_Call(null);
    await setCall_Data(null);
    await setCall_State({microphone: true, speaker: false}).then(() =>
      setTimeout(async () => {
        await setAccount_Detail(null);
        await removeCallDetails();
        await setCall_Status({active: false, status: 'Connecting'});
      }, 200),
    );
    check = false;
  };
  return {
    hangupCall,
    setMediacoreConfigurations,
    startServices,
    useSpeaker,
    useEarpiece,
    muteCall,
    unMuteCall,
    endCall,
  };
};
