import {useEffect, useState} from 'react';
import {Switch} from 'react-native';
import {Platform} from 'react-native';
import {request, check, PERMISSIONS, RESULTS} from 'react-native-permissions';
var PERMISSION_CHECK = false;
export default usePermissions = () => {
  const checkingPermissions = async (DATA) => {
    Platform.OS === 'ios'
      ? await permissions('IOS', DATA)
      : await permissions('ANDROID', DATA);
    return PERMISSION_CHECK;
  };

  const permissions = async (OS, PERMISSIONS_VALUES) => {
    await check(PERMISSIONS[OS][PERMISSIONS_VALUES])
      .then(async (result) => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            console.log(
              'This feature is not available (on this device / in this context)',
            );
            PERMISSION_CHECK = false;
            break;
          case RESULTS.DENIED:
            console.log(
              'The permission has not been requested / is denied but requestable',
            );
            await request(PERMISSIONS[OS][PERMISSIONS_VALUES]).then(
              (result) => {
                if (result === 'granted') {
                  PERMISSION_CHECK = true;
                } else {
                  PERMISSION_CHECK = false;
                }
              },
            );
            break;
          case RESULTS.LIMITED:
            console.log('The permission is limited: some actions are possible');
            PERMISSION_CHECK = true;
            break;
          case RESULTS.GRANTED:
            console.log('The permission is granted');
            PERMISSION_CHECK = true;
            break;
          case RESULTS.BLOCKED:
            console.log('The permission is denied and not requestable anymore');
            PERMISSION_CHECK = false;
            break;
        }
      })
      .catch((error) => {});
  };
  return {checkingPermissions};
};
