import {useEffect, useState} from 'react';
import {Platform} from 'react-native';
import auth from '@react-native-firebase/auth';
import {useDispatch, useSelector} from 'react-redux';
export default usePhoneAuth = () => {
  const [confirm, setConfirm] = useState(null);

  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const signInWithPhoneNumber = async (phoneNumber) => {
    const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
    setConfirm(confirmation);
  };

  const confirmCode = async (code) => {
    try {
      const result = await confirm.confirm(code);
      return result;
    } catch (error) {
      console.log(error);
      return error;
    }
  };
  return {signInWithPhoneNumber, confirmCode};
};
