import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, Platform} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import useStyles from './useStyles';
// import Colors from '../../assets/colors/Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import usePermissions from '../../handlers/usePermissions';
import {useDispatch, useSelector} from 'react-redux';
import {
  setDialedNumber,
  deleteDialedNumber,
  clearDialedNumber,
  setDialtoInformation,
  setNumberHistory,
} from '../../redux_handler/actions/app';
import {callEnd} from '../../redux_handler/api/index';
import useAsyncStorage from '../../handlers/useAsyncStorage';
let permissions = false;
const Dialer = ({navigation}) => {
  const {Styles, COLORS} = useStyles();

  const dispatch = useDispatch();

  const setDialed_Number = (number) => dispatch(setDialedNumber(number));
  const setDialto_Information = async (info) =>
    dispatch(setDialtoInformation(info));
  const deleteDialed_Number = () => dispatch(deleteDialedNumber());
  const clearDialed_Number = () => dispatch(clearDialedNumber());
  const setNumber_History = async (number) =>
    dispatch(setNumberHistory(number));

  const callState = useSelector((state) => state.appReducer.callState);
  const dialPad = useSelector((state) => state.appReducer.dialPad);
  const accountDetail = useSelector((state) => state.appReducer.accountDetail);
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  const dialtoInformation = useSelector(
    (state) => state.appReducer.dialtoInformation,
  );
  const numberHistory = useSelector((state) => state.appReducer.numberHistory);
  const {checkingPermissions} = usePermissions();
  const dialedNumber = useSelector((state) => state.appReducer.dialedNumber);

  const checkPermissions = async () => {
    await checkingPermissions(
      Platform.OS == 'ios' ? 'CONTACTS' : 'CALL_PHONE',
    ).then((PERMISSION) => {
      PERMISSION ? (permissions = true) : (permissions = false);
    });
    await androidPermissions();
    return permissions;
  };

  const androidPermissions = async () => {
    if (Platform.OS == 'android') {
      await checkingPermissions('RECORD_AUDIO').then((PERMISSION) => {
        PERMISSION ? (permissions = true) : (permissions = false);
      });
    }
  };

  const {getCallEndDataAsyncStorage, removeCallEndDataAsyncStorage} =
    useAsyncStorage();

  const checkHistory = async () => {
    let check = false;
    await getCallEndDataAsyncStorage()
      .then(async (res) => {
        res != null
          ? await callEnd(res)
              .then(async (result) => {
                console.log(res);
                await removeCallEndDataAsyncStorage();
                check = false;
              })
              .catch((err) => {
                console.log(err);
                check = true;
              })
          : null;
      })
      .catch((err) => {
        console.log(err);
        check = true;
      });
    return check;
  };

  const mapKeys = ({item, key}) => {
    return (
      <View style={Styles.itemList} key={key}>
        <TouchableOpacity
          style={Styles.button}
          onPress={async () => {
            item.key && setDialed_Number(item.key);
            item.icon === 'backspace' && deleteDialed_Number();
            item.icon === 'phone-in-talk'
              ? (await checkPermissions())
                ? accountDetail == null
                  ? dialedNumber.length == 0
                    ? setDialed_Number(numberHistory)
                    : checkHistory() == true
                    ? console.log('True')
                    : setNumber_History(dialedNumber).then(() =>
                        setDialto_Information({
                          ...dialtoInformation,
                          dialedFrom: userInformation.phoneNumber,
                          dialedTo: dialedNumber,
                        }).then(() => navigation.navigate('OutgoingCall')),
                      )
                  : navigation.navigate('OutgoingCall')
                : null
              : null;
          }}
          delayLongPress={500}
          onLongPress={() => item.icon === 'backspace' && clearDialed_Number()}>
          {item.key ? (
            <Text style={Styles.digit_text}>{item.key}</Text>
          ) : (
            <MaterialCommunityIcons
              name={item.icon}
              size={30}
              color={COLORS.color_yellow}
            />
          )}
        </TouchableOpacity>
      </View>
    );
  };
  return (
    <View style={Styles.body}>
      <FlatList
        numColumns={3}
        data={dialPad}
        renderItem={mapKeys}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        style={Styles.dialer_key_flatlist}
        scrollEnabled={false}
      />
    </View>
  );
};

export default Dialer;
