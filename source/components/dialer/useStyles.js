import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {height: '100%', marginTop: hp('2%')},
    dialer_key_flatlist: {width: '100%'},
    itemList: {
      height: hp('100%') / 8,
      flex: 1,
      width: wp('100%') / 3,
      alignItems: 'center',
    },
    button: {
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.1,
      elevation: 5,
      backgroundColor: COLORS.color_textfield,
      justifyContent: 'center',
      alignItems: 'center',
      height: 70,
      width: 70,
      borderRadius: 300,
    },
    digit_text: {
      fontSize: 22,
      fontFamily: 'Poppins-Medium',
      color: COLORS.color_text,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
