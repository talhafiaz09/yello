import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    main_container: {flex: 1},
    header_container: {
      height: hp('25%'),
      alignItems: 'center',
      justifyContent: 'center',
    },
    header_text: {
      fontSize: 16,
      fontFamily: 'Poppins-Light',
      color: COLORS.color_white,
      marginTop: 10,
    },
    green_t: {
      position: 'absolute',
    },
    container_divider: {
      height: 20,
      backgroundColor: COLORS.color_blur,
    },
    exit_icon_container: {
      position: 'absolute',
      right: 20,
      top: 20,
      zIndex: 200,
    },
    bottom_container: {
      paddingTop: 10,
      alignItems: 'center',
      paddingBottom: hp('5%'),
    },
    bottom_text: {
      fontFamily: 'Poppins-SemiBold',
      color: COLORS.color_side,
      marginLeft: 10,
    },
    share_bottom_text: {
      fontSize: 16,
      fontFamily: 'Poppins-SemiBold',
      color: COLORS.color_side,
      marginTop: 20,
    },
    btn: {
      width: '90%',
      padding: 10,
      justifyContent: 'center',
      backgroundColor: COLORS.color_white,
      borderRadius: 10,
      flexDirection: 'row',
    },
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
