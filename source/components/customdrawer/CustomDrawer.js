import React, {useEffect, useState} from 'react';
import {Share, Text, View, TouchableOpacity, Platform} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import {useDispatch, useSelector} from 'react-redux';
import ProfileAvatar from '../../assets/images/profileavatar/ProfileAvatar';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import useStyles from './useStyles';
import {FullGreenT} from '../../assets/images/svg';
import {logout} from '../../redux_handler/actions/app';
import auth from '@react-native-firebase/auth';
import useAsyncStorage from '../../handlers/useAsyncStorage';
import ConfirmationModal from '../../components/confirmationmodal/ConfirmationModal';
import useModal from '../../handlers/useModal';

const CustomDrawer = (props, {navigation}) => {
  const {Styles, COLORS, hp} = useStyles();

  const {removeUserInformationAsyncStorage} = useAsyncStorage();

  const {
    isOpen,
    toggleOpen,
    modalStructure,
    showLoading,
    resetModal,
    showAlert,
    showConfirm,
  } = useModal();

  const log_out = async () => dispatch(logout());

  const onConfirmLogOut = async () => {
    await showLoading(`Please wait...`).then(async () => {
      await auth()
        .signOut()
        .then((res) => {
          removeUserInformationAsyncStorage().then(async () => {
            await log_out();
            props.navigation.replace('Splash');
          });
        })
        .catch((err) => showAlert('Logout failed.\nError: ' + err.message));
    });
    await toggleOpen();
  };

  const dispatch = useDispatch();

  const configuration = useSelector((state) => state.appReducer.configuration);
  const callStatus = useSelector((state) => state.appReducer.callStatus);
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const ASPECT_RATIO = hp('10%');

  return (
    <View style={Styles.main_container}>
      {isOpen && (
        <ConfirmationModal
          modalStructure={modalStructure}
          pressedOk={async () => {
            await toggleOpen();
            resetModal();
          }}
          pressedNo={async () => {
            await toggleOpen();
            resetModal();
          }}
          pressedYes={async () => {
            onConfirmLogOut();
          }}
        />
      )}
      <TouchableOpacity
        style={Styles.exit_icon_container}
        onPress={() => {
          props.navigation.closeDrawer();
        }}>
        <MaterialCommunityIcons
          name={'close-circle-outline'}
          size={20}
          color={COLORS.color_white}
        />
      </TouchableOpacity>
      <View style={Styles.green_t}>
        <FullGreenT height={ASPECT_RATIO * 1.16} width={ASPECT_RATIO} />
      </View>
      <View style={Styles.header_container}>
        <ProfileAvatar />
        <Text style={Styles.header_text}>
          {'+' + userInformation.phoneNumber}
        </Text>
      </View>
      <View style={Styles.container_divider} />
      <DrawerContentScrollView {...props}>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
      <View style={Styles.bottom_container}>
        <TouchableOpacity
          style={Styles.btn}
          onPress={async () => {
            await toggleOpen();
            if (
              configuration.username == null &&
              configuration.domain == null &&
              configuration.password == null &&
              !callStatus.active
            ) {
              // ? showConfirm(`Do you want to logout of the app?`)
              showConfirm(`Do you want to logout of the app?`);
            } else {
              showAlert('Please end the on going call first.');
            }
          }}>
          <MaterialCommunityIcons
            name={'exit-to-app'}
            size={20}
            color={COLORS.color_yellow}
          />
          <Text style={Styles.bottom_text}>Logout</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default CustomDrawer;
