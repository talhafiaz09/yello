import {StyleSheet, Platform, StatusBar} from 'react-native';
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : 0;
const Styles = StyleSheet.create({
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
});
export default Styles;
