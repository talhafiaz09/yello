import React from 'react';
import {View, StatusBar} from 'react-native';
import Styles from './Styles';
const CustomStatusBar = ({backgroundColor, ...props}) => (
  <View style={[Styles.statusBar, {backgroundColor: backgroundColor}]}>
    <StatusBar
      backgroundColor={backgroundColor}
      {...props}
      barStyle={'light-content'}
    />
  </View>
);
export default CustomStatusBar;
