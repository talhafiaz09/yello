import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    main_container: {justifyContent: 'center'},
    icon: {
      position: 'absolute',
      alignSelf: 'center',
      flexWrap: 'wrap',
      right: wp('9.5%'),
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
