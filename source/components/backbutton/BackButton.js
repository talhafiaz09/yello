import React, {useEffect, useState} from 'react';
import {Modal, View, Text, TouchableOpacity} from 'react-native';
import {BackButtonBG} from '../../assets/images/svg';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import useStyles from './useStyles';
const BackButton = ({onPress}) => {
  const {Styles, COLORS} = useStyles();
  const ASPECT_RATIO = hp('6%');

  return (
    <View>
      <View style={Styles.main_container}>
        <BackButtonBG width={ASPECT_RATIO * 1.8} height={ASPECT_RATIO} />
        <TouchableOpacity onPress={onPress} style={Styles.icon}>
          <MaterialCommunityIcons
            name={'keyboard-backspace'}
            size={ASPECT_RATIO / 2}
            color={COLORS.color_white}
            onPress={onPress}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default BackButton;
