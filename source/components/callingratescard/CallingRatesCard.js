import React from 'react';
import Flag from 'react-native-flags';
import {Text, View, TouchableOpacity} from 'react-native';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
const MenuCard = ({country_code, country_name, rate}) => {
  const {Styles, COLORS} = useStyles();

  const currency_rates = useSelector(
    (state) => state.userReducer.currency_rates,
  );
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  return (
    <View style={Styles.card_container}>
      <View style={Styles.card_container_left}>
        <Flag code={country_code} size={32} />
        <Text style={Styles.country_name_text}>{country_name}</Text>
      </View>
      <View style={Styles.card_container_right}>
        <Text style={Styles.rate_text}>
          {currency_rates[userInformation.currency]?.symbol +
            ' ' +
            (
              parseFloat(rate) * currency_rates[userInformation.currency]?.rate
            ).toFixed(4) +
            ' '}
          / min
        </Text>
      </View>
    </View>
  );
};
export default MenuCard;
