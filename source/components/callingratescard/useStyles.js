import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    card_container: {
      alignSelf: 'center',
      backgroundColor: COLORS.color_textfield,
      padding: 8,
      width: '95%',
      flexDirection: 'row',
      justifyContent: 'space-between',
      borderRadius: 10,
      marginBottom: 10,
    },
    card_container_left: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    card_container_right: {
      justifyContent: 'center',
      alignItems: 'flex-end',
    },
    country_name_text: {
      marginLeft: 20,
      fontSize: 12,
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
    },
    rate_text: {
      fontSize: 12,
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
