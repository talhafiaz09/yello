import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    card_container: {
      height: 84,
      // width: '45%',
      flex: 1,
      alignItems: 'center',
      flexDirection: 'row',
      borderRadius: 10,
      shadowColor: COLORS.color_yellow,
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.5,
      borderLeftWidth: 10,
      borderLeftColor: COLORS.color_yellow,
      elevation: 5,
      backgroundColor: COLORS.color_textfield,
      paddingLeft: 10,
      paddingRight: 10,
    },
    text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 13,
      color: COLORS.color_text,
      marginLeft: 5,
    },
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
