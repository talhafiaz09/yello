import * as React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import useStyles from './useStyles';

const MenuCard = ({text, children, navigation}) => {
  const {Styles, COLORS} = useStyles();

  return (
    <TouchableOpacity
      style={Styles.card_container}
      onPress={() => {
        navigation.navigate(text);
      }}>
      {children}
      <Text style={Styles.text}>{text}</Text>
    </TouchableOpacity>
  );
};

export default MenuCard;
