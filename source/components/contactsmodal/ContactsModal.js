import React, {useEffect, useState} from 'react';
import {Modal, View, Animated, Text, FlatList, Platform} from 'react-native';
import {TouchableOpacity} from 'react-native';
import useStyles from './useStyles';
import SearchBar from '../../components/searchbar/SearchBar';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Avatar} from 'react-native-paper';
import Loader from '../../components/loader';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import {
  changeContactsModalVisibility,
  setContactList,
  setDialedNumber,
  clearDialedNumber,
} from '../../redux_handler/actions/app';
import usePermissions from '../../handlers/usePermissions';
import {fetchAllContacts} from '../../handlers/functions';
let animatedValue = new Animated.Value(hp('90%'));
const ContactsModal = ({navigation}) => {
  const [check, setCheck] = useState(false);

  const {Styles, COLORS} = useStyles();

  const dispatch = useDispatch();

  const changeContactsModal_Visibility = () =>
    dispatch(changeContactsModalVisibility());
  const setContact_List = async (contactList) =>
    dispatch(setContactList(contactList));
  const setDialed_Number = (number) => dispatch(setDialedNumber(number));
  const clearDialed_Number = () => dispatch(clearDialedNumber());

  const visible = useSelector(
    (state) => state.appReducer.contactsModalVisibility,
  );
  const contactList = useSelector((state) => state.appReducer.contactList);
  const searchBarText = useSelector((state) => state.appReducer.searchBarText);

  const {checkingPermissions} = usePermissions();

  useEffect(() => {
    // console.log(contactList);
    const unsubscribe = navigation.addListener('focus', () => {
      checkingPermissions(
        Platform.OS == 'ios' ? 'CONTACTS' : 'READ_CONTACTS',
      ).then(async (PERMISSION) => {
        PERMISSION
          ? await fetchAllContacts().then(async (CONTACT_LIST) => {
              // console.log(CONTACT_LIST);
              await setContact_List(CONTACT_LIST);
            })
          : null;
      });
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    if (visible) {
      Animated.timing(animatedValue, {
        toValue: hp('0%'),
        duration: 500,
        useNativeDriver: true,
      }).start();
      setTimeout(() => {
        setCheck(true);
      }, 1000);
    }
  }, [visible]);

  const closeToast = () => {
    Animated.timing(animatedValue, {
      toValue: hp('90%'),
      duration: 500,
      useNativeDriver: true,
    }).start();
    setTimeout(() => {
      changeContactsModal_Visibility();
      setCheck(false);
    }, 500);
  };

  const mapContacts = ({item, key}) => {
    return item?.phoneNumbers.map((number, index) => {
      return (
        <TouchableOpacity
          key={index}
          style={Styles.coutries_main_container}
          onPress={() => {
            clearDialed_Number();

            setDialed_Number(number.replace(/^0+/, ''));

            setTimeout(() => {
              closeToast();
            }, 200);
          }}>
          <View style={Styles.avatar_container}>
            <Avatar.Text
              size={30}
              label={item.givenName.substring(0, 2)}
              style={Styles.avatar}
              color={COLORS.color_white}
            />
          </View>
          <View style={Styles.number_main_container}>
            <Text style={Styles.coutries_name_text}>{item.givenName}</Text>
            <Text key={key} style={Styles.coutries_code_text}>
              {number}
            </Text>
          </View>
        </TouchableOpacity>
      );
    });
  };

  return (
    <Modal animationType="fade" transparent={true} visible={visible}>
      <View style={Styles.modal_main_container}>
        <Animated.View
          style={[
            {
              transform: [{translateY: animatedValue}],
            },
            Styles.modal_animated_container,
          ]}>
          <TouchableOpacity
            onPress={() => {
              closeToast();
            }}
            style={{alignSelf: 'flex-end', marginBottom: 10}}>
            <MaterialCommunityIcons
              name="close"
              size={24}
              color={COLORS.color_text}
            />
          </TouchableOpacity>
          <SearchBar placeholder={'Search by name'} />
          {check ? (
            <FlatList
              data={
                searchBarText == ''
                  ? contactList
                  : contactList.filter((index) => {
                      // console.log(index);
                      return index.givenName
                        .toUpperCase()
                        .replace(/\./g, '')
                        .includes(
                          searchBarText.toUpperCase().replace(/\./g, ''),
                        );
                    })
              }
              renderItem={mapContacts}
              contentContainerStyle={{flexGrow: 1}}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              styles={Styles.modal_flatlist_container}
              ListEmptyComponent={() => (
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    flex: 1,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat-Regular',
                    }}>
                    Nothing to show
                  </Text>
                </View>
              )}
            />
          ) : (
            <View style={Styles.loader_container}>
              <Loader />
            </View>
          )}
        </Animated.View>
      </View>
    </Modal>
  );
};
export default ContactsModal;
