import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    card_container: {
      marginTop: 10,
      marginBottom: 10,
      alignSelf: 'center',
      width: '95%',
      height: 'auto',
      shadowColor: COLORS.color_primary,
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.1,
      elevation: 2,
      padding: 10,
      backgroundColor: COLORS.color_textfield,
      borderRadius: 10,
    },
    detail_container: {
      width: '100%',
      alignSelf: 'center',
      marginBottom: 10,
    },
    button_container: {width: '50%', alignSelf: 'center', marginTop: 10},
    package_title: {
      fontSize: 14,
      color: COLORS.color_text,
      fontFamily: 'Poppins-SemiBold',
      marginBottom: 5,
      alignSelf: 'center',
    },

    detail_container_body: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: 10,
    },

    text_icon_container: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 12,
      marginLeft: 5,
      color: COLORS.color_text,
    },
    contentStyle: {
      height: hp('5.5%'),
    },
    labelStyle: {fontSize: hp('1.6%')},
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
