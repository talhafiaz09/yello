import React from 'react';
import Flag from 'react-native-flags';
import {Text, View, TouchableOpacity} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Button from '../button/Button';
import useStyles from './useStyles';
const CouponsCard = ({data, onPress}) => {
  const {Styles, COLORS} = useStyles();

  return (
    <View style={Styles.card_container}>
      <View style={Styles.detail_container}>
        <Text style={Styles.package_title}>{data.name.toUpperCase()}</Text>
        <View style={Styles.detail_container_body}>
          <View style={Styles.text_icon_container}>
            <MaterialCommunityIcons
              name="trophy"
              size={16}
              color={COLORS.color_yellow}
            />
            <Text style={Styles.text}>
              {parseFloat(data.amount).toFixed(2)}
            </Text>
          </View>
          <View style={Styles.text_icon_container}>
            <MaterialCommunityIcons
              name={data.life == null && 'timer-off-outline'}
              size={16}
              color={COLORS.color_yellow}
            />
            <Text style={Styles.text}>{data.expiry_date}</Text>
          </View>
        </View>
      </View>
      <View style={Styles.button_container}>
        <Button
          text={'Buy Coupon'}
          type={'contained'}
          onPress={() => onPress(data)}
          contentStyle={Styles.contentStyle}
          labelStyle={Styles.labelStyle}
        />
      </View>
    </View>
  );
};
export default CouponsCard;
