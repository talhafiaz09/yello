import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    card_container: {
      marginTop: 10,
      marginBottom: 10,
      alignSelf: 'center',
      width: '95%',
      height: 'auto',
      shadowColor: COLORS.color_primary,
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.1,
      elevation: 2,
      padding: 10,
      backgroundColor: COLORS.color_textfield,
      borderRadius: 10,
    },
    header_container: {
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 20,
      flexDirection: 'row',
    },
    country_name: {
      fontSize: 12,
      color: COLORS.color_text,
      fontFamily: 'Poppins-SemiBold',
      marginLeft: 10,
    },
    detail_container: {
      width: '90%',
      alignSelf: 'center',
      marginBottom: 20,
    },
    button_container: {width: '60%', alignSelf: 'center', marginBottom: 10},
    package_title: {
      fontSize: 14,
      color: COLORS.color_text,
      fontFamily: 'Poppins-SemiBold',
      marginBottom: 5,
    },

    detail_container_body: {
      flex: 1,
      flexDirection: 'row',
    },
    contentStyle: {
      height: hp('5.5%'),
    },
    labelStyle: {fontSize: hp('1.6%')},
    text_icon_container: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    },
    text: {
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
      fontSize: 12,
      marginLeft: 5,
    },
    s_e_time_text: {
      fontFamily: 'Poppins-SemiBold',
      fontSize: 12,
      marginLeft: 5,
      color: COLORS.color_text,
      marginTop: 10,
    },
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
