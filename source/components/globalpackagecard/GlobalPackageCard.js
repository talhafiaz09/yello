import React from 'react';
import Flag from 'react-native-flags';
import {Text, View, TouchableOpacity} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Button from '../../components/button/Button';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
const GlobalPackageCard = ({data, showConfirm, userInformation}) => {
  const {Styles, COLORS} = useStyles();

  const currency_rates = useSelector(
    (state) => state.userReducer.currency_rates,
  );

  return (
    <View style={Styles.card_container}>
      <View style={Styles.header_container}>
        <Flag code={data.country_code} size={64} />
        <Text style={Styles.country_name}>{data.country_name}</Text>
      </View>
      <View style={Styles.detail_container}>
        <Text style={Styles.package_title}>{data.name}</Text>
        <View style={Styles.detail_container_body}>
          <View style={Styles.text_icon_container}>
            <FontAwesome name="money" size={16} color={COLORS.color_yellow} />
            <Text style={Styles.text}>
              {currency_rates[userInformation.currency]?.symbol +
                ' ' +
                parseFloat(data.amount).toFixed(2) *
                  currency_rates[userInformation.currency]?.rate}
            </Text>
          </View>
          <View style={Styles.text_icon_container}>
            <MaterialCommunityIcons
              name={data.life == null ? 'timer-outline' : 'heart'}
              size={16}
              color={COLORS.color_yellow}
            />
            <View>
              <Text style={Styles.s_e_time_text}>
                {data.life == null ? 'Package start' : 'Life:'}
              </Text>
              <Text style={Styles.text}>
                {data.life == null ? data.start_date : data.life}
              </Text>
            </View>
          </View>
        </View>
        <View style={Styles.detail_container_body}>
          <View style={Styles.text_icon_container}>
            <MaterialCommunityIcons
              name={'timelapse'}
              size={16}
              color={COLORS.color_yellow}
            />
            <Text style={Styles.text}>
              {parseFloat(data.total_min).toFixed(2)} minutes
            </Text>
          </View>
          <View style={Styles.text_icon_container}>
            <MaterialCommunityIcons
              name={data.life == null ? 'timer-off-outline' : null}
              size={16}
              color={COLORS.color_yellow}
            />
            <View>
              <Text style={Styles.s_e_time_text}>
                {data.life == null ? 'Package end' : null}
              </Text>
              <Text style={Styles.text}>{data.end_date}</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={Styles.button_container}>
        <Button
          text={'Buy Package'}
          type={'contained'}
          onPress={() =>
            showConfirm(`Do you want to buy package \n"${data.name}"?`, {
              id: data.id,
            })
          }
          contentStyle={Styles.contentStyle}
          labelStyle={Styles.labelStyle}
        />
      </View>
    </View>
  );
};
export default GlobalPackageCard;
