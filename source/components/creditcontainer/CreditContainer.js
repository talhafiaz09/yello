import React, {useEffect} from 'react';
import Flag from 'react-native-flags';
import {Text, View, TouchableOpacity} from 'react-native';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
import {MasterCard, PayPal} from '../../assets/images/svg';
const CreditContainer = ({active, onPress, heading, subheading}) => {
  const {Styles, COLORS} = useStyles();

  return (
    <TouchableOpacity style={Styles.main(active)} onPress={onPress}>
      {heading == 'PayPal' ? (
        <PayPal color={active ? COLORS.color_yellow : COLORS.color_text} />
      ) : (
        <MasterCard color={active ? COLORS.color_yellow : COLORS.color_text} />
      )}
      <View flex={1} style={Styles.text_container}>
        <Text style={Styles.heading(active)}>{heading}</Text>
        <Text style={Styles.sub_heading(active)}>{subheading}</Text>
      </View>
    </TouchableOpacity>
  );
};
export default CreditContainer;
