import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    main: (active) => {
      return {
        marginTop: 20,
        height: hp('6%'),
        borderRadius: 10,
        borderWidth: 1,
        borderColor: active ? COLORS.color_yellow : COLORS.color_text,
        backgroundColor: active ? COLORS.color_side : COLORS.color_bottom,
        alignItems: 'center',
        paddingLeft: 10,
        flexDirection: 'row',
      };
    },
    text_container: {marginLeft: 10},
    heading: (active) => {
      return {
        color: active ? COLORS.color_yellow : COLORS.color_text,
        fontFamily: 'Poppins-Medium',
        fontSize: 12,
      };
    },
    sub_heading: (active) => {
      return {
        color: active ? COLORS.color_yellow : COLORS.color_text,
        fontFamily: 'Poppins-Medium',
        fontSize: 10,
      };
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
