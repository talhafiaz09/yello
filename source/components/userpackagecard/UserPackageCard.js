import React from 'react';
import Flag from 'react-native-flags';
import {Text, View} from 'react-native';
import useStyles from './useStyles';
import {useSelector} from 'react-redux';
const UserPackageCard = ({
  title,
  total_min,
  end_date,
  country,
  countrycode,
  amount,
  remaining_minutes,
}) => {
  const {Styles, COLORS} = useStyles();

  const currency_rates = useSelector(
    (state) => state.userReducer.currency_rates,
  );
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  return (
    <View style={Styles.card_container}>
      <View style={Styles.header_container}>
        <View style={Styles.header_container_left}>
          <Text style={Styles.header_text}>{title.toUpperCase()}</Text>
          <Text style={[Styles.secondary_text, {color: COLORS.color_text}]}>
            {country}
          </Text>
        </View>
        <View style={Styles.header_container_right}>
          <Text style={Styles.header_text}>
            {currency_rates[userInformation.currency]?.symbol +
              ' ' +
              (
                parseFloat(amount) *
                currency_rates[userInformation.currency]?.rate
              ).toFixed(2)}
          </Text>
          <Text style={[Styles.secondary_text, {color: COLORS.color_text}]}>
            Recharged
          </Text>
        </View>
      </View>
      <View style={Styles.body_container}>
        <View style={Styles.body_container_right}>
          <View style={Styles.minutes_text_container}>
            <Text style={Styles.minutes_text}>
              {parseFloat(total_min).toFixed(2)}
            </Text>
            <Text style={Styles.secondary_text}>total minutes</Text>
          </View>
          <View style={Styles.minutes_text_container}>
            <Text style={[Styles.minutes_text, {}]}>
              {parseFloat(remaining_minutes).toFixed(2)}
            </Text>
            <Text style={[Styles.secondary_text, {}]}>remaining minutes</Text>
          </View>
          <Text style={Styles.secondary_text}>Valid till: {end_date}</Text>
        </View>
        <Flag code={countrycode} size={64} />
      </View>
    </View>
  );
};
export default UserPackageCard;
