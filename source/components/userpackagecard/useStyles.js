import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    card_container: {
      marginTop: 10,
      marginBottom: 10,
      // height: 125,
      alignSelf: 'center',
      width: '95%',
      shadowColor: COLORS.color_primary,
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.1,
      elevation: 2,
      padding: 10,
      backgroundColor: COLORS.color_textfield,
      borderRadius: 10,
    },
    header_container: {flexDirection: 'row'},
    header_container_left: {flex: 0.5},
    header_container_right: {
      flex: 0.5,
      alignItems: 'flex-end',
    },
    header_text: {
      fontFamily: 'Poppins-Bold',
      fontSize: 14,
      color: COLORS.color_yellow,
    },
    secondary_text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 10,
      color: COLORS.color_white,
    },
    body_container: {
      flexDirection: 'row',
      marginTop: 10,
      justifyContent: 'space-between',
      backgroundColor: COLORS.color_yellow,
      borderRadius: 10,
      padding: 5,
    },
    body_container_right: {marginTop: 2},
    minutes_text_container: {
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 5,
    },
    minutes_text: {
      fontFamily: 'Poppins-Bold',
      fontSize: 12,
      color: COLORS.color_white,
      marginRight: 4,
    },
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
