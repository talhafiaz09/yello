import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    modal_main_container: {
      flex: 1,
      width: '100%',
      backgroundColor: COLORS.color_blur,
      paddingLeft: wp('10%'),
      paddingRight: wp('10%'),
      justifyContent: 'center',
    },
    body_container: {
      justifyContent: 'center',
      minHeight: 160,
      width: '100%',
      backgroundColor: COLORS.color_primary,
      padding: 10,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: COLORS.color_border,
    },
    closeicon: {
      alignSelf: 'flex-end',
    },
    loading_container: {
      marginBottom: 10,
      alignItems: 'center',
    },
    lottie_view: {
      height: hp('12%'),
    },
    modal_text: {
      textAlign: 'center',
      fontFamily: 'Poppins-Regular',
      fontSize: 14,
      textAlign: 'center',
      color: COLORS.color_white,
    },
    button_container: {
      flex: 1,
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    okay_button_container: {width: '50%', alignSelf: 'center'},
    okay_button: {height: hp('5%'), width: '100%'},
    confirmation_buttons: {flexDirection: 'row'},
    okay_btn_label_style: {fontSize: hp('1.6%')},
    confirmation_buttons_adjustment: {
      flex: 1,
      paddingLeft: 10,
      paddingRight: 10,
    },
  });
  return {
    Styles,
  };
};
export default useStyles;
