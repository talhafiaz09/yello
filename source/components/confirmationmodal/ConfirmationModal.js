import React, {useEffect, useState} from 'react';
import {Modal, View, Text, TouchableOpacity} from 'react-native';
import useStyles from './useStyles';
import Button from '../button/Button';
import {useDispatch, useSelector} from 'react-redux';
import {setModalStructure} from '../../redux_handler/actions/app';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LottieView from 'lottie-react-native';

const ConfirmationModal = ({
  modalStructure,
  pressedOk,
  pressedYes,
  pressedNo,
}) => {
  const {Styles} = useStyles();

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalStructure?.visible}>
      <View style={Styles.modal_main_container}>
        <View style={Styles.body_container}>
          <View style={Styles.loading_container}>
            {modalStructure?.type == 'loading' && (
              <LottieView
                source={require('../../assets/loader/DancingLoader.json')}
                style={Styles.lottie_view}
                autoPlay
                loop
              />
            )}
            <Text style={Styles.modal_text}>{modalStructure?.content}</Text>
          </View>
          {modalStructure?.type == 'confirm' && (
            <View style={Styles.confirmation_buttons}>
              <View style={Styles.confirmation_buttons_adjustment}>
                <Button
                  text={'Yes'}
                  type={'contained'}
                  disabled={modalStructure.buttonDisabled}
                  onPress={pressedYes}
                  contentStyle={Styles.okay_button}
                  labelStyle={Styles.okay_btn_label_style}
                />
              </View>
              <View style={Styles.confirmation_buttons_adjustment}>
                <Button
                  text={'No'}
                  type={'contained'}
                  disabled={modalStructure.buttonDisabled}
                  onPress={pressedNo}
                  contentStyle={Styles.okay_button}
                  labelStyle={Styles.okay_btn_label_style}
                />
              </View>
            </View>
          )}
          {modalStructure?.type == 'alert' && (
            <View style={Styles.okay_button_container}>
              <Button
                text={'Okay'}
                type={'contained'}
                onPress={pressedOk}
                contentStyle={Styles.okay_button}
                labelStyle={Styles.okay_btn_label_style}
              />
            </View>
          )}
        </View>
      </View>
    </Modal>
  );
};
export default ConfirmationModal;
