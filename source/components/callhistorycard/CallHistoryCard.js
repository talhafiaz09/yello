import React, {useEffect} from 'react';
import Flag from 'react-native-flags';
import {Text, View, TouchableOpacity} from 'react-native';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
const CallHistoryCard = ({data, numberSet}) => {
  const currency_rates = useSelector(
    (state) => state.userReducer.currency_rates,
  );
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  const {Styles, COLORS} = useStyles();

  return (
    <View style={Styles.card_container}>
      <TouchableOpacity
        style={Styles.card_container_left}
        onPress={() => numberSet(data.to_num.toString())}>
        <Text style={Styles.upper_text}>+{data.to_num}</Text>
        <Text style={Styles.lower_text}>
          {data.start_time == '0000-00-00 00:00:00'
            ? 'Not connected'
            : data.start_time}
        </Text>
      </TouchableOpacity>
      <View style={Styles.card_container_right}>
        <Text style={Styles.upper_text}>
          {data.tot_charge
            ? currency_rates[userInformation.currency]?.symbol +
              ' ' +
              parseFloat(data.tot_charge) *
                currency_rates[userInformation.currency]?.rate
            : ''}
        </Text>
        <Text style={Styles.lower_text}>{data.duration_min} mins</Text>
      </View>
    </View>
  );
};
export default CallHistoryCard;
