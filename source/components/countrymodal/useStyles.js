import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    modal_main_container: {
      width: wp('100%'),
      height: hp('100%'),
      backgroundColor: COLORS.color_blur,
      justifyContent: 'flex-end',
    },
    modal_animated_container: {
      width: wp('100%'),
      height: hp('90%'),
      backgroundColor: COLORS.color_bottom,
      padding: 10,
    },

    NTS_text: {
      fontFamily: 'Poppins-Italic',
      color: COLORS.color_text,
    },

    NTS_container: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
    },

    flag_container: {flexDirection: 'row', alignItems: 'center'},
    modal_flatlist_container: {width: '100%'},
    extra_space: {marginBottom: 50},
    coutries_main_container: {
      flexDirection: 'row',
      height: hp('6%'),
      backgroundColor: COLORS.color_textfield,
      alignItems: 'center',
      justifyContent: 'space-between',
      marginBottom: 10,
      borderRadius: 10,
      paddingLeft: 10,
      paddingRight: 10,
    },
    coutries_name_text: {
      textAlign: 'center',
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
      fontSize: 14,
      marginLeft: 10,
    },
    coutries_code_text: {
      textAlign: 'center',
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
      fontSize: 14,
    },
    loader_container: {flex: 1, justifyContent: 'center'},
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
