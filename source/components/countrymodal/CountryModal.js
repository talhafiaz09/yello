import React, {useEffect, useState} from 'react';
import {
  Modal,
  View,
  Animated,
  Text,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import useStyles from './useStyles';
import Flag from 'react-native-flags';
import SearchBar from '../../components/searchbar/SearchBar';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import {changeCountryModalVisibility} from '../../redux_handler/actions/app';
import {setuserInformation} from '../../redux_handler/actions/user';
import {setDialtoInformation} from '../../redux_handler/actions/app';
import Loader from '../../components/loader';
import {setDialedNumber} from '../../redux_handler/actions/app';

let animatedValue = new Animated.Value(hp('90%'));

const CountryModal = ({userInfo, dialtoInfo, signupCountry}) => {
  const {Styles, COLORS} = useStyles();

  const dispatch = useDispatch();

  const changeCountryModal_Visibility = () =>
    dispatch(changeCountryModalVisibility());
  const setuser_Information = (user) => dispatch(setuserInformation(user));
  const setDialto_Information = (dialTo) =>
    dispatch(setDialtoInformation(dialTo));
  const setDialed_Number = (number) => dispatch(setDialedNumber(number));

  const visible = useSelector(
    (state) => state.appReducer.countryModalVisibility,
  );
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  const dialtoInformation = useSelector(
    (state) => state.appReducer.dialtoInformation,
  );
  const countries = useSelector((state) => state.appReducer.countries);
  const signupCountries = useSelector(
    (state) => state.appReducer.signupCountries,
  );
  const searchBarText = useSelector((state) => state.appReducer.searchBarText);

  const [cs, setCS] = useState([]);
  const [check, setCheck] = useState(false);

  useEffect(() => {
    setCS(signupCountry ? signupCountries : countries);
  }, []);

  useEffect(() => {
    if (visible) {
      Animated.timing(animatedValue, {
        toValue: hp('0%'),
        duration: 500,
        useNativeDriver: true,
      }).start();
      setTimeout(() => {
        setCheck(true);
      }, 1000);
    }
  });

  const closeToast = () => {
    Animated.timing(animatedValue, {
      toValue: hp('90%'),
      duration: 500,
      useNativeDriver: true,
    }).start();
    setTimeout(() => {
      changeCountryModal_Visibility();
      setCheck(false);
    }, 500);
  };

  const mapCountries = ({item, key}) => {
    return (
      <TouchableOpacity
        key={key}
        style={Styles.coutries_main_container}
        onPress={() => {
          userInfo &&
            setuser_Information({
              ...userInformation,
              countryCode: item.character_code,
              dialCode: item.code,
              countryId: item.id,
            });

          if (dialtoInfo) {
            setDialto_Information({
              ...dialtoInformation,
              countryCode: item.character_code,
              dialCode: item.code,
              countryId: item.id,
            });
            setDialed_Number(item.code.toString());
          }

          setTimeout(() => {
            closeToast();
          }, 200);
        }}>
        <View style={Styles.flag_container}>
          <Flag code={item.character_code} size={24} />
          <Text style={Styles.coutries_name_text}>{item.name}</Text>
        </View>
        <Text style={Styles.coutries_code_text}>{item.code}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <Modal animationType="fade" transparent={true} visible={visible}>
      <View style={Styles.modal_main_container}>
        <Animated.View
          style={[
            {
              transform: [{translateY: animatedValue}],
            },
            Styles.modal_animated_container,
          ]}>
          <TouchableOpacity
            onPress={() => {
              closeToast();
            }}
            style={{alignSelf: 'flex-end', marginBottom: 10}}>
            <MaterialCommunityIcons
              name="close"
              size={24}
              color={COLORS.color_text}
            />
          </TouchableOpacity>
          <SearchBar placeholder={'Search by country name or code'} />
          {check ? (
            <FlatList
              data={
                searchBarText === ''
                  ? cs
                  : cs.filter((index) => {
                      return (
                        index.name
                          .toUpperCase()
                          .replace(/\./g, '')
                          .includes(
                            searchBarText?.toUpperCase().replace(/\./g, ''),
                          ) ||
                        index.code
                          .toString()
                          .includes(
                            searchBarText?.toUpperCase().replace(/\./g, ''),
                          )
                      );
                    })
              }
              renderItem={mapCountries}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{flexGrow: 1}}
              styles={Styles.modal_flatlist_container}
              ListEmptyComponent={() => (
                <View style={Styles.NTS_container}>
                  <Text style={Styles.NTS_text}>Nothing to show</Text>
                </View>
              )}
            />
          ) : (
            <View style={Styles.loader_container}>
              <Loader />
            </View>
          )}
        </Animated.View>
      </View>
    </Modal>
  );
};
export default CountryModal;
