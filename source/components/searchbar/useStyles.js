import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    text_field_main_container: {
      width: wp('80%'),
      height: hp('6%'),
      backgroundColor: COLORS.color_textfield,
      alignItems: 'center',
      flexDirection: 'row',
      alignSelf: 'center',
      borderRadius: 10,
      marginBottom: 10,
      padding: 2,
      justifyContent: 'space-between',
      paddingLeft: 10,
      paddingRight: 10,
    },
    icon_container: {
      alignItems: 'center',
      justifyContent: 'center',
      padding: hp('1%'),
      backgroundColor: COLORS.color_primary,
    },

    text_field: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingRight: 5,
      fontFamily: 'Poppins-Regular',
      fontSize: 14,
      flex: 1,
      color: COLORS.color_text,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
