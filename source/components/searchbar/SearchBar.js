import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TextInput,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import useStyles from './useStyles';
import Colors from '../../assets/colors/Colors';
import {useDispatch, useSelector} from 'react-redux';
import {setSearchBarText} from '../../redux_handler/actions/app';
const SearchBar = ({placeholder}) => {
  const dispatch = useDispatch();

  const {Styles, COLORS} = useStyles();

  const setSearchBar_Text = (text) => dispatch(setSearchBarText(text));

  const searchBarText = useSelector((state) => state.appReducer.searchBarText);

  useEffect(() => {
    setSearchBar_Text('');
  }, []);

  return (
    <View style={Styles.text_field_main_container}>
      <TextInput
        style={Styles.text_field}
        value={searchBarText}
        placeholder={placeholder}
        onChangeText={(text) => setSearchBar_Text(text)}
        placeholderTextColor={COLORS.color_text}
      />
      <View style={[Styles.icon_container]}>
        <MaterialCommunityIcons
          name="magnify"
          size={20}
          color={COLORS.color_white}
        />
      </View>
    </View>
  );
};
export default SearchBar;
