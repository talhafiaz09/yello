import React, {useEffect} from 'react';
import {View} from 'react-native';
import useStyles from './useStyles';
import * as Animatable from 'react-native-animatable';
import {
  LoadingParticleYellow,
  LoadingParticleRed,
  LoadingParticleGreen,
} from '../../assets/images/loader_particles';
const Loader = ({}) => {
  const {Styles} = useStyles();
  return (
    <View style={Styles.main}>
      <Animatable.View
        animation={'jello'}
        iterationCount="infinite"
        duration={3000}>
        <LoadingParticleYellow />
      </Animatable.View>

      <Animatable.View
        animation={'bounce'}
        iterationCount="infinite"
        duration={2000}>
        <LoadingParticleGreen />
      </Animatable.View>

      <Animatable.View
        animation={'jello'}
        iterationCount="infinite"
        duration={3000}>
        <LoadingParticleRed />
      </Animatable.View>
    </View>
  );
};

export default Loader;
