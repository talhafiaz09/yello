import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    main: {
      flexDirection: 'row',
      width: 62,
      justifyContent: 'space-between',
      alignSelf: 'center',
      bottom: 0,
    },
  });
  return {
    Styles,
  };
};
export default useStyles;
