import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import Colors from '../../assets/colors/Colors';
import Styles from './Styles';
const NotificationCard = ({message, title, time, seen}) => {
  return (
    <View style={Styles.mainContainer}>
      <View style={Styles.notificationContent}>
        <View
          style={[
            Styles.border,
            {borderColor: !seen ? Colors.color_primary : Colors.color_text},
          ]}></View>
        <View style={Styles.textContainer}>
          <Text
            style={[
              {color: !seen ? Colors.color_primary : Colors.color_text},
              Styles.heading,
            ]}>
            {title}
          </Text>
          <Text style={Styles.bodyText}>{message}</Text>
          <Text
            style={[
              {
                color: !seen ? Colors.color_primary : Colors.color_text,
                marginTop: 5,
              },
              Styles.heading,
            ]}>
            {parseFloat(new Date().getTime() / 36000 - time / 36000).toFixed(
              0,
            ) + ' minutes ago'}
          </Text>
        </View>
      </View>
    </View>
  );
};
export default NotificationCard;
