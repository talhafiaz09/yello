import {StyleSheet, Platform, Keyboard} from 'react-native';
import Colors from '../../assets/colors/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const Styles = StyleSheet.create({
  mainContainer: {
    marginBottom: 10,
  },
  notificationContent: {flexDirection: 'row'},
  border: {
    borderWidth: 2,
    borderRadius: 5,
  },
  textContainer: {
    paddingLeft: 5,
    paddingTop: 2,
    paddingBottom: 2,
  },
  heading: {fontSize: 10, fontFamily: 'Montserrat-Medium', marginBottom: 5},
  bodyText: {fontSize: 10, fontFamily: 'Montserrat-Light', lineHeight: 14},
});
export default Styles;
