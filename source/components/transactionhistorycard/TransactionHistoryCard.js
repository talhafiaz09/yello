import React from 'react';
import Flag from 'react-native-flags';
import {Text, View, TouchableOpacity} from 'react-native';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
const TransactionHistoryCard = ({data}) => {
  const {Styles, COLORS} = useStyles();

  const currency_rates = useSelector(
    (state) => state.userReducer.currency_rates,
  );
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );

  return (
    <View style={Styles.card_container}>
      <View style={Styles.card_container_left}>
        <Text style={Styles.upper_text}>{data.transaction_type}</Text>
        <Text style={Styles.lower_text}>
          {data.package_purchased ? data.package_purchased : null}
          {data.transfer_to ? '+' + data.transfer_to : null}
        </Text>
      </View>
      <View style={Styles.card_container_right}>
        <Text style={Styles.upper_text}>
          {currency_rates[userInformation.currency]?.symbol +
            ' ' +
            parseFloat(data.amount) *
              currency_rates[userInformation.currency]?.rate}
        </Text>
        <Text style={Styles.lower_text}>{data.created_on}</Text>
      </View>
    </View>
  );
};
export default TransactionHistoryCard;
