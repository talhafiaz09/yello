import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    card_container: {
      flexDirection: 'row',
      padding: 10,
      backgroundColor: COLORS.color_textfield,
      marginTop: 10,
      justifyContent: 'space-between',
      borderRadius: 10,
    },
    card_container_left: {},
    card_container_right: {
      alignItems: 'flex-end',
    },
    upper_text: {
      fontFamily: 'Poppins-Regular',
      fontSize: 12,
      marginBottom: 10,
      color: COLORS.color_text,
    },
    lower_text: {
      fontFamily: 'Poppins-Light',
      fontSize: 8,
      color: COLORS.color_text,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
