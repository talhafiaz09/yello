import React, {useEffect, useState} from 'react';
import {View, Modal, ScrollView} from 'react-native';
import {WebView} from 'react-native-webview';
import useStyles from './useStyles';
import {TouchableOpacity} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const PaypalModal = ({
  visible,
  showModal,
  amountValue,
  userInformation,
  showAlert,
}) => {
  const {Styles, COLORS} = useStyles();

  return (
    <Modal visible={visible}>
      <View flex={1} style={Styles.body}>
        <TouchableOpacity style={Styles.icon_container} onPress={showModal}>
          <MaterialCommunityIcons
            name="close"
            size={25}
            color={COLORS.color_side}
          />
        </TouchableOpacity>
        <View style={Styles.webview}>
          <WebView
            source={{
              uri:
                `https://retail.iconnectglobal.com/paypal/` +
                amountValue +
                `/` +
                userInformation.currency +
                `/` +
                userInformation.phoneNumber,
            }}
            onNavigationStateChange={async (data) => {
              let c = data.url.split('/');
              c = c[c.length - 1];
              if (c === 'failed' || c === 'success') {
                showAlert(`Transaction ` + c);
                showModal(false);
              }
            }}
          />
        </View>
      </View>
    </Modal>
  );
};

export default PaypalModal;
