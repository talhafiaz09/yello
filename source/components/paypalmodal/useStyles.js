import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    body: {justifyContent: 'flex-end'},
    icon_container: {position: 'absolute', top: 40, right: 10, zIndex: 1000},
    webview: {
      flex: 1,
      marginTop: 20,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
