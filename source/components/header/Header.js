import React from 'react';
import useStyles from './useStyles';
import {Appbar, Badge} from 'react-native-paper';
import {useDispatch, useSelector} from 'react-redux';
import {changeResendOtpVisibility} from '../../redux_handler/actions/app';
import {HalfRedT, HalfGreenT} from '../../assets/images/svg';
import {Keyboard, View} from 'react-native';
const Header = ({
  navigation,
  otpViewHandler,
  handleScroll,
  backButton,
  screenindex,
  menu,
  notification,
  badge,
  moveback,
  style,
  title,
  handleNotification,
}) => {
  const dispatch = useDispatch();
  const changeResendOtp_Visibility = () =>
    dispatch(changeResendOtpVisibility());

  const notifications = useSelector(
    (state) => state.notificationReducer.notifications,
  );

  const {Styles, COLORS, hp} = useStyles();

  const ASPECT_RATIO = hp('3%');

  return (
    <Appbar.Header style={[Styles.header_style, style]}>
      <View style={Styles.background_s}>
        <HalfRedT opacity={0.5} />
        <HalfGreenT
          opacity={0.5}
          height={ASPECT_RATIO * 2.2}
          width={ASPECT_RATIO * 2.5}
        />
      </View>
      {backButton && (
        <Appbar.Action
          icon="keyboard-backspace"
          size={20}
          color={COLORS.color_white}
          style={Styles.alignment}
          onPress={() => {
            // handleNotification ? handleNotification() : null;
            navigation.pop();
          }}
        />
      )}
      {menu && (
        <Appbar.Action
          icon="format-align-left"
          size={20}
          color={COLORS.color_white}
          style={Styles.alignment}
          onPress={() => {
            navigation.openDrawer();
            Keyboard.dismiss();
          }}
        />
      )}
      <Appbar.Content
        title={title}
        titleStyle={Styles.title}
        style={Styles.alignment}
        // color={Colors.color_text}
      />
      {/* //Notification */}
      <Appbar.Action />
      {/* {notification ? (
        <Appbar.Action
          size={20}
          // style={[Styles.alignment, {backgroundColor: COLORS.color_white}]}
          // color={COLORS.color_green}
          // icon={
          //   notifications.filter((data) => data.seen == false).length > 0
          //     ? 'bell-ring'
          //     : 'bell'
          // }
          onPress={() => {
            // navigation.navigate('Notification');
          }}
        />
      ) : (
        <Appbar.Action />
      )} */}
      {/* {badge &&
      notifications.filter((data) => data.seen == false).length > 0 ? (
        <Badge style={Styles.badge} size={12}>
          {notifications.filter((data) => data.seen == false).length}
        </Badge>
      ) : null} */}
    </Appbar.Header>
  );
};
export default Header;
