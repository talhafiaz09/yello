import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    header_style: {
      backgroundColor: COLORS.color_primary,
      height: hp('8%'),
      width: '100%',
      elevation: 5,
    },
    alignment: {alignSelf: 'flex-end'},
    badge: {
      position: 'absolute',
      top: 10,
      right: 12,
      // backgroundColor: Colors.color_badge,
    },
    title: {
      alignSelf: 'center',
      fontSize: 16,
      color: COLORS.color_white,
      alignContent: 'flex-end',
      fontFamily: 'Poppins-Regular',
      marginBottom: hp('1.2%'),
    },
    background_s: {
      position: 'absolute',
      top: 0,
      left: 0,
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '102.2%',
    },
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
