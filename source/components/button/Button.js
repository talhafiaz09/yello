import React, {useEffect} from 'react';
import {Text} from 'react-native';
import {Button as RNPButton} from 'react-native-paper';
import useStyles from './useStyles';

const Button = ({
  text,
  icon,
  type,
  onPress,
  disabled,
  labelStyle,
  contentStyle,
  style,
}) => {
  const {Styles} = useStyles();

  return (
    <RNPButton
      disabled={disabled ? disabled : false}
      icon={icon}
      mode={type}
      onPress={onPress && onPress}
      style={[Styles.button_background, {...style}]}
      contentStyle={[Styles.button_container, {...contentStyle}]}
      uppercase={false}
      labelStyle={[Styles.label_style, {...labelStyle}]}>
      <Text>{text}</Text>
    </RNPButton>
  );
};

export default Button;
