import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    button_container: {
      height: 60,
      backgroundColor: COLORS.color_yellow,
      justifyContent: 'center',
    },
    label_style: {fontFamily: 'Poppins-Regular', fontSize: 18},
    button_background: {
      backgroundColor: COLORS.color_yellow,
      borderRadius: 10,
      shadowColor: COLORS.color_shadow, // IOS
      shadowOffset: {height: 0, width: 0}, // IOS
      shadowOpacity: 1, // IOS
      shadowRadius: 1, //IOS
      elevation: 2, // Android
    },
  });
  return {
    Styles,
  };
};
export default useStyles;
