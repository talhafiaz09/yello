import React, {useEffect, useState} from 'react';
import {Modal, View, Text, TouchableOpacity} from 'react-native';
import {BackButtonBG} from '../../assets/images/svg';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {SemiCircle, HalfGreenT, FullRedT} from '../../assets/images/svg';
import BackButton from '../../components/backbutton/BackButton';
import useStyles from './useStyles';
const TopContainer = ({onPress, children}) => {
  const {Styles, COLORS} = useStyles();

  const ASPECT_RATIO_GREEN_T = hp('6%');
  const ASPECT_RATIO_RED_T = hp('6%');

  return (
    <View style={Styles.top_container}>
      <SemiCircle style={Styles.half_C} width={hp('8%')} height={hp('8%')} />
      <View style={Styles.svg_view}>
        <BackButton onPress={onPress} />
        <HalfGreenT
          style={Styles.half_G}
          width={ASPECT_RATIO_GREEN_T}
          height={ASPECT_RATIO_GREEN_T * 2.2}
        />
      </View>
      {children}
      <View style={Styles.redT_container}>
        <FullRedT
          width={ASPECT_RATIO_RED_T}
          height={ASPECT_RATIO_RED_T * 1.2}
        />
      </View>
    </View>
  );
};
export default TopContainer;
