import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    top_container: {
      height: hp('30%'),
      backgroundColor: COLORS.color_top,
      width: '100%',
    },
    svg_view: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    half_C: {
      position: 'absolute',
      top: '7%',
      left: '30%',
      zIndex: 10,
    },
    half_G: {},
    redT_container: {position: 'absolute', bottom: 0, right: 0},
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
