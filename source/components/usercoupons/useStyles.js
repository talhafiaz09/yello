import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    card_container: {
      marginTop: 10,
      marginBottom: 10,
      alignSelf: 'center',
      width: '95%',
      height: 'auto',
      shadowColor: COLORS.color_primary,
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.1,
      elevation: 2,
      padding: 10,
      backgroundColor: COLORS.color_textfield,
      borderRadius: 10,
    },
    detail_container: {
      width: '100%',
      alignSelf: 'center',
      marginBottom: 10,
    },
    package_title: {
      fontSize: 14,
      color: COLORS.color_text,
      fontFamily: 'Poppins-SemiBold',
      marginBottom: 5,
      alignSelf: 'center',
    },

    detail_container_body: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },

    text_icon_container: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    text: {
      fontFamily: 'Poppins-Regular',
      color: COLORS.color_text,
      fontSize: 12,
      marginLeft: 5,
    },
  });
  return {
    Styles,
    COLORS,
  };
};
export default useStyles;
