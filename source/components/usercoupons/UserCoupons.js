import React from 'react';
import Flag from 'react-native-flags';
import {Text, View, TouchableOpacity} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import useStyles from './useStyles';
const UserCoupons = ({data}) => {
  const {Styles, COLORS} = useStyles();

  return (
    <View style={Styles.card_container}>
      <View style={Styles.detail_container}>
        <Text style={Styles.package_title}>{data.name.toUpperCase()}</Text>
        <View style={Styles.detail_container_body}>
          <View style={Styles.text_icon_container}>
            <MaterialCommunityIcons
              name="currency-usd"
              size={16}
              color={COLORS.color_yellow}
            />
            <Text style={Styles.text}>{data.amount}</Text>
          </View>
          <View style={Styles.text_icon_container}>
            <MaterialCommunityIcons
              name={data.life == null ? 'timer-off-outline' : null}
              size={16}
              color={COLORS.color_yellow}
            />
            <Text style={Styles.text}>{data.expiry_date}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};
export default UserCoupons;
