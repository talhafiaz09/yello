import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    available_balance_text: {
      fontFamily: 'Poppins-Bold',
      fontSize: 14,
      color: COLORS.color_yellow,
    },
    credit_display_container: {
      flexDirection: 'row',
      width: '100%',
      flexWrap: 'wrap',
    },
    credit_text_view: {flex: 1, flexDirection: 'row', alignItems: 'center'},
    credit_number: {
      fontFamily: 'Poppins-Light',
      fontSize: 24,
      color: COLORS.color_text,
      flexDirection: 'row',
    },
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
