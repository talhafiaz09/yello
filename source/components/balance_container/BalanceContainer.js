import React, {useEffect} from 'react';
import {Share, Text, View, TouchableOpacity, Platform} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import ConfirmationModal from '../confirmationmodal/ConfirmationModal';
// import Colors from '../../assets/colors/Colors';
import {useDispatch, useSelector} from 'react-redux';
import {setModalStructure} from '../../redux_handler/actions/app';
import ProfileAvatar from '../../assets/images/profileavatar/ProfileAvatar';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import useStyles from './useStyles';

const BalanceContainer = ({
  symbol,
  text,
  title,
  name,
  creditNumber,
  size,
  titleStyle,
  mainContainer,
  creditContainer,
}) => {
  const {Styles, COLORS, hp} = useStyles();

  return (
    <View style={{...mainContainer}}>
      <Text style={[Styles.available_balance_text, {...titleStyle}]}>
        {title}
      </Text>
      <View style={[Styles.credit_display_container, {}]}>
        <View style={[Styles.credit_text_view, {...creditContainer}]}>
          {name && (
            <FontAwesome5
              name={name}
              color={COLORS.color_yellow}
              size={size ? size : 18}
            />
          )}
          <Text
            numberOfLines={1}
            style={[Styles.credit_number, {...creditNumber}]}>
            {symbol && (
              <Text
                style={[
                  Styles.credit_number,
                  {
                    color: COLORS.color_yellow,
                    fontFamily: 'Poppins-Semibold',
                    ...creditNumber,
                  },
                ]}>
                {symbol}
              </Text>
            )}
            {' ' + text}
          </Text>
        </View>
      </View>
    </View>
  );
};
export default BalanceContainer;
