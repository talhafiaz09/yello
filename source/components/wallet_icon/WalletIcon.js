import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import Colors from '../../assets/colors/Colors';
import useStyles from './useStyles';
import {useSelector} from 'react-redux';
const WalletIcon = ({backgroundColor, color, fontFamily, borderColor}) => {
  const {Styles} = useStyles();

  const currency_rates = useSelector(
    (state) => state.userReducer.currency_rates,
  );
  const userInformation = useSelector(
    (state) => state.userReducer.userInformation,
  );
  return (
    <View
      style={[Styles.body, {backgroundColor: backgroundColor, borderColor}]}>
      <Text
        style={[
          Styles.text,
          {
            color: color,
            fontFamily: fontFamily,
          },
        ]}>
        {currency_rates[userInformation.currency]?.symbol
          ? currency_rates[userInformation.currency]?.symbol +
            ' ' +
            Math.floor(
              parseFloat(
                userInformation.balance *
                  currency_rates[userInformation.currency]?.rate,
              ) * 1000,
            ) /
              1000
          : 0}
      </Text>
    </View>
  );
};
export default WalletIcon;
