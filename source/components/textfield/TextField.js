import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TextInput,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import useStyles from './useStyles';
import {useDispatch, useSelector} from 'react-redux';
import {setSearchBarText} from '../../redux_handler/actions/app';
const SearchBar = ({placeholder, onChangeText, value, onClick}) => {
  const {Styles, COLORS} = useStyles();

  return (
    <View style={Styles.text_field_main_container}>
      <View style={Styles.icon_container}>
        <MaterialCommunityIcons
          name="chat"
          size={20}
          color={COLORS.color_yellow}
        />
      </View>
      <View style={Styles.text_field_container}>
        <TextInput
          style={Styles.text_field}
          value={value}
          placeholder={placeholder}
          placeholderTextColor={COLORS.color_text}
          onChangeText={onChangeText}
        />
      </View>
      <TouchableOpacity
        style={[Styles.icon_container, {paddingRight: 10}]}
        onPress={onClick}>
        <MaterialCommunityIcons
          name="send"
          size={20}
          color={COLORS.color_yellow}
        />
      </TouchableOpacity>
    </View>
  );
};
export default SearchBar;
