import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    text_field_main_container: {
      width: wp('95%'),
      height: hp('6%'),
      alignItems: 'center',
      flexDirection: 'row',
      alignSelf: 'center',
      borderRadius: 10,
      marginBottom: 10,
      padding: 2,
      shadowOffset: {width: 0, height: 1},
      shadowColor: COLORS.color_primary,
      elevation: 5,
      shadowOpacity: 0.2,
      backgroundColor: COLORS.color_textfield,
      marginBottom: hp('5.5%'),
    },
    icon_container: {
      flex: 0.08,
      alignItems: 'center',
      paddingLeft: 10,
    },
    text_field_container: {flex: 0.95},
    text_field: {
      paddingTop: 0,
      paddingBottom: 0,
      margin: 0,
      paddingLeft: 5,
      paddingRight: 5,
      height: '100%',
      color: COLORS.color_text,
      fontFamily: 'Poppins-Regular',
    },
  });
  return {
    Styles,
    COLORS,
    hp,
  };
};
export default useStyles;
