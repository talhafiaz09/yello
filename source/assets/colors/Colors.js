const CONSTANT_COLORS = {
  color_primary: '#000000',
  color_white: '#ffffff',
  color_blur: '#1C1C1CB3',
  color_shadow: '#00000066',
  color_yellow: '#FFA200',
  color_dot_inactive: '#FFA20059',
  color_side: '#3D3737',
  color_transparent: '#00000000',
  color_green: '#0F8712',
  color_badge: '#D73420',
  color_bt_icon: '#5A5A5A',
};

export const COLORS = {
  COLORS_DARK: {
    ...CONSTANT_COLORS,
    color_border: '#ffffff',
    color_top: '#3D3737',
    color_bottom: '#000000',
    color_textfield: '#3D3737',
    color_text: '#ffffff',
    color_active_tab: '#9A9A9A',
    color_inactive_tab: '#000000',
  },
  COLORS_LIGHT: {
    ...CONSTANT_COLORS,
    color_border: '#FFA200',
    color_top: '#000000',
    color_bottom: '#ECEFF4',
    color_textfield: '#FFFFFF',
    color_text: '#5A5A5A',
    color_active_tab: '#000000',
    color_inactive_tab: '#9A9A9A',
  },
};
