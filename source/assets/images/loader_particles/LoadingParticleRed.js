import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

export const LoadingParticleRed = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={10.5}
    height={11.5}
    viewBox="0 0 10.5 11.498"
    {...props}>
    <Path
      id="Path_2273"
      data-name="Path 2273"
      d="M573.87,442.337h6.59l3.911-11.5h-10.5Z"
      transform="translate(-573.87 -430.838)"
      fill="#e20509"
    />
  </Svg>
);
