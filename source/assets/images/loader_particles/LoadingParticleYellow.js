import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

export const LoadingParticleYellow = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={10.5}
    height={11.498}
    viewBox="0 0 10.5 11.498"
    {...props}>
    <Path
      id="Path_14"
      data-name="Path 14"
      d="M584.37,442.337h-6.59l-3.911-11.5h10.5Z"
      transform="translate(-573.87 -430.838)"
      fill="#ffa200"
    />
  </Svg>
);
