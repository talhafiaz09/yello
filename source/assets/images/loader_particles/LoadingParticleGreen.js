import * as React from 'react';
import Svg, {Rect} from 'react-native-svg';

export const LoadingParticleGreen = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={10.5}
    height={11.498}
    viewBox="0 0 10.5 11.498"
    {...props}>
    <Rect
      id="Rectangle_2"
      data-name="Rectangle 2"
      width={10.5}
      height={11.498}
      fill="#0f8712"
    />
  </Svg>
);
