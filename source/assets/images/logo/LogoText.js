import React from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import Logo from './Logo';
import useStyles from './useStyles';
const LogoText = (props, {loading}) => {
  const {Styles} = useStyles();
  return (
    <View style={Styles.main}>
      <Logo {...props} />
      {loading ? null : <Text style={Styles.logo_container_text}>yello</Text>}
    </View>
  );
};
export default LogoText;
