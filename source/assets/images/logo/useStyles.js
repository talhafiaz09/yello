import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
const useStyles = () => {
  const COLORS = useSelector((state) => state.appthemeReducer.colors);
  const Styles = StyleSheet.create({
    main: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    logo_container_text: {
      fontFamily: 'Poppins-ExtraboldItalic',
      color: COLORS.color_white,
      fontSize: 42,
    },
  });
  return {
    Styles,
  };
};
export default useStyles;
