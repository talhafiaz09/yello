import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

export const SemiCircle = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={39.855}
    height={79.163}
    viewBox="0 0 39.855 79.163"
    {...props}>
    <Path
      id="Path_31"
      data-name="Path 31"
      d="M109.838,84.4h-.3V5.241h.3a39.581,39.581,0,0,1,0,79.163Z"
      transform="translate(-109.543 -5.241)"
      fill="#5a5a5a"
      opacity={0.38}
    />
  </Svg>
);
