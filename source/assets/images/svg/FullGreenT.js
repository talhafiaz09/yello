import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

export const FullGreenT = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={81}
    height={94}
    viewBox="0 0 81.274 94.103"
    preserveAspectRatio="none"
    {...props}>
    <Path
      id="Path_2279"
      data-name="Path 2279"
      d="M2756,977.882v94.1l81.274-40.816Z"
      transform="translate(-2756 -977.882)"
      fill="#0f8712"
    />
  </Svg>
);
