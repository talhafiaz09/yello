import * as React from 'react';
import Svg, {
  Defs,
  Pattern,
  Image,
  Rect,
  LinearGradient,
  Stop,
  G,
  Path,
} from 'react-native-svg';

export const FullRedT = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={29}
    height={35}
    viewBox="0 0 28.885 35.269"
    preserveAspectRatio="none"
    {...props}>
    <Path
      id="Path_2278"
      data-name="Path 2278"
      d="M2221,970.736v35.269l28.885-9.771Z"
      transform="translate(-2221 -970.736)"
      fill="#fb0908"
    />
  </Svg>
);
