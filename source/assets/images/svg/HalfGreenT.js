import * as React from 'react';
import Svg, {G, LinearGradient, Stop, Path, Defs} from 'react-native-svg';

export const HalfGreenT = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={52}
    height={115}
    viewBox="0 0 52.498 115.043"
    preserveAspectRatio="none"
    {...props}>
    <Path
      id="Path_2276"
      data-name="Path 2276"
      d="M1103,957l-52.45,115.043,52.5-7.4Z"
      transform="translate(-1050.55 -957)"
      fill="#0f8712"
    />
  </Svg>
);
