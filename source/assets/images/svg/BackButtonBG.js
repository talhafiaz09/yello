import * as React from 'react';
import Svg, {G, Image, Polygon, Defs, Path} from 'react-native-svg';

export const BackButtonBG = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={118}
    height={64}
    viewBox="0 0 117 64"
    preserveAspectRatio="none"
    {...props}>
    <G filter="url(#a)">
      <Path
        data-name="Path 2275"
        d="M.5.504v61.728h114.167L78.94.504Z"
        fill="#ffa200"
      />
    </G>
  </Svg>
);
