import React from 'react';
import Svg, {G, Path, Rect, Circle, Text, TSpan} from 'react-native-svg';

const RewardpointsIcon = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={334}
    height={319}
    viewBox="0 0 333.922 319.406"
    preserveAspectRatio="none"
    {...props}>
    <G
      id="Group_1310"
      data-name="Group 1310"
      transform="translate(-52.078 -119.424)">
      <G
        id="_43031_Converted_"
        data-name="43031 [Converted]"
        transform="translate(-140.925 56.684)">
        <G id="Group_1308" data-name="Group 1308">
          <G id="Shadow" transform="translate(193.002 349.308)">
            <G id="Group_1019" data-name="Group 1019" opacity={0.53}>
              <Path
                id="Path_1940"
                data-name="Path 1940"
                d="M340.742,691a282.963,282.963,0,0,0-28.275,1.209c-3.549.365-7.024,1.172-10.582,1.5-7.107.648-14.085,1.861-21.215,2.417-5.743.452-11.508.716-17.237,1.286a65.354,65.354,0,0,1-16.64-.474,117.23,117.23,0,0,1-11.782-2.075,51.712,51.712,0,0,0-13.57-1.989c-6.445.041-12.721.05-18.888,2.126a18.982,18.982,0,0,0-6.459,3.458c-3.763,3.5-4.274,12.242-.515,15.873,6.331,6.112,19.837,8.511,28.709,8.625,9.72.123,19.167-.547,28.9-.94,9.045-.365,18.072-.885,27.131-.867,18.51.036,37.02,0,55.493-1.149,17.356-1.076,34.757-.707,52.154-.707,17.839,0,35.555.132,53.262,2.331,7.718.958,15.577,1.3,23.331,1.966,6.947.6,13.62-.014,20.535-.566a51.976,51.976,0,0,0,16.854-4.192c4.178-1.852,11.622-7.608,12.9-12.028,3.59-12.4-50.612-12.1-58.932-12.069-9.31.032-18.692.315-28-.014a70.78,70.78,0,0,1-11.586-1.624c-3.8-.775-7.636-.579-11.494-.9-16.088-1.359-32.212-1.159-48.35-1.159C351.246,691.032,346,690.987,340.742,691Z"
                transform="translate(-193.003 -691.001)"
                fill="#4d8627"
              />
            </G>
          </G>
          <G id="Object" transform="translate(200.163 62.74)">
            <G
              id="Group_1046"
              data-name="Group 1046"
              transform="translate(94.364)">
              <G
                id="Group_1032"
                data-name="Group 1032"
                transform="translate(17.578)">
                <G
                  id="Group_1025"
                  data-name="Group 1025"
                  transform="translate(29.468)">
                  <G
                    id="Group_1021"
                    data-name="Group 1021"
                    transform="translate(2.02 4.787)">
                    <G id="Group_1020" data-name="Group 1020">
                      <Path
                        id="Path_1941"
                        data-name="Path 1941"
                        d="M552.242,81.552a6.866,6.866,0,0,1-1.56,4.251c-5.7,8.229-18.82,16.941-27.532,11.234,1.008-.182,3.613-.114,5.67-.552,4.885-1.031,8.133-6.24,10.806-10.924,1.638-2.869,2.107-11.13,5.255-12.211C547.881,72.315,552.023,78.418,552.242,81.552Z"
                        transform="translate(-523.15 -73.234)"
                        fill="#292949"
                      />
                    </G>
                  </G>
                  <G id="Group_1024" data-name="Group 1024">
                    <G id="Group_1023" data-name="Group 1023">
                      <G id="Group_1022" data-name="Group 1022">
                        <Path
                          id="Path_1942"
                          data-name="Path 1942"
                          d="M522.223,80.137a44.974,44.974,0,0,1,7.453-11.371c2.965-3.448,6.983-6.039,11.28-6.025a3.021,3.021,0,0,1,1.5.3,4.142,4.142,0,0,1,1.254,1.359c1.971,2.887,5.775,6.764,6.076,10.778-7.444-.789-15.28,1.847-21.392,6.577-4.561,3.535-7.321,10.058-7.8,10.049C516,91.7,521.179,82.413,522.223,80.137Z"
                          transform="translate(-518.72 -62.74)"
                          fill="#c10d11"
                        />
                      </G>
                    </G>
                  </G>
                </G>
                <G
                  id="Group_1031"
                  data-name="Group 1031"
                  transform="translate(0 4.581)">
                  <G
                    id="Group_1027"
                    data-name="Group 1027"
                    transform="translate(0.052 5.2)">
                    <G id="Group_1026" data-name="Group 1026">
                      <Path
                        id="Path_1943"
                        data-name="Path 1943"
                        d="M454.243,93.505a6.76,6.76,0,0,0,2.176,3.946c6.856,7.207,21.11,13.7,28.868,6.655-1.022-.018-3.59.47-5.683.369-4.981-.233-8.963-4.853-12.3-9.045-2.048-2.573-3.736-10.655-7.006-11.216C457.181,83.68,453.993,90.376,454.243,93.505Z"
                        transform="translate(-454.23 -84.184)"
                        fill="#292949"
                      />
                    </G>
                  </G>
                  <G id="Group_1030" data-name="Group 1030">
                    <G id="Group_1029" data-name="Group 1029">
                      <G id="Group_1028" data-name="Group 1028">
                        <Path
                          id="Path_1944"
                          data-name="Path 1944"
                          d="M482.1,87.091a43.9,43.9,0,0,0-9.054-10.03c-3.444-2.928-7.8-4.84-12.037-4.128a2.979,2.979,0,0,0-1.432.543,4.206,4.206,0,0,0-1.04,1.546c-1.519,3.17-4.7,7.613-4.4,11.627,7.234-1.98,15.372-.643,22.113,3.047,5.036,2.755,8.73,8.758,9.2,8.666C489.961,97.5,483.47,89.17,482.1,87.091Z"
                          transform="translate(-454.116 -72.783)"
                          fill="#c10d11"
                        />
                      </G>
                    </G>
                  </G>
                </G>
              </G>
              <G
                id="Group_1039"
                data-name="Group 1039"
                transform="translate(0 25.47)">
                <G
                  id="Group_1035"
                  data-name="Group 1035"
                  transform="translate(5.971 0.483)">
                  <G id="Group_1034" data-name="Group 1034">
                    <G id="Group_1033" data-name="Group 1033">
                      <Path
                        id="Path_1945"
                        data-name="Path 1945"
                        d="M521.866,197.515l-87.372,7.1-5.825-77.875,87.372-7.1Z"
                        transform="translate(-428.67 -119.64)"
                        fill="#e9a126"
                      />
                    </G>
                  </G>
                </G>
                <G id="Group_1038" data-name="Group 1038">
                  <G id="Group_1037" data-name="Group 1037">
                    <G id="Group_1036" data-name="Group 1036">
                      <Path
                        id="Path_1946"
                        data-name="Path 1946"
                        d="M516.366,138.244l-99.318,8.064-1.469-19.664,99.313-8.064Z"
                        transform="translate(-415.58 -118.58)"
                        fill="#e9a126"
                      />
                    </G>
                  </G>
                </G>
              </G>
              <G
                id="Group_1042"
                data-name="Group 1042"
                transform="translate(11.161 45.622)"
                opacity={0.1}>
                <G id="Group_1041" data-name="Group 1041">
                  <G id="Group_1040" data-name="Group 1040">
                    <Path
                      id="Path_1947"
                      data-name="Path 1947"
                      d="M440.05,169.552l83.65-6.792.525,7.011Z"
                      transform="translate(-440.05 -162.76)"
                    />
                  </G>
                </G>
              </G>
              <G
                id="Group_1045"
                data-name="Group 1045"
                transform="translate(43.419 26.803)">
                <G id="Group_1044" data-name="Group 1044">
                  <G id="Group_1043" data-name="Group 1043">
                    <Path
                      id="Path_1948"
                      data-name="Path 1948"
                      d="M527.487,201.7l-10.815.876-5.9-78.851a1.405,1.405,0,0,1,1.213-1.555l8.174-.661a1.382,1.382,0,0,1,1.428,1.341Z"
                      transform="translate(-510.77 -121.501)"
                      fill="#c10d11"
                    />
                  </G>
                </G>
              </G>
            </G>
            <G
              id="Group_1064"
              data-name="Group 1064"
              transform="translate(217.934 104.996)">
              <G
                id="Group_1047"
                data-name="Group 1047"
                transform="translate(46.416 34.521)">
                <Path
                  id="Path_1949"
                  data-name="Path 1949"
                  d="M792.9,368.61l29.489,31.966-22.414,19.03-3.312-4,8.776-18.241L788.25,382.212Z"
                  transform="translate(-788.25 -368.61)"
                  fill="#babcbe"
                />
              </G>
              <G id="Group_1063" data-name="Group 1063">
                <G
                  id="Group_1051"
                  data-name="Group 1051"
                  transform="translate(33.097 109.942)">
                  <G id="Group_1049" data-name="Group 1049">
                    <G id="Group_1048" data-name="Group 1048">
                      <Path
                        id="Path_1950"
                        data-name="Path 1950"
                        d="M784.361,538.2l1.332,30.866-3.9,52.487-5.838.415-7.093-47.67-9.811-40.34Z"
                        transform="translate(-759.05 -533.96)"
                        fill="#c10d11"
                      />
                    </G>
                  </G>
                  <G
                    id="Group_1050"
                    data-name="Group 1050"
                    transform="translate(16.886 86.865)">
                    <Path
                      id="Path_1951"
                      data-name="Path 1951"
                      d="M796.294,728l6.541,2.709,2.933-3.38-3.385-2.933-6.313.447Z"
                      transform="translate(-796.07 -724.4)"
                      fill="#212135"
                    />
                  </G>
                </G>
                <G
                  id="Group_1055"
                  data-name="Group 1055"
                  transform="translate(15.8 100.86)">
                  <G id="Group_1053" data-name="Group 1053">
                    <G id="Group_1052" data-name="Group 1052">
                      <Path
                        id="Path_1952"
                        data-name="Path 1952"
                        d="M722.412,516.449,721.13,539.78l3.8,69.382,5.843.292,8.762-45.5,7.727-49.9Z"
                        transform="translate(-721.13 -514.05)"
                        fill="#c10d11"
                      />
                    </G>
                  </G>
                  <G
                    id="Group_1054"
                    data-name="Group 1054"
                    transform="translate(0.009 94.387)">
                    <Path
                      id="Path_1953"
                      data-name="Path 1953"
                      d="M730.633,724.46l-6.482,2.842-3-3.316,3.316-3.006,6.322.319Z"
                      transform="translate(-721.15 -720.98)"
                      fill="#212135"
                    />
                  </G>
                </G>
                <G
                  id="Group_1058"
                  data-name="Group 1058"
                  transform="translate(10.518 32.569)">
                  <G id="Group_1057" data-name="Group 1057">
                    <G id="Group_1056" data-name="Group 1056">
                      <Path
                        id="Path_1954"
                        data-name="Path 1954"
                        d="M750.1,366.282l-18.241-1.952s-10.149,1.086-17.032,25.183c-3.831,13.406-4.995,34.529-5.277,50.635.429,2.3,2.933,12.361,13.515,10.651,18.172-2.933,27.847-5.861,35.761-4.4,6.742,1.464,6.172-7.818,5.432-14.9l-2.687-21.917Z"
                        transform="translate(-709.55 -364.33)"
                        fill="#c10d11"
                      />
                    </G>
                  </G>
                </G>
                <G
                  id="Group_1061"
                  data-name="Group 1061"
                  transform="translate(23.826)">
                  <G
                    id="Group_1059"
                    data-name="Group 1059"
                    transform="translate(5.291 10.435)">
                    <Path
                      id="Path_1955"
                      data-name="Path 1955"
                      d="M768.755,317.189c-4.6-1.013-9.328-1.92-13.93-1.008a3.862,3.862,0,0,0-2.636,1.419,4.14,4.14,0,0,0-.429,1.108c-1.163,4.315-2.636,10.719.205,14.7a7.094,7.094,0,0,0,2.053,1.847,10.138,10.138,0,0,0,3.4,1.336,18,18,0,0,1-1.031,3.134c1.054,1.587,5.962,1.779,6.869.255.05-1.318.1-2.632.151-3.95a9.431,9.431,0,0,0,1.715-.894,11.1,11.1,0,0,0,4.283-5.825c.876-2.554,2.1-4.986,2.906-7.585a3.671,3.671,0,0,0,.023-2.887,3.924,3.924,0,0,0-2.709-1.455Z"
                      transform="translate(-750.324 -315.805)"
                      fill="#aa6755"
                    />
                  </G>
                  <G id="Group_1060" data-name="Group 1060">
                    <Path
                      id="Path_1956"
                      data-name="Path 1956"
                      d="M747.143,307.215a14.345,14.345,0,0,1,7.818-1.934c6.459.47,8.826,5.214,9.173,11.152,1.779-2.262,5.118-5.109,5.323-8.019.164-2.381-.164-5.633-1.5-7.69-1.25-1.925-4.443-2.983-6.34-4.1q-2.682-1.58-5.369-3.156a2.718,2.718,0,0,0-2.691-.347q-6.349,1.642-12.694,3.28c-.812.21-1.738.515-2.039,1.3a2.7,2.7,0,0,0,.032,1.51c.479,2.2,1.4,4.443,1.633,6.669.26,1.291,1.031,7.918,3.316,7.152.671-1.1.771-2.454,1.3-3.631A5.282,5.282,0,0,1,747.143,307.215Z"
                      transform="translate(-738.725 -292.928)"
                      fill="#292949"
                    />
                  </G>
                </G>
                <G
                  id="Group_1062"
                  data-name="Group 1062"
                  transform="translate(0 32.569)">
                  <Path
                    id="Path_1957"
                    data-name="Path 1957"
                    d="M719.318,364.33l-6.742,2.053-24.622,25.794-1.464,8.5,11.768,19.436,3.179-7.12-4.981-14.364,7.914-5.569,10.55-23.746Z"
                    transform="translate(-686.49 -364.33)"
                    fill="#c10d11"
                  />
                </G>
              </G>
            </G>
            <G
              id="Group_1083"
              data-name="Group 1083"
              transform="translate(100.412 96.996)">
              <G
                id="Group_1068"
                data-name="Group 1068"
                transform="translate(56.305 0)">
                <G
                  id="Group_1065"
                  data-name="Group 1065"
                  transform="translate(34.21)">
                  <Path
                    id="Path_1958"
                    data-name="Path 1958"
                    d="M632.827,283.536l1.218-7.485-3.175-.661-1.97,4.944-1.359-1.008-.26,6.532,3.211,1.131Z"
                    transform="translate(-627.28 -275.39)"
                    fill="#eac1a4"
                  />
                </G>
                <G
                  id="Group_1067"
                  data-name="Group 1067"
                  transform="translate(0 10.468)">
                  <G id="Group_1066" data-name="Group 1066">
                    <Path
                      id="Path_1959"
                      data-name="Path 1959"
                      d="M552.28,339.246l21.073-17.73L586.49,298.34l4.356,1.163-6.568,28.773-22.268,26.861Z"
                      transform="translate(-552.28 -298.34)"
                      fill="#c10d11"
                    />
                  </G>
                </G>
              </G>
              <G id="XMLID_108_" transform="translate(47.761 135.457)">
                <G id="XMLID_110_" transform="translate(5.136 69.072)">
                  <G id="Group_1069" data-name="Group 1069">
                    <Path
                      id="Path_1960"
                      data-name="Path 1960"
                      d="M545.116,727.01l.31,3.22,6.518,2.792,2.8-3.435-3.412-3.006-6.518-2.792Z"
                      transform="translate(-544.81 -723.79)"
                      fill="#212135"
                    />
                  </G>
                </G>
                <G id="XMLID_109_">
                  <G id="Group_1070" data-name="Group 1070">
                    <Path
                      id="Path_1961"
                      data-name="Path 1961"
                      d="M533.55,579.389l5.017,64.25,6.281.853,5.2-54.758,4.3-17.374Z"
                      transform="translate(-533.55 -572.36)"
                      fill="#e9a126"
                    />
                  </G>
                </G>
              </G>
              <G id="XMLID_105_" transform="translate(24.9 125.627)">
                <G id="XMLID_107_" transform="translate(4.073)">
                  <G id="Group_1072" data-name="Group 1072">
                    <G id="Group_1071" data-name="Group 1071">
                      <Path
                        id="Path_1962"
                        data-name="Path 1962"
                        d="M496.123,550.81l-3.763,76.342,5.729,1.081,11.937-57.769,6.568-16.726Z"
                        transform="translate(-492.36 -550.81)"
                        fill="#e9a126"
                      />
                    </G>
                  </G>
                </G>
                <G id="XMLID_106_" transform="translate(0 75.535)">
                  <G id="Group_1073" data-name="Group 1073">
                    <Path
                      id="Path_1963"
                      data-name="Path 1963"
                      d="M492.721,720.825l-6.805,2.08-2.486-3.831,3.708-2.664,6.194,1.163Z"
                      transform="translate(-483.43 -716.41)"
                      fill="#212135"
                    />
                  </G>
                </G>
              </G>
              <G id="XMLID_104_" transform="translate(11.458 40.908)">
                <G id="Group_1075" data-name="Group 1075">
                  <G id="Group_1074" data-name="Group 1074">
                    <Path
                      id="Path_1964"
                      data-name="Path 1964"
                      d="M483.344,367.34c4.639-3.841,15.823-1.633,21.712-1.633-.566,0,18.314,74.217,19.65,78.459,2.217,7.034,2.212,15.317,1.587,22.6-6.317,1.738-12.233,4.986-18.729,5.829-5.346.693-10.742-.292-16.111-.766-6.349-.561-12.74-.411-19.094-.9s-12.776-1.656-18.4-4.653a92.234,92.234,0,0,0,6.062-15.239c1.6-5.191,3.658-10.961,4.242-16.375.529-4.89.616-9.871,1.327-14.829.83-5.77,1.825-11.522,3.207-17.187a129.084,129.084,0,0,1,6.226-18.989c1.948-4.689,4.274-12.366,7.937-15.974Q483.145,367.5,483.344,367.34Z"
                      transform="translate(-453.96 -365.076)"
                      fill="#c10d11"
                    />
                  </G>
                </G>
              </G>
              <G id="XMLID_100_" transform="translate(34.742 16.343)">
                <G id="XMLID_103_" transform="translate(0 2.888)">
                  <G id="Group_1076" data-name="Group 1076">
                    <Path
                      id="Path_1965"
                      data-name="Path 1965"
                      d="M520.964,323.731c-2.937.534-6.564,3.047-7.271,6.149-.342,1.492.146,2.869-.283,4.452-.835,3.125,2.947,7.731-.794,9.9-1.925,1.117-4.329.753-6.518.36a1.661,1.661,0,0,1-.753-.269,1.141,1.141,0,0,1-.274-1.222,4.682,4.682,0,0,1,.657-1.154c2.573-3.868,1.948-9.415,2.951-13.821s3.362-8.776,7.718-10.578a10.01,10.01,0,0,1,4.292,6.226Z"
                      transform="translate(-505.008 -317.55)"
                      fill="#292949"
                    />
                  </G>
                </G>
                <G id="XMLID_102_" transform="translate(6.671 5.541)">
                  <G id="Group_1077" data-name="Group 1077">
                    <Path
                      id="Path_1966"
                      data-name="Path 1966"
                      d="M525.228,345.823a9.73,9.73,0,0,1-5.318-7.1,19.455,19.455,0,0,1,.707-9.109c2.769-9.852,15.134-7.029,18.017,1.4C541.147,338.347,533.452,349.34,525.228,345.823Z"
                      transform="translate(-519.633 -323.366)"
                      fill="#eac1a4"
                    />
                  </G>
                </G>
                <G id="XMLID_101_" transform="translate(11.033)">
                  <G id="Group_1078" data-name="Group 1078">
                    <Path
                      id="Path_1967"
                      data-name="Path 1967"
                      d="M544.824,344.636a8.635,8.635,0,0,1-3.553-3.38c-1.3-2.6-.753-6.65-.908-9.478-.164-2.979-.433-6.226-2.477-8.4a7.053,7.053,0,0,0-4.826-2c-1.268-.128-1.359.347-2.376-.429a5.475,5.475,0,0,1-1.46-3.143,5.28,5.28,0,0,1,1.688-4.251,7.428,7.428,0,0,1,6.021-2.322c8.3.525,14.09,5.962,15.737,14.254.789,3.982-.411,8.242.47,12.087.543,2.372,1.893,4.037,2.281,6.55,1.013,6.518,3.841,14.979,3.01,21.561a.935.935,0,0,1-.278.657c-.4.3-.926-.151-1.218-.556q-2.559-3.551-5.122-7.1a12.365,12.365,0,0,1-1.738-2.951c-.967-2.682-.068-5.971-1.738-8.283A11.574,11.574,0,0,0,544.824,344.636Z"
                      transform="translate(-529.195 -311.219)"
                      fill="#292949"
                    />
                  </G>
                </G>
              </G>
              <G
                id="Group_1082"
                data-name="Group 1082"
                transform="translate(0 6.34)">
                <G id="Group_1079" data-name="Group 1079">
                  <Path
                    id="Path_1968"
                    data-name="Path 1968"
                    d="M430.893,297.605l-2.053-7.3,3.083-1.013,2.513,4.689,1.236-1.154.99,6.463-3.061,1.487Z"
                    transform="translate(-428.84 -289.29)"
                    fill="#eac1a4"
                  />
                </G>
                <G
                  id="Group_1081"
                  data-name="Group 1081"
                  transform="translate(3.622 9.998)">
                  <G id="Group_1080" data-name="Group 1080">
                    <Path
                      id="Path_1969"
                      data-name="Path 1969"
                      d="M479.565,348.02l-22.93-15.253L440.981,311.21l-4.2,1.647,9.757,27.851,25.142,24.2Z"
                      transform="translate(-436.78 -311.21)"
                      fill="#c10d11"
                    />
                  </G>
                </G>
              </G>
            </G>
            <G
              id="Group_1107"
              data-name="Group 1107"
              transform="translate(0 94.807)">
              <G
                id="Group_1084"
                data-name="Group 1084"
                transform="translate(17.511)">
                <Path
                  id="Path_1970"
                  data-name="Path 1970"
                  d="M247.386,278.408l5.245-7.818,4.383,1.9-2.066,6.08,2.522.146-5.533,6.19-4.849-1.492Z"
                  transform="translate(-247.09 -270.59)"
                  fill="#eac1a4"
                />
              </G>
              <G
                id="Group_1086"
                data-name="Group 1086"
                transform="translate(51.201 50.266)">
                <G id="Group_1085" data-name="Group 1085">
                  <Path
                    id="Path_1971"
                    data-name="Path 1971"
                    d="M320.95,380.79l7.348,2.518,11.887,17.821,15.226-10.185,4.84,5.743-7.681,11.39-13.825,9.875L320.95,399.327Z"
                    transform="translate(-320.95 -380.79)"
                    fill="#4d8627"
                  />
                </G>
              </G>
              <G
                id="Group_1104"
                data-name="Group 1104"
                transform="translate(16.676 16.507)">
                <G
                  id="Group_1096"
                  data-name="Group 1096"
                  transform="translate(6.733 80.959)">
                  <G
                    id="Group_1090"
                    data-name="Group 1090"
                    transform="translate(17.401)">
                    <G id="Group_1088" data-name="Group 1088">
                      <G id="Group_1087" data-name="Group 1087">
                        <Path
                          id="Path_1972"
                          data-name="Path 1972"
                          d="M325.834,488.343l.52,45.23L321.538,591.8h-6.555l-7.841-53.718-8.972-53.81Z"
                          transform="translate(-298.17 -484.27)"
                          fill="#c10d11"
                        />
                      </G>
                    </G>
                    <G
                      id="Group_1089"
                      data-name="Group 1089"
                      transform="translate(16.849 106.748)">
                      <Path
                        id="Path_1973"
                        data-name="Path 1973"
                        d="M335.11,721.844l7.088,3.544,3.544-3.544L342.2,718.3H335.11Z"
                        transform="translate(-335.11 -718.3)"
                        fill="#212135"
                      />
                    </G>
                  </G>
                  <G id="Group_1095" data-name="Group 1095">
                    <G id="Group_1093" data-name="Group 1093">
                      <G id="Group_1092" data-name="Group 1092">
                        <G id="Group_1091" data-name="Group 1091">
                          <Path
                            id="Path_1974"
                            data-name="Path 1974"
                            d="M260.152,488.343l-.132,26.164,4.42,77.291h6.555l10.965-51.287,5.852-56.241Z"
                            transform="translate(-260.02 -484.27)"
                            fill="#c10d11"
                          />
                        </G>
                      </G>
                    </G>
                    <G
                      id="Group_1094"
                      data-name="Group 1094"
                      transform="translate(0.306 106.748)">
                      <Path
                        id="Path_1975"
                        data-name="Path 1975"
                        d="M271.322,721.844l-7.088,3.544-3.544-3.544,3.544-3.544h7.088Z"
                        transform="translate(-260.69 -718.3)"
                        fill="#212135"
                      />
                    </G>
                  </G>
                </G>
                <G
                  id="Group_1098"
                  data-name="Group 1098"
                  transform="translate(0 31.128)">
                  <G id="Group_1097" data-name="Group 1097">
                    <Path
                      id="Path_1976"
                      data-name="Path 1976"
                      d="M245.306,444.681c-.141,4.151-.05,9.706,1.7,9.912a1.687,1.687,0,0,0,.931-.246c3.038-1.478,5.2-3.562,8.2-4.99a37.052,37.052,0,0,1,10.2-2.819,106.073,106.073,0,0,1,15.727-1.378c4.5-.041,9.182,2.012,13.433,3.143,1.638.433,2.969,1.628,4.329,2.563,2.007,1.382,2.892,1.929,2.618-.894-.447-4.607-.912-9.218-1.478-13.812a170.473,170.473,0,0,0-5.273-27.185c-.994-3.389-2.144-6.732-3.289-10.076q-2.094-6.1-4.192-12.206c-1.738-5.058-4.666-8.621-9.8-10.391-4.434-1.528-9.944-1.948-14.259.192-4.794,2.381-7.075,10.482-9.405,14.815-2.043,3.8-4.616,14.145-5.378,18.241-.921,4.926-1.77,9.825-2.44,14.715a151.223,151.223,0,0,0-1.414,16.179C245.493,441.041,245.374,442.7,245.306,444.681Z"
                      transform="translate(-245.26 -375.022)"
                      fill="#4d8627"
                    />
                  </G>
                </G>
                <G
                  id="Group_1103"
                  data-name="Group 1103"
                  transform="translate(13.606)">
                  <G
                    id="Group_1099"
                    data-name="Group 1099"
                    transform="translate(5.507 12.904)">
                    <Path
                      id="Path_1977"
                      data-name="Path 1977"
                      d="M297.154,355.66a1.044,1.044,0,0,1-.36.205,1.96,1.96,0,0,1-.862.023c-1.172-.137-2.345-.278-3.517-.415.388-3.59-1.71-4.4-3.754-6.705-2.071-2.34-1.432-5.492-1.318-8.356a6.042,6.042,0,0,1,.411-2.34,5.129,5.129,0,0,1,3.307-2.386,12.461,12.461,0,0,1,10.318,1.136,5.493,5.493,0,0,1,1.98,2.007,7.416,7.416,0,0,1,.31,4.32,14.612,14.612,0,0,1-2.212,6.317c-1.177,1.756-3.421,2.6-3.681,4.7A2.367,2.367,0,0,1,297.154,355.66Z"
                      transform="translate(-287.161 -335.07)"
                      fill="#eac1a4"
                    />
                  </G>
                  <G id="Group_1102" data-name="Group 1102">
                    <G
                      id="Group_1100"
                      data-name="Group 1100"
                      transform="translate(21.344 13.41)">
                      <Path
                        id="Path_1978"
                        data-name="Path 1978"
                        d="M322.456,341.147a6.119,6.119,0,0,0-.4,2.709,5.205,5.205,0,0,0,2.874-3.439,5.277,5.277,0,0,0,.338-2.1,2.5,2.5,0,0,0-.931-1.834,1.572,1.572,0,0,0-2.422.68C321.69,338.186,322.757,339.792,322.456,341.147Z"
                        transform="translate(-321.882 -336.179)"
                        fill="#292949"
                      />
                    </G>
                    <G id="Group_1101" data-name="Group 1101">
                      <Path
                        id="Path_1979"
                        data-name="Path 1979"
                        d="M304.61,317.239a3.206,3.206,0,0,0-1.409-2.7,6.544,6.544,0,0,1-2.3-1.92c-.392-.693-.452-1.551-.839-2.249-.7-1.259-2.23-1.628-3.562-2.057-3.325-1.067-5.118-2.3-8.639-.935-2.5.972-5.738,1.1-8.064,2.522a8.077,8.077,0,0,0-2.472,2.6,14.693,14.693,0,0,0-1.382,12.662,15.463,15.463,0,0,0,1.241,2.4c.931,1.51,1.925,3.366,3.572,4.105.1-.712.634-1.1.8-1.8a5.654,5.654,0,0,0,.055-1.546c-.1-2.012-.721-5.2,1.291-6.194,2.541-1.263,7.312-.123,9.761.944,2.167.944,5.825,2.176,8.133,1.163C302.941,323.3,304.683,319.547,304.61,317.239Z"
                        transform="translate(-275.089 -306.779)"
                        fill="#292949"
                      />
                    </G>
                  </G>
                </G>
              </G>
              <G
                id="Group_1106"
                data-name="Group 1106"
                transform="translate(0 8.999)">
                <G id="Group_1105" data-name="Group 1105">
                  <Path
                    id="Path_1980"
                    data-name="Path 1980"
                    d="M246.271,329.971l-22.483-6.623,8.32-28.111-6.856-4.917L208.7,322.86l4.575,11.266,21.484,11.089Z"
                    transform="translate(-208.7 -290.32)"
                    fill="#4d8627"
                  />
                </G>
              </G>
            </G>
            <G
              id="Group_1108"
              data-name="Group 1108"
              transform="translate(225.023 161.935)">
              <Path
                id="Path_1981"
                data-name="Path 1981"
                d="M712.512,420.547l1.747,3.85-6.349,2.76-5.88-4.064,3.1-5.332Z"
                transform="translate(-702.03 -417.76)"
                fill="#aa6755"
              />
            </G>
            <G
              id="Group_1112"
              data-name="Group 1112"
              transform="translate(0.193 154.304)">
              <G id="Group_1110" data-name="Group 1110">
                <G id="Group_1109" data-name="Group 1109">
                  <Rect
                    id="Rectangle_421"
                    data-name="Rectangle 421"
                    width={244.837}
                    height={93.58}
                    transform="translate(0 15.327) rotate(-3.589)"
                    fill="#3d3737"
                  />
                </G>
              </G>
              <G
                id="Group_1111"
                data-name="Group 1111"
                transform="translate(6.868 6.441)">
                <Path
                  id="Path_1982"
                  data-name="Path 1982"
                  d="M229.284,511l-5.1-81.328L455.575,415.15l5.1,81.328Zm-3.549-79.96,4.922,78.4,228.466-14.336L454.2,416.7Z"
                  transform="translate(-224.18 -415.15)"
                  fill="#fff"
                />
              </G>
            </G>
            <G
              id="Group_1140"
              data-name="Group 1140"
              transform="translate(221.766 134.928)">
              <G id="Group_1130" data-name="Group 1130">
                <G
                  id="Group_1116"
                  data-name="Group 1116"
                  transform="translate(0 75.161)">
                  <G id="Group_1115" data-name="Group 1115">
                    <G id="Group_1114" data-name="Group 1114">
                      <G id="Group_1113" data-name="Group 1113">
                        <Path
                          id="Path_1983"
                          data-name="Path 1983"
                          d="M719.553,582.773l25.771-45.462-12.334-6.988-12.329-6.992L694.89,568.792l17.251-1.692Z"
                          transform="translate(-694.89 -523.33)"
                          fill="#e9a126"
                        />
                      </G>
                    </G>
                  </G>
                </G>
                <G
                  id="Group_1120"
                  data-name="Group 1120"
                  transform="translate(47.529 82.231)">
                  <G id="Group_1119" data-name="Group 1119">
                    <G id="Group_1118" data-name="Group 1118">
                      <G id="Group_1117" data-name="Group 1117">
                        <Path
                          id="Path_1984"
                          data-name="Path 1984"
                          d="M811.068,596.193,799.09,545.325l13.8-3.248,13.8-3.248L838.659,589.7l-16.083-6.463Z"
                          transform="translate(-799.09 -538.83)"
                          fill="#e9a126"
                        />
                      </G>
                    </G>
                  </G>
                </G>
                <G
                  id="Group_1129"
                  data-name="Group 1129"
                  transform="translate(3.973)">
                  <G id="Group_1125" data-name="Group 1125">
                    <G id="Group_1124" data-name="Group 1124">
                      <G id="Group_1123" data-name="Group 1123">
                        <G id="Group_1122" data-name="Group 1122">
                          <G id="Group_1121" data-name="Group 1121">
                            <Path
                              id="Path_1985"
                              data-name="Path 1985"
                              d="M796.97,421.172l7.476-4.789-5.8-6.714,5.98-6.559-7.339-4.99,4.078-7.882-8.384-2.919,1.9-8.671L786.027,378l-.406-8.867L776.9,370.8l-2.687-8.457-7.991,3.863-4.789-7.476-6.714,5.8-6.559-5.98-4.99,7.339-7.882-4.078-2.919,8.384-8.671-1.9-.648,8.854-8.867.406,1.665,8.721-8.457,2.687,3.863,7.991-7.476,4.785,5.8,6.719-5.98,6.559,7.339,4.99-4.078,7.882,8.384,2.919-1.9,8.671,8.854.648.406,8.867,8.721-1.665,2.687,8.457L742,451.92l4.785,7.476,6.719-5.8,6.559,5.98,4.99-7.339,7.882,4.078,2.919-8.384,8.671,1.9.648-8.853,8.867-.406-1.665-8.721,8.457-2.687Z"
                              transform="translate(-703.6 -358.55)"
                              fill="#ffa200"
                            />
                          </G>
                        </G>
                      </G>
                    </G>
                  </G>
                  <G
                    id="Group_1128"
                    data-name="Group 1128"
                    transform="translate(13.82 13.832)">
                    <G id="Group_1127" data-name="Group 1127">
                      <G id="Group_1126" data-name="Group 1126">
                        <Circle
                          id="Ellipse_107"
                          data-name="Ellipse 107"
                          cx={31.181}
                          cy={31.181}
                          r={31.181}
                          transform="matrix(0.196, -0.98, 0.98, 0.196, 0, 61.146)"
                          fill="#fff"
                        />
                      </G>
                    </G>
                  </G>
                </G>
              </G>
              <G
                id="Group_1139"
                data-name="Group 1139"
                transform="translate(34.377 29.053)">
                <G id="Group_1138" data-name="Group 1138">
                  <G id="Group_1137" data-name="Group 1137">
                    <G id="Group_1134" data-name="Group 1134">
                      <G id="Group_1133" data-name="Group 1133">
                        <G id="Group_1132" data-name="Group 1132">
                          <G id="Group_1131" data-name="Group 1131">
                            <Path
                              id="Path_1986"
                              data-name="Path 1986"
                              d="M793.946,422.708l4.52,12.981a.688.688,0,0,0,.465.438l13.228,3.617a.687.687,0,0,1,.233,1.209l-10.915,8.333a.686.686,0,0,0-.269.579l.648,13.739a.686.686,0,0,1-1.076.6l-11.266-7.832a.684.684,0,0,0-.634-.078l-12.826,4.871a.685.685,0,0,1-.9-.839l3.955-13.173a.694.694,0,0,0-.123-.629l-8.575-10.728a.689.689,0,0,1,.52-1.117l13.707-.31a.689.689,0,0,0,.556-.31l7.526-11.5A.691.691,0,0,1,793.946,422.708Z"
                              transform="translate(-770.258 -422.245)"
                              fill="#e9a126"
                            />
                          </G>
                        </G>
                      </G>
                    </G>
                    <G
                      id="Group_1136"
                      data-name="Group 1136"
                      transform="translate(4.576 40.47)">
                      <G id="Group_1135" data-name="Group 1135">
                        <Path
                          id="Path_1987"
                          data-name="Path 1987"
                          d="M780.29,510.97Z"
                          transform="translate(-780.29 -510.97)"
                          fill="#292949"
                        />
                      </G>
                    </G>
                  </G>
                </G>
              </G>
            </G>
            <G
              id="Group_1141"
              data-name="Group 1141"
              transform="translate(307.436 149.164)">
              <Path
                id="Path_1988"
                data-name="Path 1988"
                d="M893.484,393.9l.981,4.11-6.76,1.505L882.71,394.4l4.05-4.643Z"
                transform="translate(-882.71 -389.76)"
                fill="#aa6755"
              />
            </G>
            <G
              id="Group_1142"
              data-name="Group 1142"
              transform="translate(81.98 153.967)">
              <Path
                id="Path_1989"
                data-name="Path 1989"
                d="M392.563,400.29l7.5,2.814,1.866,7.964-4.356.99-3.526-4.826-2.9-.178-2.719-4.4Z"
                transform="translate(-388.43 -400.29)"
                fill="#eac1a4"
              />
            </G>
          </G>
        </G>
      </G>
      <G id="Group_1309" data-name="Group 1309">
        <Text
          id="Earned_Points"
          data-name="Earned Points"
          transform="matrix(0.998, -0.07, 0.07, 0.998, 104.986, 314.32)"
          fill="#fff"
          fontSize={18}
          fontWeight={800}
          fontFamily="Poppins">
          <TSpan x={parseInt('-' + props.points.toString().length * 3)} y={25}>
            {props.points + ' Earned Points'}
          </TSpan>
        </Text>
      </G>
    </G>
  </Svg>
);

export default RewardpointsIcon;
