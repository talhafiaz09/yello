import React from 'react';
import Svg, {G, Path} from 'react-native-svg';

const PackagesIcon = (props) => (
  <Svg width={13.45} height={23.073} viewBox="0 0 13.45 23.073" {...props}>
    <G
      id="Group_101"
      data-name="Group 101"
      transform="translate(-1029.598 -366.987)">
      <G
        id="Group_98"
        data-name="Group 98"
        transform="translate(1029.598 366.987)">
        <Path
          id="Path_58"
          data-name="Path 58"
          d="M1040.565,390.06h-8.485a2.486,2.486,0,0,1-2.483-2.483V369.469a2.485,2.485,0,0,1,2.483-2.482h8.485a2.485,2.485,0,0,1,2.483,2.482v18.108A2.485,2.485,0,0,1,1040.565,390.06Zm-8.485-22.192a1.6,1.6,0,0,0-1.6,1.6v18.108a1.6,1.6,0,0,0,1.6,1.6h8.485a1.6,1.6,0,0,0,1.6-1.6V369.469a1.6,1.6,0,0,0-1.6-1.6Z"
          transform="translate(-1029.598 -366.987)"
          fill="#fda50f"
        />
      </G>
      <G
        id="Group_99"
        data-name="Group 99"
        transform="translate(1035.371 386.035)">
        <Path
          id="Path_59"
          data-name="Path 59"
          d="M1050.206,433.743a.952.952,0,1,1,.952-.952A.953.953,0,0,1,1050.206,433.743Zm0-1.61a.658.658,0,1,0,.658.658A.659.659,0,0,0,1050.206,432.133Z"
          transform="translate(-1049.254 -431.839)"
          fill="#fda50f"
        />
      </G>
      <G
        id="Group_100"
        data-name="Group 100"
        transform="translate(1033.974 375.211)">
        <Path
          id="Path_60"
          data-name="Path 60"
          d="M1046.846,399.784a.44.44,0,0,1-.441-.441v-2.853l-1.156,1.156a.441.441,0,1,1-.623-.623l1.906-1.906.026-.025a.44.44,0,0,1,.234-.1.433.433,0,0,1,.083,0,.44.44,0,0,1,.282.128l1.909,1.909a.44.44,0,1,1-.623.623l-1.156-1.156v2.853A.441.441,0,0,1,1046.846,399.784Z"
          transform="translate(-1044.497 -394.987)"
          fill="#fda50f"
        />
      </G>
    </G>
  </Svg>
);

export default PackagesIcon;
