import React from 'react';
import Svg, {G, Path, Circle} from 'react-native-svg';

const ProfileAvatar = (props) => (
  <Svg
    id="Group_912"
    data-name="Group 912"
    xmlns="http://www.w3.org/2000/svg"
    width={46.588}
    height={49.147}
    viewBox="0 0 46.588 49.147"
    {...props}>
    <Path
      id="Path_961"
      data-name="Path 961"
      d="M95.48,134.5A11.053,11.053,0,1,1,84.427,123.45,11.06,11.06,0,0,1,95.48,134.5ZM84.427,149.638a23.671,23.671,0,0,0-23.233,19.133,3.229,3.229,0,0,0,3.177,3.827h40.111a3.229,3.229,0,0,0,3.177-3.827A23.685,23.685,0,0,0,84.427,149.638Z"
      transform="translate(-61.133 -123.45)"
      fill="#fff"
    />
  </Svg>
);

export default ProfileAvatar;
